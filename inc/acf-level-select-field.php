<?php

function acf_load_building_svg_map_field_choices( $field ) {
  $svg_maps = returnSVGMaps();
  if ( count($svg_maps) > 0 ) {
  $field['choices'] = $svg_maps;
  }
  return $field;
}

add_filter('acf/load_field/name=building_svg_map', 'acf_load_building_svg_map_field_choices');

function acf_load_floorplates_svg_map_field_choices( $field ) {
  $svg_maps = returnSVGMaps();
  if ( count($svg_maps) > 0 ) {
  $field['choices'] = $svg_maps;
  }
  return $field;
}

add_filter('acf/load_field/name=floorplates_svg_map', 'acf_load_floorplates_svg_map_field_choices');

function acf_load_location_svg_map_field_choices( $field ) {
  $svg_maps = returnSVGMaps();
  if ( count($svg_maps) > 0 ) {
  $field['choices'] = $svg_maps;
  }
  return $field;
}

add_filter('acf/load_field/name=location_svg_map', 'acf_load_location_svg_map_field_choices');

function acf_load_amenities_svg_map_field_choices( $field ) {
  $svg_maps = returnSVGMaps();
  if ( count($svg_maps) > 0 ) {
  $field['choices'] = $svg_maps;
  }
  return $field;
}

add_filter('acf/load_field/name=amenities_svg_map', 'acf_load_amenities_svg_map_field_choices');

function acf_load_floorplates_svg_layer_field_choices( $field ) {

  if ( get_field('floorplates_svg_map', 'option') ) {
    $floorplates_svg_map = get_field('floorplates_svg_map', 'option');
    $svg_image_map_options = get_option('image-map-pro-wordpress-admin-options');

    $svg_image_map_meta = json_decode(json_encode($svg_image_map_options), true);
    $svg_image_map_meta_saves = $svg_image_map_meta['saves'];

    $svg_maps = array();
    $shapes = array();
    $shapesField = array();

    foreach ( $svg_image_map_meta_saves as $save ) { 

      $decoded_save = sanitize_json_for_save($save);
      $decoded_save = json_decode($decoded_save['json']);

      if (intval($decoded_save->id) === intval($floorplates_svg_map['value'])) {

        $floorplates_svg_levels = $decoded_save->layers->layers_list;

        foreach ( $floorplates_svg_levels as $shape ) {
          $shapes[strval($shape->id)]['id'] = strval($shape->id);
          $shapes[strval($shape->id)]['title'] = $shape->title;
          $shapes[strval($shape->id)]['image_url'] = $shape->image_url;
          $shapes[strval($shape->id)]['image_width'] = $shape->image_width;
          $shapes[strval($shape->id)]['image_height'] = $shape->image_height;
          $shapesField[$shape->id] = $shape->title;
        }
      }
    }
    $field['choices'] = $shapesField;
  }

  return $field;
}

add_filter('acf/load_field/name=floorplates_svg_layer', 'acf_load_floorplates_svg_layer_field_choices');



function acf_load_building_svg_shape_field_choices( $field ) {

  if ( get_field('building_svg_map', 'option') ) {
    $building_svg_map = get_field('building_svg_map', 'option');
    $svg_image_map_options = get_option('image-map-pro-wordpress-admin-options');

    $svg_image_map_meta = json_decode(json_encode($svg_image_map_options), true);
    $svg_image_map_meta_saves = $svg_image_map_meta['saves'];

    $svg_maps = array();
    $shapes = array();
    $shapesField = array();

    foreach ( $svg_image_map_meta_saves as $save ) { 

      $decoded_save = sanitize_json_for_save($save);
      $decoded_save = json_decode($decoded_save['json']);

      if (intval($decoded_save->id) === intval($building_svg_map['value'])) {

        $building_svg_shapes = $decoded_save->spots;
        foreach ( $building_svg_shapes as $shape ) {
          $shapes[strval($shape->id)]['id'] = strval($shape->id);
          $shapes[strval($shape->id)]['title'] = strval($shape->title);
          $shapes[strval($shape->id)]['width'] = strval($shape->width);
          $shapes[strval($shape->id)]['height'] = strval($shape->height);
          $shapesField[strval($shape->id)] = strval($shape->title);
        }

      }
    }

    $field['choices'] = $shapesField;
  }

  return $field;
}

add_filter('acf/load_field/name=building_svg_shape', 'acf_load_building_svg_shape_field_choices');

function acf_load_floorplates_level_field_choices( $field ) {
  $query = new WP_Query(array(
      'post_type' => 'level',
      'post_status' => 'publish',
      'orderby' => 'date',
      'order' => 'DESC',
      'posts_per_page' => -1
  ));
  $level_array = array();
  while ($query->have_posts()) {
      $query->the_post();
      $level_id = get_the_ID();
      $level_title = get_the_title();
      $level_content = apply_filters( 'the_content', get_the_content() );
      $level_permalink = get_the_permalink();
      $level_array[strval($level_id)] = $level_title;
  }
  wp_reset_query();
  // get the field... use the field key
  $field['choices'] = $level_array;
  return $field;
}

add_filter('acf/load_field/name=level', 'acf_load_floorplates_level_field_choices');

function acf_load_floorplate_polygon_layer_field_choices( $field ) {

  if ( get_field('floorplates_svg_map', 'option') ) {
    $floorplates_svg_map = get_field('floorplates_svg_map', 'option');
    $svg_image_map_options = get_option('image-map-pro-wordpress-admin-options');

    $svg_image_map_meta = json_decode(json_encode($svg_image_map_options), true);
    $svg_image_map_meta_saves = $svg_image_map_meta['saves'];

    $svg_maps = array();
    $polygons = array();
    $polygonsField = array();

    foreach ( $svg_image_map_meta_saves as $save ) { 

      $decoded_save = sanitize_json_for_save($save);
      $decoded_save = json_decode($decoded_save['json']);

      if (intval($decoded_save->id) === intval($floorplates_svg_map['value'])) {

        $floorplates_svg_polygons = $decoded_save->spots;
        foreach ( $floorplates_svg_polygons as $polygon ) {
          $polygons[strval($polygon->id)]['id'] = strval($polygon->id);
          $polygons[strval($polygon->id)]['title'] = strval($polygon->title);
          $polygons[strval($polygon->id)]['layerID'] = strval($polygon->title);
          $polygonsField[strval($polygon->id)] = strval($polygon->title);
        }
     }
    }
    $field['choices'] = $polygonsField;
  }

  return $field;
}

add_filter('acf/load_field/name=floorplate_polygon', 'acf_load_floorplate_polygon_layer_field_choices');
