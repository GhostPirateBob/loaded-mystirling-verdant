<?php
/**
 * Understrap enqueue scripts
 *
 * @package mystirling
 */

if ( ! defined( 'ABSPATH' ) ) {
  exit; // Exit if accessed directly.
}



if ( ! function_exists( 'understrap_scripts' ) ) {
  function understrap_scripts() {

    if (function_exists('get_field')) {
      if ( get_field( 'filename', 'option' ) ) {
        $site_slug = strtolower(get_field( 'filename', 'option' ));
      } 
      if ( get_field( 'filename', 'option' ) === false || get_field( 'filename', 'option' ) === null ) {
        $site_slug = strtolower(basename( get_bloginfo('url') ));
      }
    }
    if (!function_exists('get_field')) {
      $site_slug = strtolower(basename( get_bloginfo('url') ));
    }

    if ( !is_admin() ) {
      if (file_exists(get_stylesheet_directory() . '/js/jquery-3.4.1.migrate/jquery.min.js')) {
        wp_deregister_script('jquery');
        wp_deregister_script('jquery-migrate');
        wp_enqueue_script( 'jquery',  get_stylesheet_directory_uri() . '/js/jquery-3.4.1.migrate/jquery.min.js', array(), '3.4.1', true );
      }
    }

    if (file_exists( get_stylesheet_directory() . '/css/theme-' . $site_slug . '.min.css' )) { 
      wp_enqueue_style( 'mystirling-styles', get_stylesheet_directory_uri() . '/css/theme-' . $site_slug . '.min.css', array(), array() );
    }

    if (file_exists( get_stylesheet_directory() . '/js/theme.js' )) { 
      wp_register_script( 'mystirling-scripts', get_template_directory_uri() . '/js/theme.js', array(), '1.0.4', true );

      if ( function_exists('get_field') ) {
        if ( get_field( 'filename', 'option' ) ) {
          $site_slug = strtolower(get_field( 'filename', 'option' ));
        } 
        if ( get_field( 'filename', 'option' ) === false || get_field( 'filename', 'option' ) === null ) {
          $site_slug = strtolower(basename( get_bloginfo('url') ));
        }
      }

      if (!function_exists('get_field')) {
        $site_slug = strtolower(basename( get_bloginfo('url') ));
      }

      if (function_exists('generate_apartment_object')) {
        $floorplateJSON = generate_apartment_object();
        if (isset($floorplateJSON)) {
          if (count($floorplateJSON) > 0) {
            wp_localize_script( 'mystirling-scripts', 'apartmentsObject', [ 'apartments' => $floorplateJSON ]);
          }
        }
      }

      if (function_exists('apartments_level_query')) {
        $levelObject = apartments_level_query();
        if (isset($levelObject)) {
          if (count($levelObject) > 0) {
            wp_localize_script( 'mystirling-scripts', 'levelObject', [ 'levels' => $levelObject ]);
          }
        }
      }
      
      if (function_exists('generate_svg_maps_levels')) {
        $returnArray = generate_svg_maps_levels();
        $svg_saves = $returnArray[0];
        $levels = $returnArray[1];
      }

      if (isset($svg_saves)) {
        if (count($svg_saves) > 0) {
          wp_localize_script( 'mystirling-scripts', 'svg_saves', $svg_saves );
        }
      }
      
      if (isset($levels)) {
        if (count($levels) > 0) {
          wp_localize_script( 'mystirling-scripts', 'levels', $levels );
        }
      }

      wp_enqueue_script( 'mystirling-scripts' );
//      wp_enqueue_script( 'mystirling-scripts', get_template_directory_uri() . '/js/theme.js', array(), array(), true );
    }
  }
} 

add_action( 'wp_enqueue_scripts', 'understrap_scripts' );
