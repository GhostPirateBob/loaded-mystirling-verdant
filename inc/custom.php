<?php

function return_level_array() {
  $level_array = array();
  $query = new WP_Query(array(
    'post_type' => 'level',
    'post_status' => 'publish',
    'orderby' => 'date',
    'order' => 'DESC',
    'posts_per_page' => -1
  ));
  while ( $query->have_posts() ) {
    $query->the_post();
    $level_id = get_the_ID();
    $level_title = get_the_title();
    $level_content = apply_filters( 'the_content', get_the_content() );
    $level_permalink = get_the_permalink();
    $level_array[strval($level_id)] = $level_title;
  }
  wp_reset_query();
  return $level_array;
}

// function return_building_level_array() {
//   $building_level_array = array();
//   $query = new WP_Query(array(
//     'post_type' => 'level',
//     'post_status' => 'publish',
//     'orderby' => 'date',
//     'order' => 'DESC',
//     'posts_per_page' => -1
//   ));
//   while ( $query->have_posts() ) {
//     $query->the_post();
//     $building_level_id = get_the_ID();
//     $building_level_title = get_the_title();
//     $building_level_content = apply_filters( 'the_content', get_the_content() );
//     $building_level_permalink = get_the_permalink();

//     $building_level_array[] = $level_title;
//   }
//   wp_reset_query();
//   return $building_level_array;
// }

function return_amenities_array() {
  $amenities_array = array();
  $query = new WP_Query(array(
    'post_type' => 'level',
    'post_status' => 'publish',
    'orderby' => 'date',
    'order' => 'DESC',
    'posts_per_page' => -1
  ));
  while ( $query->have_posts() ) {
    $query->the_post();
    $amenities_item_id = get_the_ID();
    $amenities_item_title = get_the_title();
    $amenities_item_content = apply_filters( 'the_content', get_the_content() );
    $amenities_item_permalink = get_the_permalink();
    $amenities_item_array[strval($amenities_item_id)] = $amenities_item_title;
  }
  wp_reset_query();
  return $level_array;
}

add_filter('gform_init_scripts_footer', '__return_true');

function gform_cdata_open($content = '') {
  $content = 'document.addEventListener( "DOMContentLoaded", function() { ';
  return $content;
}
add_filter('gform_cdata_open', __NAMESPACE__ . '\\gform_cdata_open');

function gform_cdata_close($content = '') {
  $content = ' }, false );';
  return $content;
}
add_filter('gform_cdata_close', __NAMESPACE__ . '\\gform_cdata_close');

// END Force Gravity Forms to init scripts in the footer

add_action('after_setup_theme', 'register_mystirling_nav_areas');
function register_mystirling_nav_areas() {
  register_nav_menus(array(
    'header_nav'             => 'Project Right Sidebar Navigation',
    'footer_nav'             => 'Project Footer Navigation',
    'sidebar_mystirling_nav' => 'Left Sidebar MyStirling Navigation',
    'footer_mystirling_nav'  => 'Footer MyStirling Navigation',
  ));
}

if (function_exists('acf_add_options_page')) {
  acf_add_options_page(array(
    'page_title' => 'MyStirling Theme Settings',
    'menu_title' => 'Edit Theme',
    'menu_slug'  => 'theme-general-settings',
    'menu_order' => 20,
    'capability' => 'edit_posts',
    'redirect'   => false,
    'icon_url'   => 'https://s3-ap-southeast-2.amazonaws.com/loaded-public/loaded-static/img/theme-edit.png',
  ));
  acf_add_options_sub_page(array(
    'page_title'  => 'Header/Footer Settings',
    'menu_title'  => 'Header & Footer',
    'parent_slug' => 'theme-general-settings',
    'menu_slug'  => 'theme-header-footer-settings'
  ));
}

function register_mystirling_cpts() {

  /**
   * Post Type: Apartments.
   */

  $labels = array(
    "name"          => __("Apartments", "mystirling"),
    "singular_name" => __("Apartment", "mystirling"),
  );

  $args = array(
    "label"                 => __("Apartments", "mystirling"),
    "labels"                => $labels,
    "description"           => "",
    "public"                => true,
    "publicly_queryable"    => true,
    "show_ui"               => true,
    "delete_with_user"      => false,
    "show_in_rest"          => true,
    "rest_base"             => "",
    "rest_controller_class" => "WP_REST_Posts_Controller",
    "has_archive"           => false,
    "show_in_menu"          => true,
    "show_in_nav_menus"     => true,
    "exclude_from_search"   => false,
    "capability_type"       => "post",
    "map_meta_cap"          => true,
    "hierarchical"          => false,
    "rewrite"               => array("slug" => "apartment", "with_front" => true),
    "query_var"             => true,
    "menu_icon"             => "https://s3-ap-southeast-2.amazonaws.com/loaded-public/loaded-static/img/apartments-edit.png",
    "supports"              => array("title", "thumbnail", "custom-fields", "page-attributes", "menu_order"),
  );

  register_post_type("apartment", $args);

  /**
   * Post Type: Levels.
   */

  $labels = array(
    "name"          => __("Levels", "mystirling"),
    "singular_name" => __("Level", "mystirling"),
  );

  $args = array(
    "label"                 => __("Levels", "mystirling"),
    "labels"                => $labels,
    "description"           => "",
    "public"                => true,
    "publicly_queryable"    => true,
    "show_ui"               => true,
    "delete_with_user"      => false,
    "show_in_rest"          => true,
    "rest_base"             => "",
    "rest_controller_class" => "WP_REST_Posts_Controller",
    "has_archive"           => false,
    "show_in_menu"          => true,
    "show_in_nav_menus"     => true,
    "exclude_from_search"   => false,
    "capability_type"       => "post",
    "map_meta_cap"          => true,
    "hierarchical"          => false,
    "rewrite"               => array("slug" => "level", "with_front" => true),
    "query_var"             => true,
    "supports"              => array("title", "custom-fields", "page-attributes", "menu_order"),
    "menu_icon"             => "dashicons-building",
  );

  register_post_type("level", $args);
}

add_action('init', 'register_mystirling_cpts');

function loaded_util_admin_head() {
  if (function_exists('get_field')) {
    if (get_field('bg_pattern_one', 'option')) {
      $bg_pattern_one = get_field('bg_pattern_one', 'option');
    }
    if (get_field('bg_pattern_two', 'option')) {
      $bg_pattern_two = get_field('bg_pattern_two', 'option');
    }
    if (get_field('bg_pattern_one', 'option') || get_field('bg_pattern_two', 'option')) {
      echo '<div id="bgPatterns" ';
    }
    if (get_field('bg_pattern_one', 'option') && ! is_numeric($bg_pattern_one)) {
      if (array_key_exists('url', $bg_pattern_one)) {
        echo 'data-bg-pattern-one="' . $bg_pattern_one['url'] . '" ';
      }
    }
    if (get_field('bg_pattern_two', 'option') && ! is_numeric($bg_pattern_two)) {
      if (array_key_exists('url', $bg_pattern_two)) {
        echo 'data-bg-pattern-two="' . $bg_pattern_two['url'] . '" ';
      }
    }
    if (get_field('bg_pattern_one', 'option') || get_field('bg_pattern_two', 'option')) {
      echo '></div>
  ';
    }
  }
}

add_action('admin_head', 'loaded_util_admin_head');

add_filter('medium-editor-theme', 'medium_editor_color_scheme');

function medium_editor_color_scheme($theme) {
  $theme = 'beagle';
  return $theme;
}

function cc_mime_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');

function fix_svg_thumb_display() {
  echo '
  <style>
    td.media-icon img[src$=".svg"], img[src$=".svg"].attachment-post-thumbnail {
      width: 100% !important;
      height: auto !important;
    }
  </style>
  ';
}
add_action('admin_head', 'fix_svg_thumb_display', 50);

add_filter('acf/settings/save_json', 'acf_json_save_point');

function acf_json_save_point($path) {
  $path = get_stylesheet_directory() . '/acf-json';
  return $path;
}

