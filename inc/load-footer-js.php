<script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/2.1.3/plugins/CSSPlugin.min.js" integrity="sha256-LBjlnpPrM6Aig8LDFc9PJctPHLGUc6RaUvnmXE4hV5Y=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/2.1.3/easing/EasePack.min.js" integrity="sha256-Kmyt+nZHXBP0Dc93zU2XMTyo9Bb94gIYk/2H7knbl6U=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/2.1.3/TweenMax.min.js" integrity="sha256-lPE3wjN2a7ABWHbGz7+MKBJaykyzqCbU96BJWjio86U=" crossorigin="anonymous"></script>
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/2.1.3/TweenLite.min.js" integrity="sha256-VV47uJSoHZUeiBcCs3FcBOQLMn++yeG/zqZvaUkvGZM=" crossorigin="anonymous"></script> -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/2.1.3/jquery.gsap.min.js" integrity="sha256-E4WXVTeClYSTEcSRFMdupBD+rTt2A0jalEcDv355Txw=" crossorigin="anonymous"></script>
<script type="text/javascript">

fitty('.fit-fp-banner', {
  minSize: 28,
  maxSize: 45,
  multiline: false
});

AOS.init({
  disable: '',
  startEvent: 'DOMContentLoaded',
  initClassName: 'aos-init',
  animatedClassName: 'aos-animate',
  useClassNames: false,
  disableMutationObserver: false,
  debounceDelay: 50,
  throttleDelay: 50,
  offset: 50,
  duration: 320,
  easing: 'ease-in',
  once: true,
  mirror: true,
  anchorPlacement: 'top-bottom'
});

if ( jQuery('#sidebar-right-nav').length > 0 ) {
  var ldSidenavRight = jQuery( "#sidebar-right-nav" ).ldSidenav({
    top: 0, 
    gap: 0, 
    zIndex: 1204, 
    align: "right",
    attr: "nav-open",
    selectors: {
      trigger: "#sidebar-right-nav-button"
    },
    sidebar: {
      width: 288
    },
    events: {
      callbacks: {
        animation: {
          freezePage: true
        }
      }
    }
  });
}

if ( jQuery('#sidebar-right-enquire').length > 0 ) {
  var ldSidenavRightEnquire = jQuery( "#sidebar-right-enquire" ).ldSidenav({
    top: 0, 
    gap: 0, 
    zIndex: 1203, 
    align: "right",
    attr: "nav-open",
    selectors: {
      trigger: "#sidebar-right-enquire-button"
    },
    sidebar: {
      width: 1088
    },
    events: {
      callbacks: {
        animation: {
          freezePage: true
        }
      }
    }
  });
}

if ( jQuery('#sidebar-left-enquire').length > 0 ) {
  var ldSidenavLeftEnquire = jQuery( "#sidebar-left-enquire" ).ldSidenav({
    top: 0, 
    gap: 0, 
    zIndex: 1201, 
    align: "left",
    attr: "nav-open",
    selectors: {
      trigger: ".sidebar-left-enquire-button"
    },
    sidebar: {
      width: 908
    },
    events: {
      callbacks: {
        animation: {
          freezePage: true
        }
      }
    }
  });
}

if ( jQuery('#sidebar-left-nav').length > 0 ) {
  var ldSidenavLeft = jQuery( "#sidebar-left-nav" ).ldSidenav({
    top: 0, 
    gap: 0, 
    zIndex: 1202, 
    align: "left",
    attr: "nav-open",
    selectors: {
      trigger: "#sidebar-left-nav-button"
    },
    sidebar: {
      width: 636
    },
    events: {
      callbacks: {
        animation: {
          freezePage: true
        }
      }
    }
  });
}

jQuery( 'a[href*="#"]' )
.not( '[href="#"]' )
.not( '[href="#0"]' )
.click( function( event ) {
  if ( location.pathname.replace( /^\//, '' ) == this.pathname.replace( /^\//, '' ) && location.hostname == this
    .hostname ) {
    var target = jQuery( this.hash );
    target = target.length ? target : jQuery( '[name=' + this.hash.slice( 1 ) + ']' );

    if ( target.length ) {
      event.preventDefault();
      jQuery( 'html, body' )
        .animate( {
          scrollTop: target.offset() - 100
            .top
        }, 1000, function() {
          var jQuerytarget = jQuery( target );
          jQuerytarget.focus();
          if ( jQuerytarget.is( ":focus" ) ) {
            return false;
          }
          else {
            jQuerytarget.attr( 'tabindex', '-1' );
            jQuerytarget.focus();
          };
        } );
    }
  }
} );

</script>
