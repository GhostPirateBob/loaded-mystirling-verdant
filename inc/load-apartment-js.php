<script type="text/javascript">
document.addEventListener('DOMContentLoaded', function(event) {

if ( jQuery('.jarallax').length > 0 && !jQuery('html').hasClass('msie')) {
  jarallax( document.querySelectorAll( '.jarallax' ), {
    speed: .0625
  });
}

if (!jQuery('html').hasClass('msie')) {
  fitty('.fittext', {
    minSize: 20,
    maxSize: 28,
    multiline: true
  });
}

if ( jQuery('.choco-gallery').length > 0 ) {
  jQuery('.choco-gallery').each(function (index, element) {
    jQuery(this).Chocolat({
      imageSelector: '.chocolat-image',
      loop: true
    });
  });
}
var errorFunction = function (error) {
  if (error) {
    console.log('Instagram feed error state');
    console.log(error);
    jQuery('.col-footer-nav-right').addClass('error-state');
    jQuery('.row-footer-social').addClass('error-state');
  }
};
var feed = new Instafeed({
  get: 'user',
  userId: 8496109641,
  clientId: 'e114a05f958a420783271b18c5ccd34d',
  target: 'instagramFeed',
  accessToken: '8496109641.e114a05.061463f0150d4e199802774606aee02e',
  resolution: 'standard_resolution',
  sortBy: 'most-recent',
  limit: 4,
  links: true,
  error: errorFunction,
  template: '<div class="col-24 col-lg-12 col-insta-feed gutters mb-3"> <a class="insta-feed-link" data-aos="fade-up" href="{{link}}" target="_blank" style="background-image: url(\'{{image}}\');"> </a> </div>',
    after: function() {
      console.log('Instagram feed loaded OK');
    }
});
feed.run();
});

<?php if ( is_page('apartments') ) : ?>

function generateApartments( floorName ) {
  jQuery( '.imp-image-backgrounds-container > .imp-shape-background-image' ).each( function( index, element ) {
    var targetNodeID = jQuery( this ).attr( 'data-id' );
    if ( apartmentsObject.apartments.hasOwnProperty( targetNodeID ) && toString(targetNodeID) !== "" ) {
      jQuery( '.hs-poly-svg #' + targetNodeID ).addClass( 'poly-status-' + apartmentsObject.apartments[targetNodeID].status);
      var outputHTML = "<div class='target-node-wrapper' " + 
        "data-apartment-id='" + apartmentsObject.apartments[targetNodeID].id + "' " + 
        "data-apartment-image='" + apartmentsObject.apartments[targetNodeID].image + "' " + 
        "data-apartment-poly-id='" + apartmentsObject.apartments[targetNodeID].polyID + "' " + 
        "data-apartment-poly-title='" + apartmentsObject.apartments[targetNodeID].polyTitle + "' " + 
        "data-apartment-permalink='" + apartmentsObject.apartments[targetNodeID].permalink + "' " + 
        "data-apartment-pdf='" + apartmentsObject.apartments[targetNodeID].pdf + "' " + 
        "data-apartment-level='" + apartmentsObject.apartments[targetNodeID].level + "' " + 
        "data-apartment-number='" + apartmentsObject.apartments[targetNodeID].number + "' " + 
        "data-apartment-type='" + apartmentsObject.apartments[targetNodeID].type + "' " + 
        "data-apartment-beds='" + apartmentsObject.apartments[targetNodeID].beds + "' " + 
        "data-apartment-baths='" + apartmentsObject.apartments[targetNodeID].baths + "' " + 
        "data-apartment-cars='" + apartmentsObject.apartments[targetNodeID].cars + "' " + 
        "data-apartment-study='" + apartmentsObject.apartments[targetNodeID].study + "' " + 
        "data-apartment-internal-architectural='" + apartmentsObject.apartments[targetNodeID].internalArchitecturalArea + "' " + 
        "data-apartment-balcony-architectural='" + apartmentsObject.apartments[targetNodeID].balconyArchitecturalArea + "' " + 
        "data-apartment-internal-strata='" + apartmentsObject.apartments[targetNodeID].internalStrataArea + "' " + 
        "data-apartment-balcony-strata='" + apartmentsObject.apartments[targetNodeID].balconyStrataArea + "' " + 
        "data-apartment-total-area='" + apartmentsObject.apartments[targetNodeID].totalArea + "' " + 
        "data-apartment-total-outdoor-area='" + apartmentsObject.apartments[targetNodeID].totalOutdoorArea + "' " + 
        "data-apartment-mc-bay='" + apartmentsObject.apartments[targetNodeID].mcBay + "' " + 
        "data-apartment-plan='" + apartmentsObject.apartments[targetNodeID].plan + "' " + 
        "data-apartment-aspect='" + apartmentsObject.apartments[targetNodeID].aspect + "' " + 
        "data-apartment-store='" + apartmentsObject.apartments[targetNodeID].store + "' " + 
        "data-apartment-status='" + apartmentsObject.apartments[targetNodeID].status + "' >";
      outputHTML += "</div>";
    }
  });
  jQuery('.btn-group-floorplan-select-level .btn-floorplan-select-level').each(function (index, element) {
    var btnMenuOrder = jQuery(this).attr('data-menu-order');
    var btnFloorTitle = jQuery(this).attr('data-floor-title');
    jQuery('#floorplanLevelMap polygon[data-layer-title="'+btnFloorTitle+'"]').attr('data-menu-order', btnMenuOrder);
  });
}

<?php endif; ?>
document.addEventListener('DOMContentLoaded', function(event) {

<?php if ( get_page_template_slug() === 'page-templates/page-location.php' ) : ?>
jQuery( '.btn-amenities-select-level' ).each( function( index, element ) {
  jQuery( this ).click( function( e ) {
    e.preventDefault();
    var dataFloorNumber = jQuery( this ).attr( 'data-layer-id' );
    var dataFloorTitle = jQuery( this ).attr( 'data-floor-title' );
    if ( !jQuery( this ).hasClass( 'active' ) ) {
      jQuery( '.btn-amenities-select-level' ).removeClass( 'active' );
      jQuery( '.btn-amenities-select-level[data-layer-title="'+dataFloorTitle+'"]' ).addClass( 'active' );
      var tempFloor = jQuery( '#amentitiesImageMap .imp-ui-layers-select option[data-layer-title="'+dataFloorTitle+'"]' ).val();
      jQuery( '#amentitiesImageMap .imp-ui-layers-select' ).val( tempFloor ).change();
    } 
  });
});
jQuery.imageMapProInitialized = function( imageMapName ) {
  console.log( "imageMapName: " + imageMapName );
  if (jQuery('.imp-shape[data-toggle="tooltip"]').length > 0) {
    jQuery('.imp-shape[data-toggle="tooltip"]').tooltip();
  }
};
<?php endif; ?>

<?php if ( get_page_template_slug() === 'page-templates/page-apartments.php' ) : ?>

if ( jQuery('#backToTopFloorplans').length > 0 ) {
  jQuery('#backToTopFloorplans').click(function (e) { 
    e.preventDefault();
    jQuery(this).blur();
    setTimeout( function() {
      jQuery( "#floorplanBuilding" ).get( 0 ).scrollIntoView( { behavior: 'smooth', block: "start", inline: "nearest" } );
    }, 250 );
  });
}
if ( jQuery('#backToTopApartmentInfo').length > 0 ) {
  jQuery('#backToTopApartmentInfo').click(function (e) { 
    e.preventDefault();
    jQuery(this).blur();
    setTimeout( function() {
      jQuery( "#floorplans" ).get( 0 ).scrollIntoView( { behavior: 'smooth', block: "start", inline: "nearest" } );
    }, 250 );
  });
}

jQuery( '.btn-floorplan-select-level' ).each( function( index, element ) {
  var dataFloorID = jQuery( this ).attr( 'data-floor-id' );
  jQuery( this ).mouseenter( function() {
    jQuery('.btn-floorplan-level[data-floor-id="' + dataFloorID + '"]' ).addClass( 'hovering-on' );
  });
  jQuery( this ).mouseleave( function() {
    jQuery('.btn-floorplan-level[data-floor-id="' + dataFloorID + '"]' ).removeClass( 'hovering-on' );
  });
  jQuery( this ).click( function( e ) {
    e.preventDefault();
    jQuery(this).blur();
    var dataPostTitle = jQuery( this ).attr( 'data-post-title' );
    var dataLayerID = jQuery( this ).attr( 'data-layer-id' );
    var dataMenuOrder = jQuery( this ).attr( 'data-menu-order' );

    jQuery('#levelNumber').text( dataPostTitle );

    if ( jQuery( this ).hasClass( 'active' ) === false ) {
      jQuery( '.btn-floorplan-select-level' ).removeClass( 'active' );
      jQuery( '.btn-floorplan-select-level[data-post-title="' + dataPostTitle + '"]' ).addClass( 'active' );
      var tempVal = jQuery( '#floorplans .imp-ui-layers-select option[data-layer-title="'+dataPostTitle+'"]' ).val();
      console.log('setting value to : '+tempVal);
      jQuery('#floorplans .imp-ui-layers-select').val(tempVal).change();
    }
    if ( ! jQuery( '#floorplans' ).hasClass('show') ) {
      jQuery( '#floorplans' ).collapse('show');
      setTimeout( function() {
        jQuery( "#floorplans" ).get( 0 ).scrollIntoView( { behavior: 'smooth', block: "start", inline: "nearest" } );
      }, 1000 );
    } else {
      setTimeout( function() {
        jQuery( "#floorplans" ).get( 0 ).scrollIntoView( { behavior: 'smooth', block: "start", inline: "nearest" } );
      }, 250 );
    }
  });
});

// jQuery( '#floorplans' )
// .on( 'shown.bs.collapse', function() {
//   setTimeout( function() {
//     jQuery( "#floorplans" )
//       .get( 0 ).scrollIntoView( { behavior: 'smooth', block: "start", inline: "nearest" } );
//     }, 1000 );
// } );

jQuery.imageMapProEventHighlightedShape = function( imageMapName, shapeName ) {
  var highlightedNodeID = jQuery( '.hs-poly-svg polygon[data-shape-title="' + shapeName + '"]' )
    .attr( 'id' );
  var highlightedNode = jQuery( '.hs-poly-svg #' + highlightedNodeID );
  var highlightedNodeTitle = jQuery( '.hs-poly-svg #' + highlightedNodeID )
    .attr( 'data-shape-title' );
  var offset = highlightedNode.offset();
  var width = highlightedNode.width();
  var height = highlightedNode.height();
  var centerX = offset.left + width / 2;
  var centerY = offset.top + height / 2;
  var targetBgNode = jQuery( '.imp-image-backgrounds-container > .imp-shape-background-image[data-id="' +
    highlightedNodeID + '"]' );
  targetBgNode.addClass( 'hovering-on' );
  highlightedNode.addClass( 'hovering-on' );
};
jQuery.imageMapProEventUnhighlightedShape = function( imageMapName, shapeName ) {
  var highlightedNodeID = jQuery( '.hs-poly-svg polygon[data-shape-title="' + shapeName + '"]' )
    .attr( 'id' );
  var highlightedNode = jQuery( '.hs-poly-svg #' + highlightedNodeID );
  var targetBgNode = jQuery( '.imp-image-backgrounds-container > .imp-shape-background-image[data-id="' +
    highlightedNodeID + '"]' );
  targetBgNode.removeClass( 'hovering-on' );
  highlightedNode.removeClass( 'hovering-on' );
};
jQuery.imageMapProEventSwitchedFloor = function( imageMapName, floorName ) {
  console.log({imageMapName: imageMapName, floorName: floorName});
  setTimeout(function() {
    jQuery('.imp-main-image').addClass('show');
    generateApartments( floorName );
  }, 50);
};
jQuery.imageMapProEventClickedShape = function( imageMapName, shapeTitle ) {
  var shapeID = jQuery('.imp-shape.imp-shape-poly[data-shape-title="'+shapeTitle+'"]').attr('id');
  console.log({shapeClicked: shapeTitle, shapeId: shapeID});
  jQuery( '.data-apartment-image' )
    .attr( 'src', apartmentsObject.apartments[shapeID].image );
  jQuery( '#data-apartment-type' )
    .text( apartmentsObject.apartments[shapeID].type );
  jQuery( '.data-apartment-number' )
    .html( apartmentsObject.apartments[shapeID].number );
  jQuery( '#data-apartment-beds' )
    .text( apartmentsObject.apartments[shapeID].beds );
  jQuery( '#data-apartment-baths' )
    .text( apartmentsObject.apartments[shapeID].baths );
  jQuery( '#data-apartment-cars' ).text( apartmentsObject.apartments[shapeID].cars );
  if ( apartmentsObject.apartments[shapeID].cars >= 1 ) {
    jQuery('#data-apartment-cars').text( apartmentsObject.apartments[shapeID].cars );
    jQuery('.data-apartment-cars-wrapper').removeClass('d-none');
  }
  if ( !apartmentsObject.apartments[shapeID].cars >= 1 ) {
    jQuery('#data-apartment-cars').text('');
    jQuery('.data-apartment-cars-wrapper').addClass('d-none');
  }
  jQuery( '#data-apartment-total-area' )
    .text( apartmentsObject.apartments[shapeID].totalArea + 'm²' );
  // jQuery( '#data-apartment-mcbays' )
  //   .text( apartmentsObject.apartments[shapeID].mc_bay );
  jQuery( '#data-apartment-internal-architectural-area' )
    .text( apartmentsObject.apartments[shapeID].internalArchitecturalArea + 'm²');
  jQuery( '#data-apartment-balcony-architectural-area' )
    .text( apartmentsObject.apartments[shapeID].balconyArchitecturalArea + 'm²');
  jQuery( '#data-apartment-internal-strata-area' )
    .text( apartmentsObject.apartments[shapeID].internalStrataArea + 'm²');
  jQuery( '#data-apartment-balcony-strata-area' )
    .text( apartmentsObject.apartments[shapeID].balconyStrataArea + 'm²');
    jQuery( '#data-apartment-plan' )
    .text( apartmentsObject.apartments[shapeID].plan );
    jQuery( '#data-apartment-aspect' )
    .text( apartmentsObject.apartments[shapeID].aspect );
    jQuery( '#data-apartment-status' )
    .text( apartmentsObject.apartments[shapeID].status );
    jQuery( '#data-apartment-level' )
    .text( apartmentsObject.apartments[shapeID].level );
    jQuery( '#data-apartment-store' )
    .text( apartmentsObject.apartments[shapeID].store + 'm²');
    jQuery( '#data-apartment-floorplan-url' )
    .attr( 'href', apartmentsObject.apartments[shapeID].pdf);
    jQuery( '#data-apartment-floorplate-url' )
    .attr( 'href', apartmentsObject.apartments[shapeID].levelPDF);
  if ( !jQuery('#apartmentInfo').hasClass('show') ) {
    jQuery('#apartmentInfo').collapse('show');
    setTimeout(function() {
      jQuery( "#apartmentInfo" ).get( 0 ).scrollIntoView({ behavior: 'smooth', block: "start", inline: "nearest" });
    }, 1000);
  } else {
    setTimeout(function() {
      jQuery( "#apartmentInfo" ).get( 0 ).scrollIntoView({ behavior: 'smooth', block: "start", inline: "nearest" });
    }, 250);
  }
};
<?php endif; ?>
});
</script>
