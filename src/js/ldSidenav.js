// Uses CommonJS, AMD or browser globals to create a jQuery plugin. 
( function( factory ) {
  if ( typeof define === "function" && define.amd ) {

    // AMD. Register as an anonymous module. 
    define( [ "jquery" ], factory );
  }
  else if ( typeof module === "object" && module.exports ) {

    // Node/CommonJS 
    module.exports = function( root, jQuery ) {
      if ( jQuery === undefined ) {
        if ( typeof window !== "undefined" ) {
          jQuery = require( "jquery" );
        }
        else {
          jQuery = require( "jquery" )( root );
        }
      }
      factory( jQuery );
      return jQuery;
    };
  }
  else {

    // Browser globals 
    factory( jQuery );
  }
}( function( $ ) {

  var pluginName = "ldSidenav";
  function uniqid(a = "",b = false){
      var c = Date.now()/1000;
      var d = c.toString(16).split(".").join("");
      while(d.length < 14){
          d += "0";
      }
      var e = "";
      if(b){
          e = ".";
          var f = Math.round(Math.random()*100000000);
          e += f;
      }
      return a + d + e;
  }

  $.fn[ pluginName ] = function( options ) {
    var currentID = uniqid();
    // Default settings 
    var ldSidenavConfig = jQuery.extend( true, {
      attr: "ldsidebar",
      top: 0,
      gap: 0,
      zIndex: 1200,
      htmlOverflowX: "hidden",
      bodyOverflowX: "hidden",
      pageOverflowX: "hidden",
      htmlOverflowY: "initial",
      bodyOverflowY: "initial",
      pageOverflowY: "auto",
      uniqueID: currentID,
      quitter: jQuery( "<a>" ).attr( 'data-quitter-id', currentID )
      .attr( 'class', 'ldSidenavQuitter' )
      .attr( 'id', 'quitter-' + currentID )
      .css( 'visibility', 'hidden' )
      .css( 'opacity', '0' )
      .css( 'height', '1px' )
      .css( 'width', '1px' )
      .css( 'position', 'absolute' )
      .css( 'left', '-9999px' )
      .css( 'overflow', 'hidden' )
      .appendTo( "body" ),
      sidebar: {
        width: 320
      },

      animation: {
        duration: 1800,
        easing: "easeOutPower3"
      },

      events: {
        on: {
          animation: {
            open: function() {},
            close: function() {},
            both: function() {}
          }
        },
        callbacks: {
          animation: {
            open: function() {},
            close: function() {},
            both: function() {},
            freezePage: true
          }
        }
      },

      mask: {
        display: true,
        css: {
          backgroundColor: "rgba(0, 0, 0, 0.3)",
        }
      }
    }, options );

    // Keep chainability 
    return this.each( function() {
      var ldStyle, pvtMaskStyle, maskStyle,
        attr = "data-" + ldSidenavConfig.attr,

        // Set anything else than "opened" to "closed" 
        init = ( "opened" === ldSidenavConfig.init ) ? "opened" : "closed",

        // Set the overflow setting to initial 
        htmlOverflowX = ldSidenavConfig.htmlOverflowX ? ldSidenavConfig.htmlOverflowX : jQuery( "html" )
        .css( "overflow-x" ),
        bodyOverflowX = ldSidenavConfig.bodyOverflowX ? ldSidenavConfig.bodyOverflowX : jQuery( "body" )
        .css( "overflow-x" ),
        pageOverflowX = ldSidenavConfig.pageOverflowX ? ldSidenavConfig.pageOverflowX : jQuery( "#page" )
        .css( "overflow-x" ),
        htmlOverflowY = ldSidenavConfig.htmlOverflowY ? ldSidenavConfig.htmlOverflowY : jQuery( "html" )
        .css( "overflow-y" ),
        bodyOverflowY = ldSidenavConfig.bodyOverflowY ? ldSidenavConfig.bodyOverflowY : jQuery( "body" )
        .css( "overflow-y" ),
        pageOverflowY = ldSidenavConfig.pageOverflowY ? ldSidenavConfig.pageOverflowY : jQuery( "#page" )
        .css( "overflow-y" ),
        // Set anything else than "left" to "right" 
        align = ( "left" === ldSidenavConfig.align ) ? "left" : "right",
        duration = ldSidenavConfig.animation.duration,
        durationInSeconds = ldSidenavConfig.animation.duration / 1000,
        gsapDurationInSeconds = 0.75,
        gsapDurationInSecondsLg = 1.25,
        easing = ldSidenavConfig.animation.easing,
        animation = {},
        gsapAnimation = {},
        // Set anything else then true to false 
        scrollCfg = ( true === ldSidenavConfig.events.callbacks.animation.freezePage ) ?
        true :
        false,
        freezePage = function() {
          jQuery( "html" )
            // .css( "overflow-y", "hidden" )
            .css( "overflow-x", "hidden" );
          jQuery( "body" )
            // .css( "overflow-y", "hidden" )
            .css( "overflow-x", "hidden" );
          if ( jQuery( '#page' )
            .length > 0 ) {
            jQuery( "#page" )
              .css( "overflow-y", "hidden" )
              .css( "overflow-x", "hidden" );
            //   .css('margin-right', '-10px !important')
            //   .css('width', 'auto !important')
            //   .css('max-width', 'auto !important');
            // jQuery( "#navbar-right-main" )
            //   .css('right', '10px !important');
          }
        },
        unfreezePage = function() {
          jQuery( "html" )
            .css( "overflow-x", htmlOverflowX )
            .css( "overflow-y", htmlOverflowY );
          jQuery( "body" )
            .css( "overflow-x", bodyOverflowX )
            .css( "overflow-y", bodyOverflowY );
          if ( jQuery( '#page' )
            .length > 0 ) {
            jQuery( "#page" )
              .css( "overflow-x", pageOverflowX )
              .css( "overflow-y", pageOverflowY );
            //   .css('margin-right', '0')
            //   .css('width', '100%')
            //   .css('max-width', '100%');
            // jQuery( "#navbar-right-main" )
            //   .css('right', '0');
          }
        },

        // Sidenav helpers 
        ldSidenav = jQuery( this ),
        setSidenavWidth = function( width ) {
          // Calculate sidebar width 
          if ( ldSidenav.attr( 'id' ) === 'sidebar-right-nav' || ldSidenav.attr( 'id' ) ===
            'sidebar-right-enquire' ) {
            return jQuery( window )
              .width();
          }
          else {
            if ( width < ( ldSidenavConfig.sidebar.width + ldSidenavConfig.gap ) ) {
              return width - ldSidenavConfig.gap;
            }
            else {
              return ldSidenavConfig.sidebar.width;
            }
          }
        },
        sidebarStatus = function() {
          // Check if the sidebar attribute is set to "opened" or "closed" 
          return ldSidenav.attr( attr );
        },
        changeSidenavStatus = function( status ) {
          ldSidenav.attr( attr, status );
        },
        ldSidenavMask = jQuery('#ldSidenavMask').length ? jQuery('#ldSidenavMask') : jQuery( "<div>" )
          .attr( attr, "mask" )
          .attr( 'class', 'ldSidenavMask' )
          .attr( 'id', 'ldSidenavMask' )
          .attr( 'data-sidenav-id', ldSidenav.attr( 'id' ) ),
        createMask = function() {
          // Create mask 
          if ( jQuery( "#ldSidenavMask" )
            .length === 0 ) {
            if ( jQuery( "#page" )
              .length === 1 ) {
              ldSidenavMask.prependTo( "#page" )
                .css( maskStyle );
            }
            else {
              ldSidenavMask.appendTo( "body" )
                .css( maskStyle );
            }
          }
        },
        showMask = function() {
          ldSidenavMask.fadeIn( 450 );
        },
        hideMask = function() {
          ldSidenavMask.fadeOut( 300 );
        },

        ldSidenavTrigger = jQuery( ldSidenavConfig.selectors.trigger ),
        ldSidenavClose = ldSidenavConfig.quitter,

        width = jQuery( window ).width(),

        // Other functions that must be run along the animation 
        events = {

          // Events triggered with the animations 
          on: {
            animation: {
              open: function() {
                jQuery( '.ldSidenavMask' )
                  .each( function( index, element ) {
                    dataSidenavID = jQuery( this )
                      .attr( 'data-sidenav-id' );
                  } );
                showMask();
                changeSidenavStatus( "opened" );

                ldSidenavConfig.events.on.animation.open();
              },
              close: function() {
                hideMask();
                changeSidenavStatus( "closed" );

                ldSidenavConfig.events.on.animation.close();
              },
              both: function() {
                ldSidenavConfig.events.on.animation.both();
              }
            }
          },

          // Events triggered after the animations 
          callbacks: {
            animation: {
              open: function() {
                if ( scrollCfg ) {
                  freezePage();
                }
                ldSidenavConfig.events.callbacks.animation.open();
              },
              close: function() {
                if ( scrollCfg ) {
                  unfreezePage();
                }

                ldSidenavConfig.events.callbacks.animation.close();
              },
              both: function() {
                ldSidenavConfig.events.callbacks.animation.both();
              }
            }
          }
        },

        // Create animations 
        animateOpen = function() {

          var buttonID = ldSidenav.attr( 'id' ) + '-button';
          if ( jQuery( '#' + buttonID )
            .hasClass( 'tcon' ) ) {
            jQuery( '#' + buttonID )
              .addClass( 'tcon-transform' )
          }

          if ( buttonID === 'sidebar-left-enquire-button' ) {
            jQuery( '.enquire-btn-chevron-right' )
              .addClass( 'show' );
          }

          var callbacks = function() {
            events.callbacks.animation.open();
            events.callbacks.animation.both();
          };

          // Define the animation 
          animation[ align ] = 0;
          var gsapOpenComplete = function() {
            events.callbacks.animation.open();
            events.callbacks.animation.both();
          };

          gsapAnimation = {
            [ align ]: 0,
            onComplete: gsapOpenComplete
          };

          if ( ldSidenav.width() > 668 ) {
            TweenLite.to( ldSidenav, gsapDurationInSecondsLg, gsapAnimation );
          }
          else {
            TweenLite.to( ldSidenav, gsapDurationInSeconds, gsapAnimation );
          }

          // ldSidenav.animate( animation, duration, easing, callbacks ); 

          events.on.animation.open();
          events.on.animation.both();
        },
        animateClose = function() {
          // console.log(ldSidenav); 
          var buttonID = ldSidenav.attr( 'id' ) + '-button';

          if ( jQuery( '#' + buttonID )
            .hasClass( 'tcon' ) ) {
            jQuery( '#' + buttonID )
              .removeClass( 'tcon-transform' )
          }

          if ( buttonID === 'sidebar-left-enquire-button' ) {
            jQuery( '.enquire-btn-chevron-right' )
              .removeClass( 'show' );
          }

          if ( ldSidenav[ 0 ][ 'id' ] === 'sidebar-right-nav' && jQuery( window )
            .width() <= 667 ||
            ldSidenav[ 0 ][ 'id' ] === 'sidebar-right-enquire' && jQuery( window )
            .width() <= 1360 ) {
            animation[ align ] = -jQuery( window )
              .width();
          }
          else {
            animation[ align ] = -ldSidenav.width();
          }

          var gsapCloseComplete = function() {
            events.callbacks.animation.close();
            events.callbacks.animation.both();
          };

          if ( ldSidenav.width() > 668 ) {
            TweenLite.to( ldSidenav, gsapDurationInSecondsLg, gsapAnimation );
          }
          else {
            TweenLite.to( ldSidenav, gsapDurationInSeconds, gsapAnimation );
          }

          gsapAnimation = {
            [ align ]: animation[ align ],
            onComplete: gsapCloseComplete
          };
          TweenLite.to( ldSidenav, gsapDurationInSeconds, gsapAnimation );

          // Apply the animation, the options and the callbacks 
          // TweenLite.to(ldSidenav, duration, {animation, easing, callbacks}); 
          // ldSidenav.animate( animation, duration, easing, callbacks ); 

          events.on.animation.close();
          events.on.animation.both();
        };

      // Create the sidebar style 
      ldStyle = {
        position: "fixed",
        top: parseInt( ldSidenavConfig.top ),
        bottom: 0,
        width: setSidenavWidth( width ),
        zIndex: ldSidenavConfig.zIndex
      };

      // Set initial position 
      ldStyle[ align ] = ( "closed" === init ) ? -setSidenavWidth( width ) : 0;

      // freeze page if sidebar is opened 
      if ( scrollCfg && "opened" === init ) {
        freezePage();
      }

      // Apply style to the sidebar 
      ldSidenav.css( ldStyle )
        .attr( attr, init ); // apply init 

      // Create the private mask style 
      pvtMaskStyle = {
        position: "fixed",
        top: parseInt( ldSidenavConfig.top ),
        right: 0,
        bottom: 0,
        left: 0,
        zIndex: 2,
        display: "none"
      };

      // Hide or show the mask according to the chosen init option 
      pvtMaskStyle.display = ( "opened" === init ) ?
        "block" :
        "none";

      // Merge the Mask private and custom style but keep private style unchangeable 
      maskStyle = jQuery.extend( true, pvtMaskStyle, ldSidenavConfig.mask.css );

      // Create Mask if required 
      // Mask is appended to body 
      if ( ldSidenavConfig.mask.display ) {
        createMask();
      }

      // Apply animations 
      ldSidenavTrigger.click( function() {
        switch ( sidebarStatus() ) {
          case "opened":
            animateClose();
            break;
          case "closed":
            animateOpen();
            break;
        }
      } );

      // ldSidenavMask.click( animateClose );
      ldSidenavMask.on( "click", ldSidenavClose, animateClose );
      // ldSidenav.on( "click", ldSidenav, animateClose );

      // Make the sidebar responsive 
      function throttledWidthCheck() {
        var width = jQuery( window )
          .width();
        ldSidenav.css( "width", setSidenavWidth( width ) );
        if ( "closed" === sidebarStatus() ) {
          if ( ldSidenav[ 0 ][ 'id' ] === 'sidebar-right-nav' && jQuery( window )
            .width() <= 667 ||
            ldSidenav[ 0 ][ 'id' ] === 'sidebar-right-enquire' && jQuery( window )
            .width() <= 1360 ) {
            ldSidenav.css( align, -jQuery( window )
              .width() );
          }
          else {
            ldSidenav.css( align, -ldSidenav.width() );
          }
        }
      }
      window.addEventListener( 'resize', _.throttle( throttledWidthCheck, 16 ), false );
    } );
  }
} ) );
