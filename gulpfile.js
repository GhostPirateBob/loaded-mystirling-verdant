var fs = require( 'fs' );
var gulp = require( 'gulp' );
var gutil = require( 'gulp-util' );
var del = require( 'del' );
var flatmap = require( 'gulp-flatmap' );
var gulpif = require( 'gulp-if' );
var exec = require( 'child_process' ).exec;
var notify = require( 'gulp-notify' );
var argv = require( 'yargs' ).argv;
var production = false;
var build = false;
const { watch } = require('gulp');
var tap = require( 'gulp-tap' );
var path = require( 'path' );
var readSync = require( 'read-file-relative' ).readSync;
var header = require( 'gulp-header' );
var cssnano = require( 'gulp-cssnano' );
var rename = require( 'gulp-rename' );
var cfg = require( './gulpconfig.json' );

var concat = require( 'gulp-concat' );
var uglify = require( 'gulp-uglify' );
// var sourcemaps = require( 'gulp-sourcemaps' );
var del = require( 'del' );

// sass
var sass = require( 'gulp-sass' );
var postcss = require( 'gulp-postcss' );
var autoprefixer = require( 'gulp-autoprefixer' );
var sourcemaps = require( 'gulp-sourcemaps' );

// scripts
var source = require('vinyl-source-stream');
var buffer = require('vinyl-buffer');
var browserify = require('browserify');
var watchify = require('watchify');
var babel = require('babelify');

var beep = function() {
  var os = require( 'os' );
  var file = 'gulp/error.wav';
  if ( os.platform() === 'linux' ) {
    exec( "aplay " + file );
  }
  else {
    console.log( "afplay " + file );
    exec( "afplay " + file );
  }
};

var handleError = function( task ) {
  return function( err ) {
    beep();
    notify.onError( {
      message: task + ' failed, check the logs..',
      sound: false
    } )( err );
    gutil.log( gutil.colors.bgRed( task + ' error:' ), gutil.colors.red( err ) );
  };
};


var tasks = {
  clean: function( cb ) {
    del( [ 'css/*' ], cb );
  },
  sassVerdant: function() {
    return gulp.src( './sass/theme.scss' )
      .pipe( header( readSync( cfg.colors.verdant ).toString() ) )
      .pipe( tap( function( file, t ) {
        console.log( 'FILEPATH: ' + file.path );
        return;
      } ) )
      .pipe( ( sourcemaps.init() ) )
      .pipe(autoprefixer())
      .pipe( sass( {
        outputStyle: "expanded",
        errLogToConsole: true
      } ) )
      .on( 'error', handleError( 'SASS' ) )
      .pipe( rename({ suffix: "-" + cfg.sites.verdant }) )
      .pipe( cssnano( {
        autoprefixer: {
          browsers: [ '> 1%', 'last 2 versions' ],
          add: true
        }
      } ) )
      .pipe( gulp.dest( 'css/' ) )
      .pipe( sourcemaps.write( {
        'includeContent': false,
        'sourceRoot': './sass'
      } ) )
      .pipe( rename({ suffix: ".min" }) )
      .pipe( gulp.dest( 'css/' ) );
  },
  sassBarque: function() {
    return gulp.src( 'sass/theme.scss' )
      .pipe( header( readSync( cfg.colors.barque ).toString() ) )
      .pipe( tap( function( file, t ) {
        console.log( 'FILEPATH: ' + file.path );
        return;
      } ) )
      .pipe( ( sourcemaps.init() ) )
      .pipe(autoprefixer())
      .pipe( sass( {
        outputStyle: "expanded",
        errLogToConsole: true
      } ) )
      .on( 'error', handleError( 'SASS' ) )
      .pipe( rename({ suffix: "-" + cfg.sites.barque }) )
      .pipe( cssnano( {
        autoprefixer: {
          browsers: [ '> 1%', 'last 2 versions' ],
          add: true
        }
      } ) )
      .pipe( gulp.dest( 'css/' ) )
      .pipe( sourcemaps.write( {
        'includeContent': false,
        'sourceRoot': './sass/'
      } ) )
      .pipe( rename({ suffix: ".min" }) )
      .pipe( gulp.dest( 'css/' ) );
  },
  scripts: function() {
    var scripts = [
      // './node_modules/promise-polyfill/dist/polyfill.min.js',
      './node_modules/mutationobserver-shim/dist/mutationobserver.min.js',
      './src/js/lodash.min.js',
      './src/js/ldSidenav.js',
      // './src/js/ldSidenavTranspiled.js',
      './src/js/bootstrap4/bootstrap.bundle.js',
      './src/js/popper.js',
      './src/js/bs4-material/bootstrap-material-design.js',
      './src/js/jarallax/jarallax.js',
      './src/js/jarallax/jarallax-element.js',
      // './src/js/jarallax/jarallax-video.js',
      // './src/js/justified-layout.js',
      // './src/js/fjGallery.js',
      './src/js/fitty.min.js',
      './src/js/aos.js',
      './src/js/skip-link-focus-fix.js',
      './src/js/transformicons.js',
      './node_modules/instafeed.js/instafeed.min.js',
      './node_modules/chocolat/dist/js/jquery.chocolat.min.js',
      './src/js/submodules/squares/js/sqares.js',
      './src/js/svg-image-maps/image-map-pro-defaults.js',
      './src/js/svg-image-maps/image-map-pro.js',
      './src/js/svg-image-maps.js'
    ];
    gulp.src( scripts )
      .pipe( concat( 'theme.min.js' ) )
      .pipe( sourcemaps.init() )
      // .pipe( uglify() )
      .pipe( sourcemaps.write() )
      .pipe( gulp.dest( './js/' ) );
  
    gulp.src( scripts )
      .pipe( concat( 'theme.js' ) )
      .pipe( gulp.dest( './js/' ) );
  }
};

// --------------------------
// CUSTOMS TASKS
// --------------------------

gulp.task( 'clean', tasks.clean );
// for production we require the clean method on every individual task

var req = build ? [ 'clean' ] : [];

// individual tasks
gulp.task( 'sassVerdant', req, tasks.sassVerdant );
gulp.task( 'sassBarque', req, tasks.sassBarque );
gulp.task( 'scripts', req, tasks.scripts );
// --------------------------
// DEV/WATCH TASK
// --------------------------

gulp.task( 'watch', function() {
  gulp.watch( ['sass/**/*.scss', '!./node_modules/'], [ 'clean', 'sassVerdant', 'sassBarque' ], function(cb) { cb(); });
  gulp.watch( ['./src/js/**/*.js', '!./node_modules/', '!js/theme.js', '!js/theme.min.js'], ['scripts'], function(cb) { cb(); } );
  gutil.log( gutil.colors.bgGreen( 'Watching for changes...' ) );
} );

// build task
gulp.task( 'build', [
  'clean',
  'sassVerdant',
  'sassBarque'
] );

gulp.task( 'default', [ 'watch' ] );
