<?php
/**
 * Template Name: Contact Page Template
 *
 * @package mystirling
 */

if (!defined('ABSPATH')) {
  exit; // Exit if accessed directly.
}

get_header();
?>

<div class="wrapper" id="page-wrapper">
  <div class="container-fluid" id="content" tabindex="-1">
    <div class="row row-entry-content">
      <div class="col-md-48 content-area entry-content" id="primary">
        <?php require( __DIR__ . '/requires.php' ); ?>
      </div>
    </div>
  </div>
</div> 

<?php get_footer();
