<?php

$siteURL        = get_site_url();
$themeURL       = get_stylesheet_directory_uri();
$themePath      = get_stylesheet_directory();
$siteTitle      = get_bloginfo( 'Title' );
$container      = get_theme_mod('understrap_container_type');
$siteURL        = get_site_url();
$themeURL       = get_stylesheet_directory_uri();
$imageURL       = "https://cdn.loadedcloud.com.au";

while (have_posts()) {
  the_post();
  $content = apply_filters('the_content', get_the_content());
  if (have_rows('flexible_content')) {
    while ( the_flexible_field('flexible_content') ) {

      $current_row_layout = get_row_layout();
      $current_row_block_name = $current_row_layout;
      $current_row_block_name = str_replace('-', '_', $current_row_block_name);
      $flexible_content = get_field('flexible_content');
      $row_index = get_row_index();

      if ( get_field('body_bg', 'option') ) {
          $body_bg = get_field('body_bg', 'option');
      }

      if (have_rows( $current_row_block_name )) {
        while (have_rows( $current_row_block_name )) {
          the_row();
          $wrapper_classes = array();
          $anchor_tag = '';
          if ( $current_row_layout === 'text-content-group' ) {
            $wrapper_classes[] = 'text-content-block-wrapper';
            $wrapper_classes[] = 'headline-wrapper';
          }
          if ( $current_row_layout === 'hero-content-block' ) {
            $wrapper_classes[] = 'container-fluid';
            $wrapper_classes[] = 'container-banner-content';
          }
          if ( $current_row_layout === 'content-offset-image-block' ) {
            $wrapper_classes[] = 'bg-gradient-light';
            $wrapper_classes[] = 'content-offset-image-block-wrapper';
          }
          if ( $current_row_layout === 'video-block' ) {
            $wrapper_classes[] = 'video-block-wrapper';
          }
          if ( $current_row_layout === 'full-width-image-block' ) {
            $wrapper_classes[] = 'full-width-image-block-wrapper';
            $wrapper_classes[] = 'maxheight-86';
          }
          if ( $current_row_layout === 'gallery-block' ) {
            $wrapper_classes[] = 'wrapper-gallery';
          }

          // If flexi order is 1 (at the top of the page)
          if (array_key_exists($row_index-2, $flexible_content)) {
            if ( $flexible_content[$row_index-2]['acf_fc_layout'] === 'content-offset-image-block' ) {
              $wrapper_classes[] = 'content-offset-after';
            }
          }

          if (array_key_exists($row_index-1, $flexible_content)) {
            if ( $flexible_content[$row_index-1]['acf_fc_layout'] === 'content-offset-image-block' ) {
              $wrapper_classes[] = 'img-offset-after';
            }
          }

          // If flexi content block after this one is 'icon-block'
          if (array_key_exists($row_index, $flexible_content)) {
            if ( $flexible_content[$row_index]['acf_fc_layout'] === 'icon-block' ) {
              $wrapper_classes[] = 'pb-0';
            }
          }

          $previous_bg = 'none';
          $current_bg = 'none';
          $next_bg = 'none';
          $before_previous_bg = 'none';
          
          if ( !isset($wrapper_bool) ) {
            $wrapper_bool = false;
          }

          if (array_key_exists($row_index-3, $flexible_content)) {
            $before_previous_layout = str_replace ('-', '_', $flexible_content[$row_index-3]['acf_fc_layout'] );
           if (array_key_exists('section_container', $flexible_content[$row_index-3][$before_previous_layout])) {
            $before_previous_bg = $flexible_content[$row_index-3][$before_previous_layout]['section_container']['background_texture'];
           }
          }
          if (array_key_exists($row_index-2, $flexible_content)) {
           $previous_layout = str_replace ('-', '_', $flexible_content[$row_index-2]['acf_fc_layout'] );
           if (array_key_exists('section_container', $flexible_content[$row_index-2][$previous_layout])) {
            $previous_bg = $flexible_content[$row_index-2][$previous_layout]['section_container']['background_texture'];
           }
          }
          if (array_key_exists($row_index-1, $flexible_content)) {
           $current_layout = str_replace ('-', '_', $flexible_content[$row_index-1]['acf_fc_layout'] );
           if (array_key_exists('section_container', $flexible_content[$row_index-1][$current_layout])) {
            $current_bg = $flexible_content[$row_index-1][$current_layout]['section_container']['background_texture'];
           }
          }
          if (array_key_exists($row_index, $flexible_content)) {
           $next_layout = str_replace ('-', '_', $flexible_content[$row_index]['acf_fc_layout'] );
           if (array_key_exists('section_container', $flexible_content[$row_index][$next_layout])) {
            $next_bg = $flexible_content[$row_index][$next_layout]['section_container']['background_texture'];
           }
          }
          if (array_key_exists($row_index+1, $flexible_content)) {
            $upcoming_layout = str_replace ('-', '_', $flexible_content[$row_index+1]['acf_fc_layout'] );
           }

          if ( $current_bg !== 'none' && $next_bg === $current_bg ) { 
            $wrapper_bool = true;
          }

          if ( $row_index === 1 ) {
            if ( $current_row_layout !== 'full-width-image-block' && 
              $current_row_layout !== 'hero-content-block' ) {
                $wrapper_classes[] = 'pt-subpage-first';
              }
          }

          if ( have_rows( 'section_container' ) ) {
            while ( have_rows( 'section_container' ) ) {
              the_row(); 
              if ( get_sub_field('anchor_tag') ) {
                $anchor_tag = get_sub_field('anchor_tag');
              }
              if ( get_sub_field( 'background_color' ) && 
                    get_sub_field( 'background_color' ) !== 'none' &&
                    get_sub_field( 'custom_background' ) !== 'no' ) {
                      if ( get_field('body_bg', 'option') ) {
                        $body_bg = get_field('body_bg', 'option');
                    }
                $wrapper_classes[] = get_sub_field( 'background_color' );
              }
              if ( $current_row_layout !== 'hero-content-block' ) {
                if ( get_sub_field( 'background_color' ) && 
                      get_sub_field( 'custom_background' ) !== 'yes' ||
                    get_sub_field( 'background_color' ) && 
                      get_sub_field( 'custom_background' ) === 'yes' &&  
                      get_sub_field( 'background_color' ) === 'none' ) {
                  if ( get_field('body_bg', 'option') ) {
                    $body_bg = get_field('body_bg', 'option');
                    $wrapper_classes[] = 'bg-' . $body_bg;
                  }
                }
              }
              if ( $wrapper_bool === false ) {
                if ( get_sub_field( 'background_texture' ) && get_sub_field( 'background_texture' ) !== 'none' ) {
                  $wrapper_classes[] = get_sub_field( 'background_texture' );
                  $wrapper_classes[] = 'bg-texture-single';
                }
              }
              if ( get_sub_field( 'padding_top' ) && get_sub_field( 'padding_top') !== 'none' ) {
                $padding_top = get_sub_field( 'padding_top');
                if ( $padding_top === 'sm' ) {
                  $wrapper_classes[] = 'pt-3';
                }
                if ( $padding_top === 'md' ) {
                  $wrapper_classes[] = 'pt-5';
                }
                if ( $padding_top === 'lg' ) {
                  $wrapper_classes[] = 'pt-6';
                }
                if ( $padding_top === 'xl' ) {
                  $wrapper_classes[] = 'pt-6';
                  $wrapper_classes[] = 'pt-lg-7';
                }
              }
              if ( get_sub_field( 'padding_bot' ) && get_sub_field( 'padding_bot') !== 'none' ) {
                $padding_top = get_sub_field( 'padding_bot');
                if ( $padding_top === 'sm' ) {
                  $wrapper_classes[] = 'pb-3';
                }
                if ( $padding_top === 'md' ) {
                  $wrapper_classes[] = 'pb-5';
                }
                if ( $padding_top === 'lg' ) {
                  $wrapper_classes[] = 'pb-6';
                }
                if ( $padding_top === 'xl' ) {
                  $wrapper_classes[] = 'pb-6';
                  $wrapper_classes[] = 'pb-lg-7';
                }
              }
              if ( get_sub_field( 'hide_section' ) === 'yes' ) {
                $wrapper_classes[] = 'd-none';
              }
            }
          }

    if ( $before_previous_bg !== 'none' && 
      $before_previous_bg === $previous_bg && 
      $current_bg !== $previous_bg && 
      $wrapper_bool === true ) { 
      $wrapper_bool = false; ?>
</div>
<?php }
          if ( $current_bg !== 'none' && 
                $next_bg === $current_bg &&
                $previous_bg !== $current_bg &&
                $wrapper_bool = true ) { ?>
<div class="wrapper bg-wrapper <?php echo $current_bg; ?>">
          <?php } ?>
          <?php if ( get_sub_field( 'image_map_type' ) ) {
            $svg_image_map_type = "";
            $svg_image_map_type = get_sub_field( 'image_map_type' );
            if ( $current_row_block_name === 'svg_image_map_block' && $svg_image_map_type === 'floorplates' ) { ?>
<div class="wrapper wrapper-floorplans">
  <div id="floorplans" class="collapse collapse-floorplans">
            <?php }
          } ?>
                <div <?php if ( strlen($anchor_tag) > 0 ) { ?> id="<?php echo $anchor_tag; ?>" <?php } ?> class="<?php
    $wrapper_total = count($wrapper_classes);
    $wrapper_count = 1;
    foreach ( $wrapper_classes as $wrapper_class ) { 
      if ( $wrapper_count === $wrapper_total ) {
        echo $wrapper_class;
      } else {
        echo $wrapper_class . ' ';
      }
      $wrapper_count++;
    }
  ?>" <?php if ( strlen($anchor_tag) > 0 ) { ?> name="<?php echo $anchor_tag; ?>" <?php } ?>>

    <p class="text-muted py-1 px-3 m-0 row-info">
      Row Layout Order: <?php echo $row_index; ?><br>
      Template File: <?php echo basename(__FILE__, '.php'); ?><br>
      Layout File : <?php echo 'flexible-content/' . $current_row_layout . '.php'; ?><br>
      Block Name : <?php echo $current_row_block_name; ?>
    </p>
    <p class="text-muted py-1 px-3 m-0 row-info row-info-next">
      Before Previous: : <?php echo $before_previous_bg; ?><br>
      Previous Background: <?php echo $previous_bg ?><br>
      Current Background : <?php echo $current_bg; ?><br>
      Next Background : <?php echo $next_bg; ?>
    </p>
<?php
      require( get_stylesheet_directory() . '/flexible-content/' . $current_row_layout .'.php' );
        }
?>
  <?php if ( get_sub_field( 'image_map_type' ) ) {
            $svg_image_map_type = "";
            $svg_image_map_type = get_sub_field( 'image_map_type' );
            if ( $current_row_block_name === 'svg_image_map_block' && $svg_image_map_type === 'floorplates' ) {  ?>
    </div>
  <?php }
    } ?>
  </div>

<?php
      }
    }
  }
}
