<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package mystirling
 */
if ( ! defined( 'ABSPATH' ) ) {
  exit; // Exit if accessed directly.
}
global $post;

$siteURL = get_bloginfo('url');
$themeURL  = get_stylesheet_directory_uri();
$siteTitle = get_bloginfo('title');
$themePath = get_stylesheet_directory();

if ( get_field('follow_us_contact_form', 'option') ) {
  $follow_us_form = get_field('follow_us_contact_form', 'option');
  $follow_us_form_title = $follow_us_form['title'];
  $follow_us_form_id = RGFormsModel::get_form_id( $follow_us_form_title ); 
}

$next_page_block = get_field('next_page_block', $post->ID);
$next_page_block_image = get_field('next_page_block_image', $post->ID);
$next_page_block_next_page = get_field('next_page_block_next_page', $post->ID);
if ( $next_page_block && $next_page_block_image && $next_page_block_next_page ) {
  $next_page_block_image_url = wp_get_attachment_image_src($next_page_block_image['id'], 'next-page-image', true );
  $next_page_block_image_url = $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'] . $next_page_block_image_url[0];
  $next_page_block_next_page_post_title = $next_page_block_next_page->post_title;
  $next_page_block_next_page_url = get_permalink($next_page_block_next_page->ID);
  if ( get_field('next_page_block_custom_title', $post->ID) === true && get_field('next_page_block_next_page_title_custom', $post->ID) ) {
    $next_page_block_heading = get_field('next_page_block_next_page_title_custom', $post->ID);
  } else {
    $next_page_block_heading = $next_page_block_next_page_post_title;
  }
}

$flexible_fields = false;
$svg_fields = false;

if ( get_field( 'flexible_content', $post->ID ) ) {
  $flexible_fields = get_field( 'flexible_content', $post->ID );
  $i = 0;
  foreach ( $flexible_fields as $field_group ) {
    if ( $field_group['acf_fc_layout'] === 'svg-image-map-block' ) {
      $svg_fields = true;
      $svg_array[$i]['image_map_type'] = $field_group['svg_image_map_block']['image_map_type'];
      $svg_array[$i]['download_pdf_package'] = $field_group['svg_image_map_block']['download_pdf_package'];
      $i++;
    }
  }
}
?>
<div class="wrapper container-fluid" id="wrapper-footer">
  <?php if ( $next_page_block && $next_page_block_image && $next_page_block_next_page ) { ?>
  <?php 

    $bg_pattern_one = get_field('bg_pattern_one', 'option');
    $bg_pattern = $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['SERVER_NAME'] . $bg_pattern_one['url'];
    $bg_width = $bg_pattern_one['width'] / 2;
    $bg_height = $bg_pattern_one['height'] / 2;
  ?>

<style type="text/css">
.link-next-page::before {
  background-image: url('<?php echo $bg_pattern; ?>') !important;
  background-size: <?php echo $bg_width; ?>px <?php echo $bg_height; ?>px;
  background-blend-mode: multiply;
  <?php if ( $bg_width > 550 && $bg_height > 276 ) { ?>
  background-position: <?php echo (mt_rand(550,$bg_width)); ?>px <?php echo( mt_rand(276,$bg_height) ); ?>px;
  <?php } else { ?>
  background-position: center;
  <?php } ?>
}
</style>

  <div class="wrapper next-page-wrapper container-fluid bg-next-page">
    <div class="container" id="np-block">
      <div class="row row-next-page align-items-center justify-content-center w-100">
        <div class="col-48 col-xl-20 col-next-page-one col-next-page d-flex align-self-center" data-aos="fade-right">
          <a href="<?php echo $next_page_block_next_page_url; ?>" class="link-next-page row bg-next-page-button w-100">
            <div class="col-48 col-next-page-inner col-next-page-inner-text px-2 py-5 d-flex flex-column align-items center">
              <div class="w-100 mb-0 next-page-link d-flex flex-row justify-content-evenly align-items-center">
                <div class="next-page-text-wrapper">
                  <h5 class="next-page-text text-uppercase d-block mt-3 mb-4 w-100"> Next Page </h5>
                  <h2 class="nextpage-heading mb-2 w-100"> <?php echo $next_page_block_heading; ?></h2>
                </div>
                <button class="btn btn-lg btn-outline-light btn-chevron-right-xl btn-chevron-right mb-0" type="button">
                  <?php echo file_get_contents( $themePath . '/img/chevron.svg'); ?>
                </button>
              </div>
            </div>
          </a>
        </div>

        <div class="col-48 col-xl-28 col-next-page-two col-next-page-img" data-aos="fade-left">
          <span class="deco-img-frame deco-img-frame-bottom"></span>
          <span class="deco-img-frame deco-img-frame-right"></span>
          <img class="img-fluid img-next-page" src="<?php echo $next_page_block_image_url; ?>" />
        </div>
        <div class="col-48 col-lg-4 col-next-page-spacer"></div>
      </div>
    </div>
  </div>

<?php } ?>

  <div class="container-fluid">
  <?php if (get_field('social_instagram_url', 'option') && get_field('social_media_instagram_user', 'option')) { ?>
    <div class="row row-footer-heading bg-footer pt-6 pt-lg-7 pb-5 pb-lg-0 internal-gutters">
      <div class="container">
        <div class="row" data-aos="fade-up">
          <div class="col-48">
            <h3 class="mb-5 gutters">
              <i class="fa fa-instagram text-primary"></i>
              <span class="vertical-sep text-white"></span>
              <span class="text-white"> Follow us at </span>
              <a href="<?php echo strip_tags(get_field('social_instagram_url', 'option')); ?>" target="_blank" 
                 rel="nofollow" class="text-white"> <?php echo strip_tags(get_field('social_media_instagram_user', 'option'), '<span>'); ?> </a>
            </h3>
          </div>
        </div>
      </div>
    </div>
    <div class="row row-footer-social bg-footer pt-0 pb-5 pb-lg-0 internal-gutters">
      <div class="container">
        <div class="row">
          <div class="col-48">
            <div id="instagramFeed" class="row row-insta-feed w-100"></div>
          </div>
        </div>
      </div>
    </div>
  <?php } ?>
    <div class="row row-footer-nav-area bg-footer pt-5 pb-0 pt-lg-0">
      <div class="container">
        <div class="row">
          <div class="col-48 col-xl-14 gutters col-footer-nav col-footer-nav-left px-5 py-4 pt-sm-0 pt-lg-4">
            <div class="footer-img-address-wrapper w-100">  
              <div class="footer-img-wrapper img-logo-bot d-block mt-4 mb-4" data-aos="fade-right">
                <?php if ( get_field('logo', 'option') ) {
                  $logo = get_field('logo', 'option');
                  $logo = $logo['url']; ?>
                <img  class="optionsLogo mb-5" src="<?php echo $logo; ?>" />
                <?php } ?>
                <h5 class="text-white mb-4 footer-wrapper-text">
                <?php echo strip_tags(get_field('address_one', 'option'), '<span>'); ?>
                <?php if (get_field('address_two', 'option')) { ?>
                  <br><?php echo strip_tags(get_field( 'address_two', 'option' ), '<span>'); ?>
                <?php } ?>
                </h5>
              </div>
            </div>
            <div class="footer-contact-wrapper d-block w-100 mt-4 mb-4" data-aos="fade-right">
              <h5 class="text-primary text-uppercase mb-4">
              <?php if (get_field('sales_title', 'option')) { ?>
                <?php echo strip_tags(get_field( 'sales_title', 'option' ), '<span>'); ?>
              <?php } ?>
              </h5>
              <?php if (get_field('phone', 'option') && get_field('phone_dial', 'option')) { ?>
              <h4 class="mb-1"><a href="tel:<?php echo strip_tags(get_field('phone_dial', 'option'), '<span>'); ?>" class="text-white">
                <?php echo strip_tags(get_field('phone', 'option'), '<span>'); ?></a>
              </h4>
              <?php } ?>
              <?php if (get_field('email', 'option')) { ?>
              <h4 class="mb-0"><a href="mailto:<?php echo strip_tags(get_field('email', 'option'), '<span>'); ?>" class="text-white">
                <small> <?php echo strip_tags(get_field('email', 'option'), '<span>'); ?></small>
              </a></h4>
              <?php } ?>
            </div>
            <?php if (get_field('opening_title', 'option')) { ?>
            <div class="footer-address-wrapper d-block w-100 mt-4 mb-4 pr-xl-5" data-aos="fade-right">
              <h5 class="text-primary text-uppercase"> 
                <?php echo strip_tags(get_field( 'opening_title', 'option' ), '<span>'); ?>
              </h5>
            </div>
            <?php } ?>
            <?php if ( have_rows( 'open_hours', 'option' ) ) { ?>
            <div class="footer-opening-wrapper d-block w-100 " data-aos="fade-right">
              <?php while ( have_rows( 'open_hours', 'option' ) ) {
                the_row(); ?>
              <h4 class="text-white"><small> 
              <?php if ( get_sub_field( 'text' )) { ?> 
                <?php echo strip_tags(get_sub_field( 'text' ), '<span>'); ?> 
              <?php } ?>
              <?php if ( get_sub_field( 'start_time' )) { ?> 
                <?php echo strip_tags(get_sub_field( 'start_time'), '<span>');?> 
              <?php } ?>
              <?php if ( get_sub_field( 'end_time' )) { ?> 
                <?php echo strip_tags(get_sub_field( 'end_time' ), '<span>'); ?>
              <?php } ?>
              </small></h4>
              <?php } ?>
            <?php } ?>
            
            <?php if ( get_field( 'google_map_view_on_google_maps_link', 'option' ) ) { ?>
              <?php if ( get_field('footer_button_hover_color', 'option') && get_field('footer_button_hover_color', 'option') === 'secondary' ) { ?>
              <a href="<?php the_field( 'google_map_view_on_google_maps_link', 'option' ); ?>" target="_blank" class="btn btn-link btn-arrow-right btn-arrow-right-sm btn-arrow-right-secondary">
              <?php } else { ?>
              <a href="<?php the_field( 'google_map_view_on_google_maps_link', 'option' ); ?>" target="_blank" class="btn btn-link btn-arrow-right btn-arrow-right-sm btn-arrow-right-secondary">
              <?php } ?>
              <span class="btn-arrow-text"> View on Google Maps </span>
              <?php echo file_get_contents($themePath . '/img/arrow-right.svg'); ?> 
            </a>
            <?php } ?>

            </div>
          </div>
          
          <div id="footer-nav-right" class="col-48 col-xl-30 gutters col-footer-nav col-footer-nav-right pt-5 px-5 bg-white text-black">
            <div class="row row-footer-content" data-aos="fade-left">
              <div class="col-48 gutters">
                <?php echo file_get_contents( $themePath . '/img/logo-stirling.svg'); ?>
              </div>
              <?php if ( get_field( 'follow_us_heading', 'option' ) ) { ?>
              <div class="col-48 gutters" data-aos="fade-left">
                <h4 class="footer-subscribe text-black mt-5 mb-5"> <?php the_field('follow_us_heading', 'option'); ?> </h4>
              </div>
              <?php } ?>
              <?php if ( get_field( 'follow_us_contact_form', 'option' ) && $follow_us_form_id > 0 ) { ?>
                <div class="col-48 internal-gutters mb-5" data-aos="fade-left">
                  <?php gravity_form( $follow_us_form_id, false, false, false, '', true, ( $follow_us_form_id * 100 ) ); ?>
                </div>
              <?php } ?>
              </div>
              <div class="col-48 internal-gutters" data-aos="fade-left">
                <div class="row row-footer-multisite-nav">
                  <div class="col-48 col-md-24 col-lg-16 col-xl-15 gutters">
                    <span class="footer-multisite-link mb-4">
                    <?php if ( get_field('short_name', 'option') ) {
                      echo get_field('short_name', 'option');
                    } else { 
                      echo get_bloginfo('title');
                    } ?> </span>
<?php wp_nav_menu(
  array(
    'menu'            => 'footer_nav',
    'theme_location'  => 'footer_nav',
    'menu_id'         => 'footer-menu',
    'fallback_cb'     => false,
    'container_class' => '',
    'container'       => '',
    'container_id'    => '',
    'menu_class'      => 'list-unstyled footer-nav-project navbar-light',
    'depth'           => 1,
    'walker'          => new NavWalker(),
    'echo'            => true,
    'before'          => '',
    'after'           => '',
    'link_before'     => '',
    'link_after'      => ''
  )
); ?>
                  </div>
                  <div class="col-48 col-md-24 col-lg-16 col-xl-15 gutters">
                    <span class="footer-multisite-link mb-4"> Developments </span>
  <?php wp_nav_menu(
    array(
      'menu'            => 'footer_mystirling_nav',
      'theme_location'  => 'footer_mystirling_nav',
      'menu_id'         => 'footer-menu',
      'fallback_cb'     => false,
      'container_class' => '',
      'container'       => '',
      'container_id'    => '',
      'menu_class'      => 'list-unstyled footer-nav-project navbar-light',
      'depth'           => 1,
      'walker'          => new NavWalker(),
      'echo'            => true,
      'before'          => '',
      'after'           => '',
      'link_before'     => '',
      'link_after'      => ''
    )
  ); ?>
                  </div>
                  <?php if ( get_field('facebook_url', 'option') || get_field('instagram_url', 'option') ) { ?>
                  <div class="col-48 col-md-24 col-lg-16 col-xl-17 gutters">
                    <span class="footer-multisite-link mb-4"> Social Media </span>
                    <?php if ( get_field('instagram_url', 'option') ) { ?>
                    <a href="<?php echo strip_tags(get_field('instagram_url', 'option'), '<span>'); ?>" class="footer-multisite-social-link mr-2" target="_blank" rel="nofollow">
                      <i class="fa fa-instagram"></i>
                    </a>
                    <?php } ?>
                    <?php if ( get_field('facebook_url', 'option') ) { ?>
                    <a href="<?php echo strip_tags(get_field('facebook_url', 'option'), '<span>'); ?>" class="footer-multisite-social-link" target="_blank" rel="nofollow">
                      <i class="fa fa-facebook"></i>
                    </a>
                    <?php } ?>
                  </div>
                  <?php } ?>
                </div>
              </div>
              <div class="col-48 col-copyright gutters py-3 d-flex align-items-center">
                <p class="text-muted">
                  <small>
                    <span> Copyright &copy; <?php echo date('Y'); ?> </span>
                    <?php if ( get_page_by_path('privacy-policy') ) { 
                      $privacy_policy = get_page_by_path('privacy-policy'); ?>
                    <span class="vertical-sep"></span>
                    <a href="<?php echo get_the_permalink($privacy_policy->ID); ?>"> Privacy Policy </a>
                    <?php } ?>
                  </small>
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div> <?php // need these extra two closes #1/2 ?>
</div><?php //  need these extra two closes #2/2 ?>

<?php wp_footer(); ?> 
<?php require_once( __DIR__ . '/inc/load-footer-js.php'); ?>
<?php require_once( __DIR__ . '/inc/load-apartment-js.php'); ?>

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-M9FHBQK');</script>
<!-- End Google Tag Manager-->

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-M9FHBQK"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

</body>
</html>
<?php
