<?php
/**
 * @package mystirling
 */

if (!defined('ABSPATH')) {
  exit; // Exit if accessed directly.
}

get_header();


?>

<div class="wrapper" id="page-wrapper">
  <div class="container-fluid" id="content" tabindex="-1">
    <div class="row row-entry-content">
      <div class="col-md-48 content-area entry-content" id="primary">
      <?php while ( have_posts() ) : the_post(); ?>


<?php
the_content();
?>

<?php endwhile; // end of the loop. ?>
        <?php require( __DIR__ . '/page-templates/requires.php' ); ?>
      </div>
    </div>
  </div>
</div> 
<?php get_footer();
