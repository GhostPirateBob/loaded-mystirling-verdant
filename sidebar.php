<?php
/**
 * The sidebar containing the main widget area.
 *
 * @package mystirling
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

if ( ! is_active_sidebar( 'sidebar-1' ) ) {
	return;
}
?>

<div class="col-md-16 widget-area" id="secondary" role="complementary">
	<?php dynamic_sidebar( 'sidebar-1' ); ?>
</div>
