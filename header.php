<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package mystirling
 */

if (!defined('ABSPATH')) {
  exit; // Exit if accessed directly.
}

$container    = get_theme_mod('understrap_container_type');
$siteURL      = get_site_url();
$themeURL     = get_stylesheet_directory_uri();
$themePath    = get_stylesheet_directory();
$siteTitle    = get_bloginfo('title');
$logoMobile   = get_field('logo_mobile', 'option');
$logo         = get_field('logo', 'option');
$body_bg      = get_field( 'body_bg', 'option' );
$body_color   = get_field( 'body_color', 'option' );

if ( $body_color === 'white' || $body_color === 'light' ) {
  if ( $body_bg === 'dark' || $body_bg === 'darker' || $body_bg === 'black' ) {
    $body_id = 'mystirling-dark';
  }
}

if ( $body_color === 'dark' || $body_color === 'darker' || $body_color === 'black' ) {
  if ( $body_bg === 'white' || $body_bg === 'light' ) {
    $body_id = 'mystirling-light';
  }
}

if ( get_field('left_enquire', 'option') ) {
  $nav_left_form = get_field('left_enquire', 'option');
  $nav_left_form_title = $nav_left_form['title'];
  $nav_left_form_id = RGFormsModel::get_form_id( $nav_left_form_title ); 
}

if ( get_field('right_enquire', 'option') ) {
  $nav_right_form = get_field('right_enquire', 'option');
  $nav_right_form_title = $nav_right_form['title'];
  $nav_right_form_id = RGFormsModel::get_form_id( $nav_right_form_title ); 
}

?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
  <meta charset="<?php bloginfo('charset'); ?>">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no, shrink-to-fit=no">
  <link rel="dns-prefetch" href="//gmpg.org">
  <link rel="dns-prefetch" href="//fonts.googleapis.com">
  <link rel="dns-prefetch" href="//www.googletagmanager.com">
  <link rel="dns-prefetch" href="//www.facebook.com">
  <link rel="dns-prefetch" href="//connect.facebook.net">
  <link rel="dns-prefetch" href="//sasinator.realestate.com.au">
  <link rel="dns-prefetch" href="//player.vimeo.com">
  <link rel="dns-prefetch" href="//cdnjs.cloudflare.com">
  <link rel="profile" href="//gmpg.org/xfn/11">
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700|Roboto:300,400,500,700" rel="stylesheet"> 
  <script type="text/javascript" src="<?php echo get_stylesheet_directory_uri() . '/js/jquery-3.4.1.migrate/jquery.min.js'; ?>"></script>
  <style>
    .xdebug-error {
      display: none;
    }
  </style>
  <?php wp_head(); ?> 
</head>
<body id="<?php if ( strlen($body_id) > 0 ) { echo $body_id; } ?>" <?php body_class(); ?>>
  <div class="site" id="page">
    <div class="ld-sidebar navbar navbar-left navbar-left-main d-flex-column align-items-center justify-content-between bg-white" id="navbar-left-main">
      <div class="d-flex align-items-center justify-content-center flex-column">
        <button id="sidebar-left-nav-button" type="button" class="btn tcon tcon-menu--xcross sidebar-left-toggler" aria-label="toggle menu">
          <span class="tcon-menu__lines" aria-hidden="true"></span>
          <span class="tcon-visuallyhidden">toggle menu</span>
        </button>
      </div>
    </div>

      <div class="navbar navbar-right navbar-right-main d-inline-block ml-auto bg-nav-right-darker" id="navbar-right-main">
        <div class="row">
          <div class="col d-flex align-items-center justify-content-start">
            <a href="<?php echo $siteURL; ?>" class="d-block w-100" alt="<?php echo $siteTitle; ?>">
            <?php if ( get_field('logo_mobile', 'option') ) { ?>
              <img class="img-fluid logo-mobile d-block d-md-none" src="<?php echo get_field('logo_mobile', 'option'); ?>" />
            <?php } ?>
            </a>
          </div>
          <div class="col-auto">
            <button id="sidebar-right-nav-button" type="button" class="tcon tcon-menu--xcross btn btn-nav-toggle text-white sidebar-right-toggler" aria-label="toggle menu">
              <span class="tcon-menu__lines" aria-hidden="true"></span>
              <span class="tcon-visuallyhidden">toggle menu</span>
            </button>
          </div>
          <div class="col-auto">
            <button id="sidebar-right-enquire-button" type="button" class="btn btn-enquire text-uppercase" aria-label="enquire now">
              Enquire
            </button>
          </div>
        </div>
      </div>

      <div class="ld-sidebar sidebar left sidebar-left sidebar-left-nav bg-white" id="sidebar-left-nav">
        <div class="wrapper py-6 py-lg-7 pr-5 pr-lg-6">
          <div class="row w-100">
            <div class="col-48 gutters">
              <h3 class="mt-3 mb-5 text-black mw-lg-20"> Creating places where people want to live. </h3>
            </div>
            <div class="col-auto gutters">
              <h3 class="mt-3 mb-4 text-uppercase text-black">
                <small> Developments </small>
              </h3>
            </div>
          </div>
          <div class="row">
            <div class="col-auto gutters">
          <?php wp_nav_menu(
    array(
      'menu'            => 'sidebar_mystirling_nav',
      'theme_location'  => 'sidebar_mystirling_nav',
      'menu_id'         => 'sidebar-mystirling-nav',
      'fallback_cb'     => false,
      'container_class' => '',
      'container'       => '',
      'container_id'    => '',
      'menu_class'      => 'navbar-nav mr-auto d-flex align-items-center flex-column justify-content-evenly list-unstyled pt-2',
      'depth'           => 1,
      'walker'          => new NavWalker(),
      'echo'            => true,
      'before'          => '',
      'after'           => '',
      'link_before'     => '',
      'link_after'      => ''
    )
  ); ?>
            </div>
          </div>
        </div>
      </div>
      
      <div class="ld-sidebar sidebar right sidebar-right sidebar-right-enquire bg-nav-right" id="sidebar-right-enquire">
        <div class="wrapper pt-7 pb-5 px-5 px-lg-6">
          <div class="row">
            <div class="col-48 col-md-36 col-lg-32 col-xl-28 gutters">
              <div class="d-block w-100">
                <h3 class="mt-3 mb-5 text-white mw-lg-30 fittext"> Please let us know your contact details and how we can help you. </h3>
              </div>
            </div>
            <div class="col-48 col-md-12 col-lg-16 col-xl-20 gutters"></div>
          </div>
          <?php if ( get_field('right_enquire', 'option') ) { ?>
          <div class="row">
            <div class="col-48">
              <?php gravity_form($nav_right_form_id, false, false, false, '', true, $nav_right_form_id * 100); ?>
            </div>
          </div>
          <?php } ?>
        </div>
      </div>

      <div class="sidebar right sidebar-right sidebar-right-nav bg-nav-right-dark" id="sidebar-right-nav" itemscope itemtype="http://schema.org/WebSite">
          <a class="skip-link sr-only sr-only-focusable" href="#content"> <?php esc_html_e('Skip to content', 'mystirling'); ?> </a>
          <div class="wrapper sidebar-wrapper">
            <nav class="navbar navbar-header navbar-expand navbar-dark">
              <div class="collapse navbar-collapse" id="sidebar-right-nav-collapse">
  <?php wp_nav_menu(
    array(
      'menu'            => 'header_nav',
      'theme_location'  => 'header_nav',
      'menu_id' => 'header-menu',
      'fallback_cb'     => false,
      'container_class' => '',
      'container'       => '',
      'container_id'    => '',
      'menu_class'      => 'navbar-nav ml-auto mr-auto d-flex align-items-center flex-column justify-content-evenly list-unstyled w-100',
      'depth'           => 2,
      'walker'          => new NavWalker(),
      'echo'            => true,
      'before'          => '',
      'after'           => '',
      'link_before'     => '',
      'link_after'      => ''
    )
  ); ?>
            </div>
          </nav>
        </div>
      </div>
<?php
