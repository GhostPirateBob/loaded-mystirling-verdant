<?php

if ( get_sub_field( 'contact_form' ) ) {
  $footer_contact_form = get_sub_field( 'contact_form' );
  $footer_contact_form_title = $footer_contact_form['title'];
  $footer_contact_form_id = RGFormsModel::get_form_id( $footer_contact_form_title ); 
}

if ( get_sub_field( 'contact_form' ) && $footer_contact_form_id > 0 ) { ?>
  <div class="row row-footer-contact px-3 px-lg-0 py-5 py-lg-6 align-items-center">
    <div class="container">
      <div class="row">
        <div class="col-48 gutters" data-aos="fade-up">
        <?php if (have_rows('heading_field')) { ?>
          <?php while (have_rows('heading_field')) {
            the_row();
            $button_color = get_sub_field('button_color');
            $button_hover_color = get_sub_field('button_hover_color');
            $heading_classes = array();
            if ( $button_color !== false ) {
              $heading_classes[] = 'text-' . $button_color;
            }
            if ( $button_hover_color !== false ) {
              $heading_classes[] = 'text-hover-' . $button_color;
            }
              $heading_classes[] = 'mb-2';
          ?>
          <?php if ( get_sub_field('heading') ) { ?>
            <h2 class="<?php foreach ( $heading_classes as $heading_class ) { echo ' ' . $heading_class; } ?>">
              <?php echo strip_tags(get_sub_field('heading'), '<span>'); ?>
            </h2>
          <?php } else { ?>
            <h2 class="footer-heading mb-2 text-white"><?php echo strip_tags(get_field('sales_title', 'option'), '<span>'); ?></h2>
          <?php
                }
              }
            } 
           ?>
          <h4 class="footer-subheading mb-0 text-white">
            <a class="text-white" href="<?php echo strip_tags(get_field('phone_dial', 'option'), '<span>'); ?>"> 
              <?php echo strip_tags(get_field('phone', 'option'), '<span>'); ?> 
            </a>
          </h4>
          <h4 class="footer-email mb-5"><a href="mailto:<?php echo strip_tags(get_field('email', 'option'), '<span>'); ?>"
              class="mb-0 text-white"> <?php echo strip_tags(get_field('email', 'option'), '<span>'); ?> </a></h4>
        </div>
        <div class="col-48 gutters">
          <div class="row w-100" data-aos="fade-up">
            <div class="col mw-56 ml-auto"> 
              <?php gravity_form( $footer_contact_form_id, false, false, false, '', true, $footer_contact_form_id * 100 ); ?> 
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
<?php }
