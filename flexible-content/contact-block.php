<?php 

$siteTitle                    = get_bloginfo( 'name' );
$siteTitle                    = get_bloginfo( 'url' );
$logo                         = get_field( 'logo', 'option' );
$google_map_location          = get_field('google_map_google_map_location', 'option');
$gmap_initial_lat             = $google_map_location['lat'];
$gmap_initial_lng             = $google_map_location['lng'];
$gmap_initial_address         = $google_map_location['address'];
$gmap_initial_marker          = get_field('google_map_marker_image', 'option');
$gmap_initial_infoboxTitle    = get_field('google_map_map_info_heading', 'option');
$gmap_initial_infoboxContent  = get_field('google_map_map_info_content', 'option');
$gmap_initial_cid             = get_field('google_map_cid', 'option');
$gmap_initial_directionsLink  = get_field('google_map_link_directions', 'option');
$gmap_initial_mapsLink        = get_field('google_map_view_on_google_maps_link', 'option');
$gmap_styling_code            = get_field('google_map_styling_code', 'option');

$phone                        = get_field('phone_display', 'option');
$phone_dial                   = get_field('phone_dial', 'option');

if ( $gmap_initial_cid ) {
  $gmap_initial_mapsLink = "http://maps.google.com/maps/place?cid=" . $gmap_initial_cid;
} 
else if ( get_field( 'google_map_view_on_google_maps_link', 'option') ) {
  $gmap_initial_mapsLink = get_field( 'google_map_view_on_google_maps_link', 'option' );
}
if ( get_field('google_map_marker_image', 'option') ) {
  $gmap_initial_marker = $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['SERVER_NAME'] . $gmap_initial_marker['url']; 
} else {
  $gmap_initial_marker = 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/1.5.1/images/marker-icon.png';
}
if ( $logo ) {
  $logo = $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['SERVER_NAME'] . $logo['url']; 
}

$footer_text_class = "";
if ( get_field('gmap_color', 'option') ) {
  $footer_text_class = 'text-' . get_field('gmap_color', 'option');
}

$footer_heading_class = "";
if ( get_field('gmap_heading_color', 'option') ) {
  $footer_heading_class = 'text-' . get_field('gmap_heading_color', 'option');
}

?>

<div class="container">
    <div class="row row-contact-block contact-details-block pb-4" data-aos="fade-up">
    <div class="col-48 col-lg-16 col-xl-14 contact-details-block col-footer-nav-left pl-5 pr-5 pl-xl-0 pr-xl-7 pt-xl-4">
        <div class="footer-img-wrapper img-logo-bot d-block mt-0 mb-3" data-aos="fade-right">
          <?php if ( get_field('logo', 'option') ) {
            $logo = get_field('logo', 'option');
            $logo = $logo['url']; ?>
          <img  class="optionsLogo mb-5" src="<?php echo $logo; ?>" />
          <?php } ?>
          <h5 class="<?php echo $footer_text_class; ?> mb-3 footer-wrapper-text">
          <?php echo strip_tags(get_field('address_one', 'option'), '<span>'); ?>
          <?php if (get_field('address_two', 'option')) { ?>
            <br><?php echo strip_tags(get_field( 'address_two', 'option' ), '<span>'); ?>
          <?php } ?>
          </h5>
        </div>
      <div class="footer-contact-wrapper d-block w-100 mt-3 mb-4" data-aos="fade-right">
        <?php if (get_field('sales_title', 'option')) { ?>  
        <h5 class="<?php echo $footer_heading_class; ?> text-uppercase mb-3">
          <?php echo strip_tags(get_field( 'sales_title', 'option' ), '<span>'); ?>
        </h5>
        <?php } ?>
        <?php if (get_field('phone', 'option') && get_field('phone_dial', 'option')) { ?>
        <h4 class="mb-1"><a href="tel:<?php echo strip_tags(get_field('phone_dial', 'option'), '<span>'); ?>" class="<?php echo $footer_text_class; ?>">
          <?php echo strip_tags(get_field('phone', 'option'), '<span>'); ?></a>
        </h4>
        <?php } ?>
        <?php if (get_field('email', 'option')) { ?>
        <h4 class="mb-0"><a href="mailto:<?php echo strip_tags(get_field('email', 'option'), '<span>'); ?>" class="<?php echo $footer_text_class; ?>">
          <small> <?php echo strip_tags(get_field('email', 'option'), '<span>'); ?></small>
        </a></h4>
        <?php } ?>
      </div>
      <?php if (get_field('opening_title', 'option')) { ?>
      <div class="footer-address-wrapper d-block w-100 mt-3 mb-3 pr-xl-5" data-aos="fade-right">
        <h5 class="<?php echo $footer_heading_class; ?> text-uppercase"> 
          <?php echo strip_tags(get_field( 'opening_title', 'option' ), '<span>'); ?>
        </h5>
      </div>
      <?php } ?>
      <?php if ( have_rows( 'open_hours', 'option' ) ) { ?>
      <div class="footer-opening-wrapper d-block w-100 " data-aos="fade-right">
        <?php while ( have_rows( 'open_hours', 'option' ) ) {
          the_row(); ?>
        <h4 class="<?php echo $footer_text_class; ?> mb-0"><small> 
        <?php if ( get_sub_field( 'text' )) { ?> 
          <?php echo strip_tags(get_sub_field( 'text' ), '<span>'); ?> 
        <?php } ?>
        <?php if ( get_sub_field( 'start_time' )) { ?> 
          <?php echo strip_tags(get_sub_field( 'start_time'), '<span>');?> 
        <?php } ?>
        <?php if ( get_sub_field( 'end_time' )) { ?> 
          <?php echo strip_tags(get_sub_field( 'end_time' ), '<span>'); ?>
        <?php } ?>
        </small></h4>
        <?php } ?>
      <?php } ?>
      
      <?php if ( get_field( 'google_map_view_on_google_maps_link', 'option' ) ) { ?>
        <?php if ( get_field('footer_button_hover_color', 'option') && get_field('footer_button_hover_color', 'option') === 'secondary' ) { ?>
        <a href="<?php the_field( 'google_map_view_on_google_maps_link', 'option' ); ?>" target="_blank" class="btn btn-link btn-arrow-right btn-arrow-right-sm btn-arrow-right-secondary d-xl-none">
        <?php } else { ?>
        <a href="<?php the_field( 'google_map_view_on_google_maps_link', 'option' ); ?>" target="_blank" class="btn btn-link btn-arrow-right btn-arrow-right-sm btn-arrow-right-secondary d-xl-none">
        <?php } ?>
        <span class="btn-arrow-text"> View on Google Maps </span>
        <?php echo file_get_contents($themePath . '/img/arrow-right.svg'); ?> 
      </a>
      <?php } ?>

      </div>
    </div>
    <div class="col-48 col-lg-32 col-xl-34 mb-3 gutters" data-aos="fade-up">
      <div class="wrapper__map">
        <div id="map" class="google-map-embed mb-3"></div>
      </div>
      <?php if ( $gmap_initial_directionsLink ) { ?>
      <a type="link" class="btn btn-link btn-arrow-right btn-arrow-right-sm d-none d-xl-inline-block" target="_blank" rel="nofollow"
        href="<?php echo $gmap_initial_directionsLink; ?>">
        <span class="btn-arrow-text">View on Google Maps</span>
        <?php echo file_get_contents($themePath . '/img/arrow-right.svg'); ?> </a>
      <?php } ?>
    </div>
  </div>
</div>


<script type="text/javascript">
function initMap() {
  var mapElement = document.getElementById( 'map' );
  var mapOptions = {
    zoom: 16,
    center: new google.maps.LatLng(<?php echo $gmap_initial_lat; ?>,<?php echo $gmap_initial_lng; ?>),
    mapTypeControl: false,
    streetViewControl: false,
    disableDefaultUI: true,
    <?php if ( get_field('google_map_styling_code', 'option') && testGmapStyleArray($gmap_styling_code) ) { ?>
    styles: <?php 
      the_field('google_map_styling_code', 'option');
    } else { ?>[ { "featureType": "all", "elementType": "geometry", "stylers": [ { "saturation": "0" }, { "lightness": "0" }, { "visibility": "on" }, { "gamma": "1" } ] }, { "featureType": "all", "elementType": "labels.text.fill", "stylers": [ { "saturation": 36 }, { "color": "#374047" }, { "lightness": 40 } ] }, { "featureType": "all", "elementType": "labels.text.stroke", "stylers": [ { "visibility": "off" }, { "color": "#000000" }, { "lightness": "0" } ] }, { "featureType": "all", "elementType": "labels.icon", "stylers": [ { "visibility": "off" } ] }, { "featureType": "administrative", "elementType": "geometry.fill", "stylers": [ { "lightness": 20 }, { "visibility": "off" } ] }, { "featureType": "administrative", "elementType": "geometry.stroke", "stylers": [ { "lightness": 17 }, { "weight": 1.2 }, { "visibility": "on" }, { "color": "#161829" } ] }, { "featureType": "landscape", "elementType": "geometry", "stylers": [ { "color": "#384148" }, { "lightness": "0" }, { "visibility": "on" }, { "weight": "1.00" }, { "gamma": "1" } ] }, { "featureType": "poi", "elementType": "geometry", "stylers": [ { "color": "#60676d" }, { "lightness": "0" }, { "visibility": "on" } ] }, { "featureType": "poi.attraction", "elementType": "geometry", "stylers": [ { "visibility": "on" } ] }, { "featureType": "poi.attraction", "elementType": "geometry.fill", "stylers": [ { "visibility": "off" } ] }, { "featureType": "poi.attraction", "elementType": "geometry.stroke", "stylers": [ { "visibility": "off" } ] }, { "featureType": "poi.business", "elementType": "geometry", "stylers": [ { "visibility": "off" } ] }, { "featureType": "poi.business", "elementType": "geometry.fill", "stylers": [ { "visibility": "simplified" } ] }, { "featureType": "poi.business", "elementType": "labels", "stylers": [ { "visibility": "off" } ] }, { "featureType": "poi.business", "elementType": "labels.text", "stylers": [ { "visibility": "off" } ] }, { "featureType": "poi.business", "elementType": "labels.text.fill", "stylers": [ { "visibility": "off" } ] }, { "featureType": "road.highway", "elementType": "geometry.fill", "stylers": [ { "lightness": "0" }, { "color": "#162029" } ] }, { "featureType": "road.highway", "elementType": "geometry.stroke", "stylers": [ { "color": "#162029" }, { "lightness": "0" }, { "weight": 0.2 }, { "visibility": "off" } ] }, { "featureType": "road.highway.controlled_access", "elementType": "geometry.fill", "stylers": [ { "color": "#162029" }, { "visibility": "off" } ] }, { "featureType": "road.arterial", "elementType": "geometry", "stylers": [ { "lightness": "0" }, { "visibility": "on" }, { "color": "#162029" } ] }, { "featureType": "road.arterial", "elementType": "geometry.fill", "stylers": [ { "color": "#162029" } ] }, { "featureType": "road.local", "elementType": "geometry", "stylers": [ { "color": "#162029" }, { "lightness": "0" } ] }, { "featureType": "road.local", "elementType": "geometry.fill", "stylers": [ { "color": "#162029" } ] }, { "featureType": "transit", "elementType": "geometry", "stylers": [ { "color": "#162029" }, { "lightness": "0" }, { "visibility": "on" } ] }, { "featureType": "water", "elementType": "geometry", "stylers": [ { "color": "#60676d" }, { "lightness": "0" } ] } ]<?php } ?>
  };
  var map = new google.maps.Map(mapElement, mapOptions);
  var marker = new google.maps.Marker({
      position: new google.maps.LatLng(<?php echo $gmap_initial_lat; ?>,<?php echo $gmap_initial_lng; ?>),
      map: map,
      title: '<?php echo $siteTitle; ?>',
      icon: '<?php echo $gmap_initial_marker; ?>'
  });
  var contentString = ' ' + 
    '<div class="gmap-info-body" style="max-width: 268px;">' + 
    <?php if ( $gmap_initial_infoboxTitle ) { ?>
    '<h3 class="gmap-info-title"><?php echo $gmap_initial_infoboxTitle; ?></h3>' + 
    <?php } ?>
    <?php if ( $gmap_initial_infoboxContent ) { ?>
    '<div class="card-text"><?php echo str_replace("\n", "", $gmap_initial_infoboxContent); ?></div>' + 
    <?php } ?>
    <?php if ( $gmap_initial_directionsLink ) { ?>
    '<a href="<?php echo $gmap_initial_directionsLink; ?>" class="card-link ml-0 d-inline-block" target="_blank">Get Directions<i class="fa fa-external-link" aria-hidden="true"></i></a>' + 
    <?php } ?>
    <?php if ( $gmap_initial_mapsLink ) { ?>
    '<a href="<?php echo $gmap_initial_mapsLink; ?>" class="card-link ml-0 d-inline-block" target="_blank">View on Google Maps<i class="fa fa-map-marker" aria-hidden="true"></i></a>' + 
    <?php } ?>
    '</div>';

  var infowindow = new google.maps.InfoWindow( {
    content: contentString
  });
  marker.addListener( 'click', function() {
    infowindow.open( map, marker );
  });
}
</script>
<script type="text/javascript"
  src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBw-Ld2uyp74D05cs6QOK3CVpnKcjxJ1e0&libraries=places&callback=initMap">
</script> 