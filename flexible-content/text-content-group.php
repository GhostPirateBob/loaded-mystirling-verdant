<?php 
if ( get_sub_field('content_field') ) {
  $subheading_classes = array('mb-4');
} else {
  $subheading_classes = array('mb-0');
}
$content_field = get_sub_field('content_field');
?>
<div class="container">
  <div class="row">
    <div class="col-48" data-aos="fade-up">
      <?php if (have_rows('heading_field')) { ?>
        <?php while (have_rows('heading_field')) {
          the_row();
          $button_hover_color = get_sub_field('button_hover_color');
          $heading_contain = get_sub_field('heading_contain');
          $heading_classes = array('home-headline', 'p-0', 'pl-md-0');
          if ( $button_hover_color !== false ) {
            $heading_classes[] = 'text-' . $button_hover_color;
          }
          if ( $heading_contain === 'yes' ) {
            $heading_classes[] = 'heading-contain';
          }
          if ( get_page_template_slug() === 'page-templates/page-gallery.php' && $next_layout === 'image_gallery_block' ) { 
            $heading_classes[] = 'mb-3';
          } else {
            $heading_classes[] = 'mb-5';
          }
        ?>
      <?php if ( get_page_template_slug() === 'page-templates/page-home.php' ) { ?>
        <div class="homepage-content-wrapper">
      <?php } ?>
      <?php if ( get_sub_field('heading') ) { ?>
          <div class="row">
            <div class="col-48 gutters">
              <?php if ( get_page_template_slug() === 'page-templates/page-gallery.php' && $next_layout === 'image_gallery_block') { ?>
              <h3 class="<?php foreach ( $heading_classes as $heading_class ) { echo ' ' . $heading_class; } ?>">
                <?php echo strip_tags(get_sub_field('heading'), '<span>'); ?>
              </h3>
              <?php } else { ?>
              <h2 class="<?php foreach ( $heading_classes as $heading_class ) { echo ' ' . $heading_class; } ?>">
                <?php echo strip_tags(get_sub_field('heading'), '<span>'); ?>
              </h2>
              <?php } ?>
            </div>
          </div>
          <?php } ?>
        <?php } ?>
      <?php } ?>

      <?php 
      if ( have_rows( 'button_link' ) ) {
      $button_link = get_sub_field('button_link');
      while ( have_rows( 'button_link' ) ) {
        the_row();
        $read_more_button = get_sub_field( 'read_more_button' );
        $button_hover_color = get_sub_field( 'button_hover_color' );
        $button_color = get_sub_field( 'button_color' );
        }
      }

      if ( $read_more_button ) {

        $button_classes = array('btn', 'btn-link', 'btn-arrow-right');
        if ( $button_color ) {
          $button_classes[] = 'btn-arrow-right-' . $button_color;
        }
        if ( $button_hover_color ) {
          $button_classes[] = 'btn-arrow-right-hover-' . $button_hover_color;
        }
      ?>

      <div class="row">
        <div class="col-48 justify-content-start d-flex mb-5 gutters">
          <a class="<?php foreach ( $button_classes as $button_class ) { echo ' ' . $button_class; } ?>"
            type="link"
            href="<?php echo $read_more_button['url']; ?>"
            <?php echo $read_more_button['target']; ?> >
            <span class="btn-arrow-text text-uppercase"><?php echo $read_more_button['text']; ?></span>
            <?php echo file_get_contents($themePath . '/img/arrow-right.svg'); ?> 
          </a>
        </div>
      </div>
      <?php } ?>
    <?php if ( get_page_template_slug() === 'page-templates/page-home.php' ) { ?>
    </div>
    <?php } ?>

      <?php 
      if ( have_rows('subheading_field') ) { 
        while (have_rows('subheading_field')) {
        the_row();
        if ( get_sub_field('subheading')|| $content_field ) {
          if ( $button_hover_color !== 'dark' ) {
            $subheading_classes[] = 'text-white';
          }
          $subheading_classes[] = 'mw-lg-38';
      ?>
      <div class="row">
        <div class="col-48 col-lg-34 gutters mx-auto mb-5">
        <?php if ( get_sub_field('subheading') ) { ?>
          <h4 class="<?php foreach ( $subheading_classes as $subheading_class ) { echo ' ' . $subheading_class; } ?>">
            <?php echo strip_tags(get_sub_field('subheading'), '<span>'); ?>
          </h4>
        <?php } ?>
        <?php if ( $content_field ) { 
          $content_classes = array('mb-0');
          if ( $button_hover_color !== 'dark' ) {
            $content_classes[] = 'text-white';
          }
          $content_classes[] = 'mw-lg-38';
        ?>
          <p class="<?php foreach ( $content_classes as $content_class ) { echo ' ' . $content_class; } ?>">
            <?php echo strip_tags($content_field, '<span>'); ?>
          </p>
        <?php } ?>
        </div>
      </div>
<?php 
        }
      }
    }
?>
    </div>
  </div>
</div>
