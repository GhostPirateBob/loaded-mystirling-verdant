<?php
$image_gallery_images = get_sub_field('image_gallery');
if ($image_gallery_images) {
  $image_gallery_count = count($image_gallery_images);
  $image_gallery_array = array();
  $i                   = 0;
  foreach ($image_gallery_images as $image_gallery_image) {
    $image_gallery_array[$i]['url']     = $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['SERVER_NAME'] . $image_gallery_image['url'];
    $image_gallery_array[$i]['alt']     = $image_gallery_image['alt'];
    $image_gallery_array[$i]['caption'] = $image_gallery_image['caption'];
    $i++;
  }
  ?> 
<?php if ($image_gallery_count === 1) { ?>
<div class="image-gallery-block-wrapper headline-wrapper">
  <div class="container">
    <div class="row row-image-block choco-gallery">
      <div class="col-48 gutters mb-3" data-aos="fade-right">
        <a class="chocolat-image ms-lightbox-a" href="<?php echo $image_gallery_array[0]['url']; ?>"><img
            class="img-fluid img-fit" src="<?php echo $image_gallery_array[0]['url']; ?>">
          <div class="bx-lightbox-overlay d-flex align-items-center justify-content-center"> <span
              class="ms-overlay-plus"></span> </div>
        </a>
      </div>
      <?php if (get_sub_field('content_field') && get_sub_field('has_content') === 'yes') { ?>
      <div class="col-48 col-lg-34 gutters mb-3 mt-5 align-items-center justify-content-center d-flex mx-auto" data-aos="fade-up">
         <p class="mb-0 text-white mw-lg-38 mr-auto"><?php echo strip_tags(get_sub_field('content_field'), '<span>'); ?></p>
      </div>
      <?php } ?>
    </div>
  </div>
</div> 
<?php } ?>
<?php if ($image_gallery_count === 2) { ?>
<div class="image-gallery-block-wrapper headline-wrapper">
  <div class="container">
    <div class="row row-image-block choco-gallery">
      <div class="col-48 col-lg-32 gutters mb-3" data-aos="fade-right">
        <a class="chocolat-image ms-lightbox-a" href="<?php echo $image_gallery_array[0]['url']; ?>"><img
            class="img-fluid img-fit" src="<?php echo $image_gallery_array[0]['url']; ?>">
          <div class="bx-lightbox-overlay d-flex align-items-center justify-content-center"> <span
              class="ms-overlay-plus"></span> </div>
        </a>
      </div>
      <div class="col-48 col-lg-16 gutters mb-3" data-aos="fade-left">
        <a class="chocolat-image ms-lightbox-a" href="<?php echo $image_gallery_array[1]['url']; ?>">
          <img class="img-fluid img-fit w-100" src="<?php echo $image_gallery_array[1]['url']; ?>">
          <div class="bx-lightbox-overlay d-flex align-items-center justify-content-center"> <span
              class="ms-overlay-plus"></span> </div>
        </a>
      </div>
      <?php if (get_sub_field('content_field') && get_sub_field('has_content') === 'yes') { ?>
      <div class="col-48 col-lg-34 gutters mb-3 mt-5 align-items-center justify-content-center d-flex mx-auto" data-aos="fade-up">
         <p class="mb-0 text-white mw-lg-38 mr-auto"><?php echo strip_tags(get_sub_field('content_field'), '<span>'); ?></p>
      </div>
      <?php } ?>
    </div>
  </div>
</div>
<?php } ?>

<?php if ($image_gallery_count === 3) { ?>
<?php if (get_page_template_slug() === 'page-templates/page-home.php' && get_sub_field('has_content') === 'no') { ?>
<div class="section-six-wrapper">
  <div class="container">
    <div class="row row-image-block choco-gallery">
      <div class="col-48 col-lg-20 gutters" data-aos="fade-right">
        <div class="row">
          <div class="col-48 mb-3 mt-5 overlay-adjust-1">
            <a class="chocolat-image ms-lightbox-a" href="<?php echo $image_gallery_array[0]['url']; ?>">
              <img class="img-fluid img-fit " src="<?php echo $image_gallery_array[0]['url']; ?>" />
              <div class="bx-lightbox-overlay d-flex align-items-center justify-content-center "> <span
                  class="ms-overlay-plus"></span> <span
                  class="choco-title"><?php echo $image_gallery_array[0]['caption']; ?></span></div>
            </a>
          </div>
        </div>
        <div class="row" data-aos="fade-up">
          <div class="col-48 mb-3 overlay-adjust-2">
            <a class="chocolat-image ms-lightbox-a" href="<?php echo $image_gallery_array[1]['url']; ?>">
              <img class="img-fluid img-fit" src="<?php echo $image_gallery_array[1]['url']; ?>" />
              <div class="bx-lightbox-overlay d-flex align-items-center justify-content-center"> <span
                  class="ms-overlay-plus"></span> <span
                  class="choco-title"><?php echo $image_gallery_array[1]['caption']; ?></span></div>
            </a></div>
        </div>
      </div>
      <div class="col-48 col-lg-28 gutters mb-3" data-aos="fade-left">
        <a class="chocolat-image ms-lightbox-a" href="<?php echo $image_gallery_array[2]['url']; ?>">
          <img class="img-fluid img-fit" src="<?php echo $image_gallery_array[2]['url']; ?>" />
          <div class="bx-lightbox-overlay d-flex align-items-center justify-content-center"> <span
              class="ms-overlay-plus"></span> <span
              class="choco-title"><?php echo $image_gallery_array[2]['caption']; ?></span></div>
        </a>
      </div>
    </div>
  </div>
</div> 
<?php } ?>
<?php if ( get_sub_field('has_content') === 'no' && get_page_template_slug() !== 'page-templates/page-home.php' ) { ?>
<div class="container">
  <div class="row pt-2">
    <div class="col-48 col-lg-28 gutters mb-3 d-flex justify-content-start flex-column align-items-end"
      data-aos="fade-right">
      <a class="chocolat-image ms-lightbox-a ml-auto mb-3"
        href="<?php echo $image_gallery_array[0]['url']; ?>"><img class="img-fluid img-fit"
          src="<?php echo $image_gallery_array[0]['url']; ?>" />
        <div class="bx-lightbox-overlay d-flex align-items-center justify-content-center"> <span
            class="ms-overlay-plus"></span> </div>
      </a>
      <a class="chocolat-image ms-lightbox-a ml-auto" href="<?php echo $image_gallery_array[1]['url']; ?>"><img
          class="img-fluid img-fit" src="<?php echo $image_gallery_array[1]['url']; ?>" />
        <div class="bx-lightbox-overlay d-flex align-items-center justify-content-center"> <span
            class="ms-overlay-plus"></span> </div>
      </a>
    </div>
    <div class="col-48 col-lg-20 gutters choco-gallery" data-aos="fade-left">
      <a class="chocolat-image ms-lightbox-a" href="<?php echo $image_gallery_array[2]['url']; ?>"><img
          class="img-fluid img-fit w-100" src="<?php echo $image_gallery_array[2]['url']; ?>" />
        <div class="bx-lightbox-overlay d-flex align-items-center justify-content-center"> <span
            class="ms-overlay-plus"></span> </div>
      </a>
    </div>
  </div>
</div> 
<?php } ?>
<?php if (get_page_template_slug() === 'page-templates/page-building.php' && get_sub_field('content_field') && get_sub_field('has_content') === 'yes') { ?>
<div class="container choco-gallery">
  <div class="row row-image-block ">
    <div class="col-48 col-lg-28 gutters mb-3" data-aos="fade-up">
      <a class="chocolat-image ms-lightbox-a" href="<?php echo $image_gallery_array[0]['url']; ?>"><img
          class="img-fluid img-fit w-100" src="<?php echo $image_gallery_array[0]['url']; ?>">
        <div class="bx-lightbox-overlay d-flex align-items-center justify-content-center"> <span
            class="ms-overlay-plus"></span> </div>
      </a>
    </div>
    <div class="col-48 col-lg-20 gutters mt-auto mb-3" data-aos="fade-left">
      <a class="chocolat-image ms-lightbox-a" href="<?php echo $image_gallery_array[1]['url']; ?>"> <img
          class="img-fluid img-fit w-100" src="<?php echo $image_gallery_array[1]['url']; ?>">
        <div class="bx-lightbox-overlay d-flex align-items-center justify-content-center"> <span
            class="ms-overlay-plus"></span> </div>
      </a>
    </div>
  </div>
  <div class="row row-image-block d-flex align-items-center"> <?php if (get_sub_field('content_field')) { ?> <div
      class="col-48 col-lg-28 gutters align-items-center mw-content px-4 px-lg-0 py-5" data-aos="fade-up">
      <div class="mw-content"> <?php echo strip_tags(get_sub_field('content_field'), '<span>'); ?> </div>
    </div> <?php } ?> <div class="col-48 col-lg-20 gutters mb-3" data-aos="fade-left">
      <a class="chocolat-image ms-lightbox-a" href="<?php echo $image_gallery_array[2]['url']; ?>">
        <img class="img-fluid img-fit w-100" src="<?php echo $image_gallery_array[2]['url']; ?>">
        <div class="bx-lightbox-overlay d-flex align-items-center justify-content-center">
          <span class="ms-overlay-plus"></span>
        </div>
      </a>
    </div>
  </div>
</div> 
<?php } ?>
<?php if (get_sub_field('content_field') && get_sub_field('has_content') === 'yes' && get_page_template_slug() !== 'page-templates/page-building.php') { ?>
<div class="container py-5 py-lg-6 choco-gallery">
  <div class="row row-image-block">
    <div class="col-48 col-lg-18 gutters d-flex align-items-end justify-content-end mb-3" data-aos="fade-right">
      <a class="chocolat-image ms-lightbox-a ml-auto" href="<?php echo $image_gallery_array[0]['url']; ?>">
        <img class="img-fluid img-fit w-100" src="<?php echo $image_gallery_array[0]['url']; ?>">
        <div class="bx-lightbox-overlay d-flex align-items-center justify-content-center"> <span
            class="ms-overlay-plus"></span> </div>
      </a>
    </div>
    <div class="col-48 col-lg-21 gutters mt-auto mb-3" data-aos="fade-left">
      <a class="chocolat-image ms-lightbox-a" href="<?php echo $image_gallery_array[1]['url']; ?>">
        <img class="img-fluid img-fit" src="<?php echo $image_gallery_array[1]['url']; ?>">
        <div class="bx-lightbox-overlay d-flex align-items-center justify-content-center"> <span
            class="ms-overlay-plus"></span> </div>
      </a>
    </div>
  </div>
  <div class="row row-image-block d-flex align-items-center">
    <div class="col-48 col-lg-24 gutters mb-3" data-aos="fade-up">
      <a class="chocolat-image ms-lightbox-a" href="<?php echo $image_gallery_array[2]['url']; ?>">
        <img class="img-fluid img-fit w-100" src="<?php echo $image_gallery_array[2]['url']; ?>">
        <div class="bx-lightbox-overlay d-flex align-items-center justify-content-center"> <span
            class="ms-overlay-plus"></span> </div>
      </a>
    </div>
    <div
      class="col-48 col-lg-15 gutters align-items-center justify-content-center pl-4 pr-4 pr-lg-0 pl-lg-5 py-5 py-lg-0 mb-3s"
      data-aos="fade-up">
      <p><?php echo strip_tags(get_sub_field('content_field'), '<span>'); ?></p>
    </div>
  </div>
</div>
<?php } ?>
<?php } ?>

<?php if ($image_gallery_count === 5) { ?>
<?php if (get_sub_field('has_content') === 'no') { ?>
<div class="container" id="thebuilding" name="thebuilding" >
  <div class="row row-image-block choco-gallery" data-aos="fade-up">
    <div class="col-48 col-lg-24 gutters mb-3">
      <a class="chocolat-image ms-lightbox-a ml-auto" href="<?php echo $image_gallery_array[0]['url']; ?>">
        <img class="img-fluid img-fit" src="<?php echo $image_gallery_array[0]['url']; ?>" />
        <div class="bx-lightbox-overlay d-flex align-items-center justify-content-center"> <span
            class="ms-overlay-plus"></span><span
            class="choco-title"><?php echo $image_gallery_array[0]['caption']; ?></span> </div>
      </a>
    </div>
    <div class="col-48 col-lg-24 choco-gallery">
      <div class="row" data-aos="fade-up">
        <div class="col-48 gutters mb-3">
          <a class="chocolat-image ms-lightbox-a" href="<?php echo $image_gallery_array[1]['url']; ?>">
            <img class="img-fluid img-fit" src="<?php echo $image_gallery_array[1]['url']; ?>" />
            <div class="bx-lightbox-overlay d-flex align-items-center justify-content-center"> <span
                class="ms-overlay-plus"></span> <span
                class="choco-title"><?php echo $image_gallery_array[1]['caption']; ?></span></div>
          </a>
        </div>
      </div>
      <div class="row" data-aos="fade-up">
        <div class="col-48 gutters mb-3">
          <a class="chocolat-image ms-lightbox-a" href="<?php echo $image_gallery_array[2]['url']; ?>">
            <img class="img-fluid img-fit" src="<?php echo $image_gallery_array[2]['url']; ?>" />
            <div class="bx-lightbox-overlay d-flex align-items-center justify-content-center"> <span
                class="ms-overlay-plus"></span> <span
                class="choco-title"><?php echo $image_gallery_array[2]['caption']; ?></span></div>
          </a>
        </div>
      </div>
      <div class="row choco-gallery" data-aos="fade-up">
        <div class="col-48 gutters mb-3">
          <a class="chocolat-image ms-lightbox-a" href="<?php echo $image_gallery_array[3]['url']; ?>">
            <img class="img-fluid img-fit" src="<?php echo $image_gallery_array[3]['url']; ?>" />
            <div class="bx-lightbox-overlay d-flex align-items-center justify-content-center"> <span
                class="ms-overlay-plus"></span>
              <span class="choco-title"><?php echo $image_gallery_array[3]['caption']; ?></span></div>
          </a>
        </div>
      </div>
    </div>
  </div>
  <div class="row row-image-block choco-gallery">
    <div class="col-48 gutters mb-3" data-aos="fade-up">
      <a class="chocolat-image ms-lightbox-a" href="<?php echo $image_gallery_array[4]['url']; ?>">
        <img class="img-fluid img-fit" src="<?php echo $image_gallery_array[4]['url']; ?>" />
        <div class="bx-lightbox-overlay d-flex align-items-center justify-content-center"> <span
            class="ms-overlay-plus"></span> <span
            class="choco-title"><?php echo $image_gallery_array[4]['caption']; ?></span> </div>
      </a>
    </div>
  </div>
</div>
<?php } ?>
<?php if ( get_sub_field('content_field') && get_sub_field('has_content') === 'yes' ) { ?>
<div id="lifestyle" name="lifestyle" class="container">
  <div class="row row-image-block choco-gallery" data-aos="fade-up">
    <div class="col-48 col-lg-24 gutters choco-gallery mb-3 d-flex align-items-end">
      <a class="chocolat-image ms-lightbox-a ml-auto" href="<?php echo $image_gallery_array[0]['url']; ?>">
        <img class="img-fluid img-fit" src="<?php echo $image_gallery_array[0]['url']; ?>">
        <div class="bx-lightbox-overlay d-flex align-items-center justify-content-center"> <span
            class="ms-overlay-plus"></span> </div>
      </a>
    </div>
    <div class="col-48 col-lg-24 gutters d-flex flex-column mb-3 mb-lg-0">
      <div class="row h-100 align-items-center" data-aos="fade-up">
        <div class="col-48 py-5 py-lg-0 gutters">
          <p class="mw-content "><?php echo strip_tags(get_sub_field('content_field'), '<span>'); ?></p>
        </div>
      </div>
      <div class="row">
        <div class="col-48 mb-3 d-flex justify-content-end" data-aos="fade-up">
          <a class="chocolat-image ms-lightbox-a" href="<?php echo $image_gallery_array[1]['url']; ?>">
            <img class="img-fluid img-fit mt-auto" src="<?php echo $image_gallery_array[1]['url']; ?>">
            <div class="bx-lightbox-overlay d-flex align-items-center justify-content-center"> <span
                class="ms-overlay-plus"></span> </div>
          </a>
        </div>
      </div>
    </div>
  </div>
  <div class="row row-image-block">
    <div class="col-48 col-lg-24 gutters choco-gallery mb-3 d-flex justify-content-end align-items-start"
      data-aos="fade-up">
      <a class="chocolat-image ms-lightbox-a" href="<?php echo $image_gallery_array[2]['url']; ?>">
        <img class="img-fluid img-fit" src="<?php echo $image_gallery_array[2]['url']; ?>">
        <div class="bx-lightbox-overlay d-flex align-items-center justify-content-center"> <span
            class="ms-overlay-plus"></span></div>
      </a>
    </div>
    <div class="col-48 col-lg-24 gutters mt-auto">
      <div class="row choco-gallery mb-3" data-aos="fade-up">
        <a class="chocolat-image ms-lightbox-a" href="<?php echo $image_gallery_array[3]['url']; ?>">
          <img class="img-fluid img-fit" src="<?php echo $image_gallery_array[3]['url']; ?>" />
          <div class="bx-lightbox-overlay d-flex align-items-center justify-content-center"> <span
              class="ms-overlay-plus"></span> </div>
        </a>
      </div>
      <div class="row choco-gallery" data-aos="fade-up">
        <a class="chocolat-image ms-lightbox-a" href="<?php echo $image_gallery_array[4]['url']; ?>">
          <img class="img-fluid img-fit" src="<?php echo $image_gallery_array[4]['url']; ?>">
          <div class="bx-lightbox-overlay d-flex align-items-center justify-content-center"> <span
              class="ms-overlay-plus"></span> </div>
        </a>
      </div>
    </div>
  </div>
</div>
<?php } ?>
<?php } ?>

<?php if ( $image_gallery_count === 9 ) { ?>
<?php if (get_page_template_slug() === 'page-templates/page-gallery.php') { ?> 
<div name="theapartment" id="theapartment" class="gallery-two-wrapper">
  <div class="container">
    <div class="row row-image-block choco-gallery" data-aos="fade-up">
      <div class="col-48 gutters mb-3">
        <a class="chocolat-image ms-lightbox-a" href="<?php echo $image_gallery_array[0]['url']; ?>">
          <img class="img-fluid w-100 img-fit" src="<?php echo $image_gallery_array[0]['url']; ?>" />
          <div class="bx-lightbox-overlay d-flex align-items-center justify-content-center"> <span
              class="ms-overlay-plus"></span> <span
              class="choco-title"><?php echo $image_gallery_array[0]['caption']; ?></span></div>
        </a>
      </div>
    </div>
    <div class="row row-image-block choco-gallery" data-aos="fade-up">
      <div class="col-48 col-lg-24 choco-gallery gutters mb-3">
        <a class="chocolat-image ms-lightbox-a" href="<?php echo $image_gallery_array[1]['url']; ?>" />
        <img class="img-fluid img-fit" src="<?php echo $image_gallery_array[1]['url']; ?>" />
        <div class="bx-lightbox-overlay d-flex align-items-center justify-content-center"> <span
            class="ms-overlay-plus"></span> <span
            class="choco-title"><?php echo $image_gallery_array[1]['caption']; ?></span> </div>
        </a>
      </div>
      <div class="col-48 col-lg-24 gutters mb-3" data-aos="fade-up">
        <a class="chocolat-image ms-lightbox-a" href="<?php echo $image_gallery_array[2]['url']; ?>">
          <img class="img-fluid img-fit" src="<?php echo $image_gallery_array[2]['url']; ?>" />
          <div class="bx-lightbox-overlay d-flex align-items-center justify-content-center"> <span
              class="ms-overlay-plus"></span> <span
              class="choco-title"><?php echo $image_gallery_array[2]['caption']; ?></span> </div>
        </a>
      </div>
    </div>
    <div class="row row-image-block" data-aos="fade-up">
      <div class="col-48 gutters mb-3 choco-gallery">
        <a class="chocolat-image ms-lightbox-a" href="<?php echo $image_gallery_array[3]['url']; ?>">
          <img class="img-fluid img-fit  w-100" src="<?php echo $image_gallery_array[3]['url']; ?>" />
          <div class="bx-lightbox-overlay d-flex align-items-center justify-content-center"> <span
              class="ms-overlay-plus"></span> <span
              class="choco-title"><?php echo $image_gallery_array[3]['caption']; ?></span></div>
        </a>
      </div>
    </div>
    <div class="row row-image-block choco-gallery" data-aos="fade-up">
      <div class="col-48 col-lg-24 choco-gallery">
        <div class="row" data-aos="fade-up">
          <div class="col-48 gutters mb-3">
            <a class="chocolat-image ms-lightbox-a" href="<?php echo $image_gallery_array[4]['url']; ?>">
              <img class="img-fluid img-fit" src="<?php echo $image_gallery_array[4]['url']; ?>" />
              <div class="bx-lightbox-overlay d-flex align-items-center justify-content-center"> <span
                  class="ms-overlay-plus"></span> <span
                  class="choco-title"><?php echo $image_gallery_array[4]['caption']; ?></span></div>
            </a>
          </div>
        </div>
        <div class="row" data-aos="fade-up">
          <div class="col-48 gutters mb-3">
            <a class="chocolat-image ms-lightbox-a" href="<?php echo $image_gallery_array[5]['url']; ?>">
              <img class="img-fluid img-fit" src="<?php echo $image_gallery_array[5]['url']; ?>" />
              <div class="bx-lightbox-overlay d-flex align-items-center justify-content-center"> <span
                  class="ms-overlay-plus"></span> <span
                  class="choco-title"><?php echo $image_gallery_array[5]['caption']; ?></span></div>
            </a>
          </div>
        </div>
        <div class="row choco-gallery" data-aos="fade-up">
          <div class="col-48 gutters mb-3">
            <a class="chocolat-image ms-lightbox-a" href="<?php echo $image_gallery_array[6]['url']; ?>">
              <img class="img-fluid img-fit" src="<?php echo $image_gallery_array[6]['url']; ?>" />
              <div class="bx-lightbox-overlay d-flex align-items-center justify-content-center"> <span
                  class="ms-overlay-plus"></span>
                <span class="choco-title"><?php echo $image_gallery_array[6]['caption']; ?></span></div>
            </a>
          </div>
        </div>
      </div>
      <div class="col-48 col-lg-24 gutters mb-3">
        <a class="chocolat-image ms-lightbox-a ml-auto" href="<?php echo $image_gallery_array[7]['url']; ?>">
          <img class="img-fluid img-fit" src="<?php echo $image_gallery_array[7]['url']; ?>" />
          <div class="bx-lightbox-overlay d-flex align-items-center justify-content-center"> <span
              class="ms-overlay-plus"></span><span
              class="choco-title"><?php echo $image_gallery_array[7]['caption']; ?></span> </div>
        </a>
      </div>
    </div>
    <div class="row row-image-block choco-gallery">
      <div class="col-48 gutters mb-3" data-aos="fade-up">
        <a class="chocolat-image ms-lightbox-a" href="<?php echo $image_gallery_array[8]['url']; ?>">
          <img class="img-fluid img-fit" src="<?php echo $image_gallery_array[8]['url']; ?>" />
          <div class="bx-lightbox-overlay d-flex align-items-center justify-content-center"> <span
              class="ms-overlay-plus"></span> <span
              class="choco-title"><?php echo $image_gallery_array[8]['caption']; ?></span> </div>
        </a>
      </div>
    </div>
  </div>
</div>
<?php } ?>
<?php } ?>

<?php if ($image_gallery_count === 11) { ?> 
<div name="thelocation" id="thelocation" class="gallery-three-wrapper choco-gallery">
  <div class="container">
    <div class="row row-image-block" data-aos="fade-up">
      <div class="col-48 gutters mb-3">
        <a class="chocolat-image ms-lightbox-a" href="<?php echo $image_gallery_array[0]['url']; ?>">
          <img class="img-fluid img-fit w-100s" src="<?php echo $image_gallery_array[0]['url']; ?>" />
          <div class="bx-lightbox-overlay d-flex align-items-center justify-content-center"> <span
              class="ms-overlay-plus"></span> <span
              class="choco-title"><?php echo $image_gallery_array[0]['caption']; ?></span></div>
        </a>
      </div>
    </div>
    <div class="row row-image-block" data-aos="fade-up">
      <div class="col-48 col-lg-24">
        <div class="row">
          <div class="col-48 gutters mb-3">
            <a class="chocolat-image ms-lightbox-a" href="<?php echo $image_gallery_array[1]['url']; ?>">
              <img class="img-fluid img-fit" src="<?php echo $image_gallery_array[1]['url']; ?>" />
              <div class="bx-lightbox-overlay d-flex align-items-center justify-content-center"> <span
                  class="ms-overlay-plus"></span> <span
                  class="choco-title"><?php echo $image_gallery_array[1]['caption']; ?></span></div>
            </a>
          </div>
        </div>
        <div class="row">
          <div class="col-48 gutters mb-3">
            <a class="chocolat-image ms-lightbox-a" href="<?php echo $image_gallery_array[2]['url']; ?>">
              <img class="img-fluid img-fit" src="<?php echo $image_gallery_array[2]['url']; ?>" />
              <div class="bx-lightbox-overlay d-flex align-items-center justify-content-center"> <span
                  class="ms-overlay-plus"></span> <span
                  class="choco-title"><?php echo $image_gallery_array[2]['caption']; ?></span></div>
            </a>
          </div>
        </div>
      </div>
      <div class="col-48 col-lg-24" data-aos="fade-up">
        <div class="row">
          <div class="col-48 gutters mb-3">
            <a class="chocolat-image ms-lightbox-a" href="<?php echo $image_gallery_array[3]['url']; ?>">
              <img class="img-fluid img-fit" src="<?php echo $image_gallery_array[3]['url']; ?>" />
              <div class="bx-lightbox-overlay d-flex align-items-center justify-content-center"> <span
                  class="ms-overlay-plus"></span> <span
                  class="choco-title"><?php echo $image_gallery_array[3]['caption']; ?></span></div>
            </a>
          </div>
        </div>
        <div class="row">
          <div class="col-48 gutters mb-3">
            <a class="chocolat-image ms-lightbox-a" href="<?php echo $image_gallery_array[4]['url']; ?>">
              <img class="img-fluid img-fit gallery-adjust-img" src="<?php echo $image_gallery_array[4]['url']; ?>" />
              <div class="bx-lightbox-overlay d-flex align-items-center justify-content-center"> <span
                  class="ms-overlay-plus"></span> <span
                  class="choco-title"><?php echo $image_gallery_array[4]['caption']; ?></span></div>
            </a>
          </div>
        </div>
      </div>
    </div>
    <div class="row row-image-block" data-aos="fade-up">
      <div class="col-48 col-lg-36 gutters mb-3">
        <a class="chocolat-image ms-lightbox-a" href=" <?php echo $image_gallery_array[5]['url']; ?>">
          <img class="img-fluid img-fit w-100 " src="<?php echo $image_gallery_array[5]['url']; ?>" />
          <div class="bx-lightbox-overlay d-flex align-items-center justify-content-center"> <span
              class="ms-overlay-plus"></span> <span
              class="choco-title"><?php echo $image_gallery_array[5]['caption']; ?></span></div>
        </a>
      </div>
      <div class="col-48 col-lg-12 gutters mb-3">
        <a class="chocolat-image ms-lightbox-a" href="<?php echo $image_gallery_array[6]['url']; ?>">
          <img class="img-fluid img-fit h-100" src="<?php echo $image_gallery_array[6]['url']; ?>" />
          <div class="bx-lightbox-overlay d-flex align-items-center justify-content-center"> <span
              class="ms-overlay-plus"></span> <span
              class="choco-title"><?php echo $image_gallery_array[6]['caption']; ?></span></div>
        </a>
      </div>
    </div>
  </div>
  <div class="container">
    <div class="row row-image-block choco-gallery" data-aos="fade-up">
      <div class="col-48 col-lg-24 choco-gallery">
        <div class="row" data-aos="fade-up">
          <div class="col-48 gutters mb-3">
            <a class="chocolat-image ms-lightbox-a" href="<?php echo $image_gallery_array[7]['url']; ?>">
              <img class="img-fluid img-fit" src="<?php echo $image_gallery_array[7]['url']; ?>" />
              <div class="bx-lightbox-overlay d-flex align-items-center justify-content-center"> <span
                  class="ms-overlay-plus"></span> <span
                  class="choco-title"><?php echo $image_gallery_array[7]['caption']; ?></span></div>
            </a>
          </div>
        </div>
        <div class="row" data-aos="fade-up">
          <div class="col-48 gutters mb-3">
            <a class="chocolat-image ms-lightbox-a" href="<?php echo $image_gallery_array[8]['url']; ?>">
              <img class="img-fluid img-fit" src="<?php echo $image_gallery_array[8]['url']; ?>" />
              <div class="bx-lightbox-overlay d-flex align-items-center justify-content-center"> <span
                  class="ms-overlay-plus"></span> <span
                  class="choco-title"><?php echo $image_gallery_array[8]['caption']; ?></span></div>
            </a>
          </div>
        </div>
        <div class="row choco-gallery" data-aos="fade-up">
          <div class="col-48 gutters mb-3">
            <a class="chocolat-image ms-lightbox-a" href="<?php echo $image_gallery_array[9]['url']; ?>">
              <img class="img-fluid img-fit" src="<?php echo $image_gallery_array[9]['url']; ?>" />
              <div class="bx-lightbox-overlay d-flex align-items-center justify-content-center"> <span
                  class="ms-overlay-plus"></span>
                <span class="choco-title"><?php echo $image_gallery_array[9]['caption']; ?></span></div>
            </a>
          </div>
        </div>
      </div>
      <div class="col-48 col-lg-24 gutters mb-3">
        <a class="chocolat-image ms-lightbox-a ml-auto" href="<?php echo $image_gallery_array[10]['url']; ?>">
          <img class="img-fluid img-fit" src="<?php echo $image_gallery_array[10]['url']; ?>" />
          <div class="bx-lightbox-overlay d-flex align-items-center justify-content-center"> <span
              class="ms-overlay-plus"></span><span
              class="choco-title"><?php echo $image_gallery_array[10]['caption']; ?></span> </div>
        </a>
      </div>
    </div>
  </div>
</div>
<?php } ?>
<?php }
