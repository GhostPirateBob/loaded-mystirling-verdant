<?php 
$image = get_sub_field( 'image' );
if ( get_sub_field( 'image' ) ) {
  $image = $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['SERVER_NAME'] . $image['url'];
?>
  <div class="container">
  <div class="row row-image-block">
    <div class="col-48 gutters py-4" data-aos="fade-up"> 
      <img class="img-fluid img-fit mh-80 mb-3" src="<?php echo $image; ?>" />
      <?php if ( get_sub_field( 'caption' ) ) { ?>
      <p class="text-white"> <?php echo strip_tags( get_sub_field( 'caption' ) , '<span>'); ?></p> 
      <?php } ?>
    </div>
  </div>
</div> 
<?php }
