<?php 
$enable_parallax = get_sub_field('enable_parallax');
if ( get_sub_field( 'image' ) ) {
  $image = get_sub_field( 'image' );
  $image = $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['SERVER_NAME'] . $image['url'];
?>
<div class="row">
  <?php if ( get_sub_field('enable_parallax') ) {
    if ( $enable_parallax === 'yes' ) { ?>
  <div class="col-48 jarallax mh-66 fw-image-block fw-image-block-jarallax">
    <img class="img-fluid img-fit w-100 jarallax-img" src="<?php echo $image; ?>" />
  </div>
  <?php
    }
  }

  if ( get_sub_field('enable_parallax') === false || $enable_parallax === 'no' ) { ?>
  <div class="col-48">
    <img class="img-fluid img-fit w-100 fw-image-block" src="<?php echo $image; ?>" />
  </div>
  <?php } ?>
</div>
<?php
}
