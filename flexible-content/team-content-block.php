<div class="container">
  <div class="row row-team-block pb-5 justify-content-between align-items-start">
  <?php if ( have_rows( 'team_content_items' ) ) : ?>
    <?php while ( have_rows( 'team_content_items' ) ) : the_row(); ?>
      <?php if ( have_rows( 'team_content_item' ) ) : ?>
        <?php while ( have_rows( 'team_content_item' ) ) : the_row(); ?>
        <?php if ( get_sub_field('image') ) {
            $content_item_image = $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['SERVER_NAME'] . get_sub_field('image');
          } else {
            $content_item_image = false;
          } ?>
          <div data-aos="fade-up" class="col-48 col-md-38 col-lg-24 col-xl-21 gutters">
            <?php if ( get_sub_field('heading') ) { ?>
            <h3 class="text-white mb-1"> <?php echo strip_tags(get_sub_field('heading'), '<span>'); ?> </h3>
            <?php } ?>
            <?php if ( get_sub_field('subheading') ) { ?>
            <h6 class="text-muted text-uppercase mb-4"> <?php echo strip_tags(get_sub_field('subheading'), '<span>'); ?> </h6>
            <?php } ?>
            <span class="sep-content mb-3"></span>
            <?php if ( $content_item_image ) { ?>
            <img class="img-fluid d-block mb-5" src="<?php echo $content_item_image; ?>" />
            <?php } ?>
            <?php if ( get_sub_field('content') ) { ?>
            <div class="team-content d-block">
              <?php echo strip_tags(get_sub_field('content'), '<span>'); ?>
            </div>
            <?php } ?> 
          </div>
        <?php endwhile; ?>
      <?php endif; ?>
    <?php endwhile; ?>
  <?php endif; ?>
  </div>
</div>