<?php 
$image = get_sub_field( 'image' );
if ( get_sub_field( 'image' ) ) {
  $image = $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['SERVER_NAME'] . $image['url'];
?>
<div class="container">
  <div class="row px-5 px-lg-0">
    <div class="col-48 col-lg-34 mx-auto mb-5" data-aos="fade-up">
      <img class="img-fluid img-fit" src="<?php echo $image; ?>" />
    </div>
  </div>
  </div>
<?php
}
