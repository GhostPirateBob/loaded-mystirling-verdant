
<div class="container">
  <div class="row">
    <div class="offset-text-content-wrapper col-48 col-xl-23 ml-auto mr-auto mb-5 d-flex flex-column justify-content-center" data-aos="fade-up">
    <?php if (have_rows('short_text_content_group')) { ?>
      <?php while (have_rows('short_text_content_group')) {
         the_row();
         if ( get_sub_field('content_field') ) {
          $subheading_classes = array('mb-4', 'heading-contain');
        } else {
          $subheading_classes = array('mb-0', 'heading-contain');
        }
        $content_field = get_sub_field('content_field'); ?>
      <?php if (have_rows('heading_field')) { ?>
        <?php while (have_rows('heading_field')) {
          the_row();
          $heading_color = get_sub_field('heading_color');
          $heading_classes = array('home-headline', 'p-0', 'pl-md-0', 'heading-contain');
          if ( $heading_color !== false ) {
            $heading_classes[] = 'text-' . $heading_color;
          }
          if ( get_page_template_slug() === 'page-templates/page-gallery.php' ) { 
            $heading_classes[] = 'mb-0';
          } else {
            $heading_classes[] = 'mb-5';
          }
        ?>
      <?php if ( get_page_template_slug() === 'page-templates/page-home.php' ) { ?>
        <div class="homepage-content-wrapper">
      <?php } ?>
      <?php if ( get_sub_field('heading') ) { ?>
          <div class="row">
            <div class="col-48 gutters">
              <?php if ( get_page_template_slug() === 'page-templates/page-gallery.php' ) { ?>
              <h3 class="<?php foreach ( $heading_classes as $heading_class ) { echo ' ' . $heading_class; } ?>">
                <?php echo strip_tags(get_sub_field('heading'), '<span>'); ?>
              </h3>
              <?php } else { ?>
              <h2 class="<?php foreach ( $heading_classes as $heading_class ) { echo ' ' . $heading_class; } ?>">
                <?php echo strip_tags(get_sub_field('heading'), '<span>'); ?>
              </h2>
              <?php } ?>
            </div>
          </div>
          <?php } ?>
        <?php } ?>
      <?php } ?>
      <?php 
      if ( have_rows('subheading_field') ) { 
        while (have_rows('subheading_field')) {
        the_row();
        if ( get_sub_field('subheading') || $content_field ) {
          $subheading_classes[] = 'mw-lg-38';
      ?>
      <div class="row">
        <div class="col-48 gutters">
        <?php if ( get_sub_field('subheading') ) { ?>
          <h4 class="<?php foreach ( $subheading_classes as $subheading_class ) { echo ' ' . $subheading_class; } ?>">
            <?php echo strip_tags(get_sub_field('subheading'), '<span>'); ?>
          </h4>
        <?php } ?>
        <?php if ( $content_field ) { 
          $content_classes = array('mb-0', 'heading-contain');
          $content_classes[] = 'mw-lg-38';
        ?>
          <p class="<?php foreach ( $content_classes as $content_class ) { echo ' ' . $content_class; } ?>">
            <?php echo $content_field; ?>
          </p>
        <?php } ?>
        </div>
      </div>
      <?php if ( get_page_template_slug() === 'page-templates/page-home.php' ) { ?>
      </div>
      <?php } ?>
    </div>
<?php 
        }
      }
    }
  }
}
?>
<?php if (have_rows('image_group')): ?>
  <?php while (have_rows('image_group')): the_row(); ?>
    <div class="col-48 col-xl-25 col-offset-image gutters d-flex align-items-start" data-aos="fade-up">
    <?php if (get_sub_field('image_src') === 'wp') { ?>
      <?php $wp_image = get_sub_field('wp_image'); ?>
      <?php if ($wp_image) { ?>
        <img class="offset-image img-fluid" src="<?php echo $wp_image['url']; ?>" alt="<?php echo $wp_image['alt']; ?>" />
      <?php } ?>
    <?php } ?>
    <?php if (get_sub_field('image_src') === 'url') { ?>
      <img class="offset-image img-fluid" src="<?php echo strip_tags(get_sub_field('image_url'), '<span>'); ?>" alt="<?php echo $wp_image['alt']; ?>" />
    <?php } ?>
    <?php if (get_sub_field('image_src') === 'svg') { ?>
      <?php echo get_sub_field('image_svg'); ?>
    <?php } ?>
    </div>
  <?php endwhile; ?>
<?php endif; ?>
  </div>
</div>
