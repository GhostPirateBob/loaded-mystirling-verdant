<?php
if ( get_sub_field( 'image_map_type' ) ) {

$building_svg_map = get_field('building_svg_map', 'option');
$floorplates_svg_map = get_field('floorplates_svg_map', 'option');
$location_svg_map = get_field('location_svg_map', 'option');
$amenities_svg_map =  get_field('amenities_svg_map', 'option');

$svg_map_array['building']['shortcode'] = 'building';
$svg_map_array['building']['name'] = $building_svg_map['label'];
$svg_map_array['building']['id'] = intval($building_svg_map['value']);
$svg_map_array['floorplates']['shortcode'] = 'floorplates';
$svg_map_array['floorplates']['name'] = $floorplates_svg_map['label'];
$svg_map_array['floorplates']['id'] = intval($floorplates_svg_map['value']);
$svg_map_array['location']['shortcode'] = 'location';
$svg_map_array['location']['name'] = $location_svg_map['label'];
$svg_map_array['location']['id'] = intval($location_svg_map['value']);
$svg_map_array['amenities']['shortcode'] = 'amenities';
$svg_map_array['amenities']['name'] = $amenities_svg_map['label'];
$svg_map_array['amenities']['id'] = intval($amenities_svg_map['value']);

$svg_image_map_type = get_sub_field( 'image_map_type' ); 
$svg_image_map_options = get_option('image-map-pro-wordpress-admin-options');
$svg_image_map_meta = json_decode(json_encode($svg_image_map_options), true);
$svg_image_map_meta_saves = $svg_image_map_meta['saves'];
$floorplates_save = json_decode(stripcslashes($svg_image_map_meta_saves[$svg_map_array['floorplates']['id']]['json']));
$floorplates_layers = $floorplates_save->layers->layers_list;
$floorplates_levels = array();
foreach ( $floorplates_layers as $floorplate_layer ) {
  $floorplates_levels[$floorplate_layer->id]['id'] = $floorplate_layer->id;
  $floorplates_levels[$floorplate_layer->id]['title'] = $floorplate_layer->title;
  $floorplates_levels[$floorplate_layer->id]['image_url'] = $floorplate_layer->image_url;
  $floorplates_levels[$floorplate_layer->id]['image_width'] = $floorplate_layer->image_width;
  $floorplates_levels[$floorplate_layer->id]['image_height'] = $floorplate_layer->image_height;
}
  foreach ( $svg_image_map_meta_saves as $id => $save ) {
    $decoded_save = sanitize_json_for_save($save);
    $save_object = json_decode(stripcslashes($save['json']), true);
    if ( $save_object['id'] === $svg_map_array[$svg_image_map_type]['id'] ) {
      if ( $svg_image_map_type === 'location' ) { ?>
<div id="aerial" name="aerial" class="container">
  <div class="row row-svg-image-map row-svg-image-map-location" data-aos="fade-up">
    <div class="col-48 col-svg-image-map col-svg-image-map-location mb-5">
      <?php  echo do_shortcode('[' . $save_object['general']['shortcode'] . ']'); ?> 
    </div>
  </div>
</div>
<div class="container">
  <div class="row">
    <div class="col-48 gutters">
      <ul class="list-unstyled list-columns px-2 px-lg-4"> 
<?php
    $location_svg_image_map_meta = $save['json'];
    $location_svg_image_map_meta = json_decode(stripcslashes($location_svg_image_map_meta), true);
    $location_svg_image_map_locations = $location_svg_image_map_meta['spots'];
      foreach ($location_svg_image_map_locations as $location_meta_id => $location_data) {
        $location_shape_id = $location_data['id'];
        $location_id = $location_meta_id + 1;
        $location_menu_order = str_pad( (int) $location_id, 2, "0", STR_PAD_LEFT );
        $location_title = $location_data['title'];
        $location_text_color = $location_data['text']['text_color']; ?>
        <li class="list-item list-item-location text-truncate mb-1 px-2 px-lg-3"
          data-item-color="<?php echo $location_text_color; ?>"
          data-menu-order="<?php echo $location_menu_order; ?>"
          data-shape-id="<?php echo $location_shape_id; ?>">
          <span class="list-item-location-count d-inline-block mr-3"><?php echo $location_menu_order; ?></span>
          <span class="d-inline-block"><?php echo $location_title; ?></span>
        </li>
<?php } ?>
      </ul>
    </div>
  </div>
  <script type="text/javascript">
  ;document.addEventListener( "DOMContentLoaded", function() { 
    (function ($, window, document, undefined ) {
      $(document).ready(function() {
        setTimeout(function() {
          var imageMapID = <?php echo $id; ?>;
          var settings = <?php echo $decoded_save['json']; ?>;
          $('#image-map-pro-'+imageMapID).imageMapPro(settings);
        }, 10);
      });
    })(jQuery, window, document);
  });
  </script>
</div>

<?php } ?>
<?php if ( $svg_image_map_type === 'amenities' ) { ?>
  <div id="location" name="location" class="container-fluid location-four-wrapper svg-wrapper-two py-4 py-lg-6">
    <div class="row align-items-center justify-content-center" data-aos="fade-right">
      <div class="col-48 col-xl-16 gutters d-flex align-items-start justify-content-between flex-column">
        <div class="d-flex flex-column justify-content-start align-items-start ml-auto mr-auto">
<?php 
$amenities_array = $save_object['layers']['layers_list'];
$i = 0; 
foreach ( $amenities_array as $amenities_id => $amenities_item ) { ?>
          <button class="btn btn-light bg-none btn-amenities-select-level text-muted text-uppercase mb-4 mt-4" 
            data-floor-number="<?php echo $amenities_id; ?>"
            data-floor-title="<?php echo $amenities_item['title']; ?>">
          <?php echo $amenities_item['title']; ?> </button>
<?php $i++; } ?>
        </div>
      </div>
      <div id="amentitiesImageMap" class="col-48 col-xl-32" data-aos="fade-left" <?php
      $image_url = $amenities_array[0]['image_url'];
      $image_width = $amenities_array[0]['image_width'];
      $image_height = $amenities_array[0]['image_height'];
      ?> style="min-height: <?php echo $image_height; ?>px;">
        <?php echo do_shortcode('[' . $save_object['general']['shortcode'] . ']'); ?>
      </div>
    </div>
  </div>
  <script type="text/javascript">
  ;document.addEventListener( "DOMContentLoaded", function() { 
    (function ($, window, document, undefined ) {
      $(document).ready(function() {
        setTimeout(function() {
          var imageMapID = <?php echo $id; ?>;
          var settings = <?php echo $decoded_save['json']; ?>;
          $('#image-map-pro-'+imageMapID).imageMapPro(settings);
        }, 10);
      });
    })(jQuery, window, document);
  });
  </script>
<?php } ?>
<?php if ( $svg_image_map_type === 'building' ) {

$building_element_colors = get_sub_field( 'building_element_colors' ); 
$building_name = $save_object['general']['name'];
$building_shortcode = $save_object['general']['shortcode'];
$building_width = $save_object['general']['width'];
$building_height = $save_object['general']['height'];
$building_layers = $save_object['spots'];
$building_img = $save_object['image']['url'];
$building_levels = apartments_level_query();
ksort($building_levels);
$floors = array();
foreach ( $building_levels as $menu_order => $building_level ) {
  $level_poly_id = $building_level['poly_id'];
  $floors[$level_poly_id]['poly_id'] = $building_level['poly_id'];
  $floors[$level_poly_id]['poly_title'] = $building_level['poly_title'];
  $floors[$level_poly_id]['post_title'] = $building_level['post_title'];
  $floors[$level_poly_id]['menu_order'] = $building_level['menu_order'];
  $floors[$level_poly_id]['floorplates_svg_title'] = $building_level['floorplates_svg_title'];
  $floors[$level_poly_id]['floorplates_svg_id'] = intval($building_level['floorplates_svg_id']);
  $floors[$level_poly_id]['pdf'] = $building_level['pdf'];
}
$floors = array_reverse($floors);
?>

<div id="floorplanBuilding" class="wrapper wrapper-floorplan-building">
  <div class="container container-floorplan-building">
    <div class="row row-floorplan-building py-5 py-lg-6 align-items-start justify-content-start">
      <div class="col-48">
        <div class="row">
          <div class="col-md-10 col-48 gutters mb-3 d-flex align-items-center justify-content-center">
            <div class="btn-group btn-group-floorplan-select-level btn-group-vertical d-none d-md-block w-100"
              role="group" aria-label="Select Building Level">
            <?php foreach ( $floors as $floor_id => $floor ) { ?>
              <button type="button" class="btn btn-light btn-floorplan-level btn-floorplan-select-level <?php
              if ( get_sub_field('building_element_colors') ) { echo 'building-elements-' . $building_element_colors; } ?>"
                data-floor-id="<?php echo $floor_id; ?>"
                data-poly-id="<?php echo $floor['poly_id']; ?>"
                data-poly-title="<?php echo $floor['poly_title']; ?>"
                data-post-title="<?php echo $floor['post_title']; ?>"
                data-menu-order="<?php echo $floor['menu_order']; ?>"
                data-layer-title="<?php echo $floor['floorplates_svg_title']; ?>"
                data-layer-id="<?php echo $floor['floorplates_svg_id']; ?>"
                data-floor-pdf="<?php echo htmlentities($floor['pdf'], ENT_QUOTES | ENT_IGNORE, "UTF-8"); ?>"> <?php echo $floor['post_title']; ?> </button>
            <?php } ?>
            </div>
          </div>
          <div class="col-48 col-md-38 px-lg-5 gutters d-flex align-items-center justify-content-center <?php
              if ( get_sub_field('building_element_colors') ) { echo 'building-elements-' . $building_element_colors; } ?>">
            <?php echo file_get_contents($themePath . '/img/barque.svg'); ?>
            <?php // echo do_shortcode('[' . $save_object['general']['shortcode'] . ']'); ?> 
          </div>
        </div>

        <?php /*
        <div class="row">
          <div class="col-48 gutters mt-4 d-block d-md-none">
            <div id="accordianId" role="tablist" aria-multiselectable="true">
              <div class="card bg-dark text-white">
                <div class="card-header m-0" role="tab" id="section1HeaderId">
                  <button data-toggle="collapse" data-parent="#accordianId" href="#section1ContentId"
                    aria-expanded="true" aria-controls="section1ContentId"
                    class="btn btn-outline-primary collapse-toggle p-3 w-100 m-0" type="button" id="dropdownMenuButton">
                    Select Building Level <?php echo file_get_contents($themePath . '/img/chevron-down-sm.svg'); ?>
                  </button>
                </div>
                <div id="section1ContentId" class="collapse in" role="tabpanel" aria-labelledby="section1HeaderId">
                  <div class="card-body m-0 row btn-group btn-group-floorplan-select-level btn-group-horizontal w-100">
                  <?php foreach ( $floors as $floor_id => $floor ) { ?>
                    <button type="button" class="text-center btn-light btn col-24 col-md-16 col-lg-12 btn-floorplan-level btn-floorplan-select-level"
                      data-floor-number="<?php echo $floor_id; ?>"><?php echo $floor['title']; ?></button>
                  <?php } ?>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        */  ?>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
;document.addEventListener( "DOMContentLoaded", function() { 
  (function ($, window, document, undefined ) {
    $(document).ready(function() {
      setTimeout(function() {
        var imageMapID = <?php echo $id; ?>;
        var settings = <?php echo $decoded_save['json']; ?>;
        $('#image-map-pro-'+imageMapID).imageMapPro(settings);
      }, 10);
    });
  })(jQuery, window, document);
});
</script>
<?php

}

if ( $svg_image_map_type === 'floorplates' ) {

$svg_map_name = $save_object['general']['name'];
$svg_map_shortcode = $save_object['general']['shortcode'];
$svg_map_width = $save_object['general']['width'];
$svg_map_height = $save_object['general']['height'];
$svg_map_naturalWidth = $save_object['general']['naturalWidth'];
$svg_map_naturalHeight = $save_object['general']['naturalHeight'];
$svg_map_enable_layers = $save_object['layers']['enable_layers'];
$svg_map_layers = $save_object['layers']['layers_list'];

$svg_map_img = $save_object['image']['url'];
$svg_map_enable_tooltips = $save_object['tooltips']['enable_tooltips'];
$level_heights = array();

foreach ( $svg_map_layers as $layer ) {
  $layer_id = $layer['id'];
  $levels[$layer_id]['id'] = $layer_id;
  $levels[$layer_id]['title'] = $layer['title'];
  $levels[$layer_id]['image_url'] = $layer['image_url'];
  $levels[$layer_id]['image_width'] = $layer['image_width'];
  $levels[$layer_id]['image_height'] = $layer['image_height'];
  $level_heights[] = $layer['image_height'];
}

asort($level_heights);
$level_heights = array_reverse($level_heights);

$floorplate_element_colors = get_sub_field( 'floorplate_element_colors' ); 
$apartment_element_colors = get_sub_field( 'apartment_element_colors' ); 
$amenities_element_colors = get_sub_field( 'amenities_element_colors' ); 
$apartment_wrapper_classes = array();

if (  get_sub_field( 'field_5d2446b4e4131' ) ) {
  $apartment_section_container =  get_sub_field( 'field_5d2446b4e4131' ); // apartment section container
  if ($apartment_section_container['section_container']['background_color']) {
    $apartment_wrapper_classes[] = $apartment_section_container['section_container']['background_color'];
  }
  if ( $apartment_section_container['section_container']['padding_top'] && $apartment_section_container['section_container']['padding_top'] !== 'none' ) {
    $padding_top = $apartment_section_container['section_container']['padding_top'];
    if ( $padding_top === 'sm' ) {
      $apartment_wrapper_classes[] = 'pt-3';
    }
    if ( $padding_top === 'md' ) {
      $apartment_wrapper_classes[] = 'pt-5';
    }
    if ( $padding_top === 'lg' ) {
      $apartment_wrapper_classes[] = 'pt-6';
    }
    if ( $padding_top === 'xl' ) {
      $apartment_wrapper_classes[] = 'pt-6';
      $apartment_wrapper_classes[] = 'pt-lg-7';
    }
  }
  if ( $apartment_section_container['section_container']['padding_bot'] && $apartment_section_container['section_container']['padding_bot'] !== 'none' ) {
    $padding_top = $apartment_section_container['section_container']['padding_bot'];
    if ( $padding_top === 'sm' ) {
      $apartment_wrapper_classes[] = 'pb-3';
    }
    if ( $padding_top === 'md' ) {
      $apartment_wrapper_classes[] = 'pb-5';
    }
    if ( $padding_top === 'lg' ) {
      $apartment_wrapper_classes[] = 'pb-6';
    }
    if ( $padding_top === 'xl' ) {
      $apartment_wrapper_classes[] = 'pb-6';
      $apartment_wrapper_classes[] = 'pb-lg-7';
    }
  }
}
?>
<div class="wrapper wrapper-floorplates wrapper-floorplans"
  data-primary-color="<?php the_field('secondary', 'option'); ?>"
  data-secondary-color="<?php the_field('primary', 'option'); ?>" >
  <div class="container container-floorplans">
    <div class="row">
      <div class="col-48 gutters">
        <button id="backToTopFloorplans" type="button"
          class="btn btn-link btn-light btn-back-to-top btn-back-to-top-apartment mb-3 <?php
          if ( get_sub_field('floorplate_element_colors') ) { echo 'floorplate-elements-' . $floorplate_element_colors; } ?>"> 
          <?php echo file_get_contents( $themePath . '/img/back-to-top-apartment.svg'); ?></button>
      </div>
    </div>
    <div class="row row-floorplans align-items-center justify-content-center">
      <div class="col-auto col-floorplans gutters order-2 order-md-1" style="min-height: <?php echo $level_heights[0]; ?>px;"> 
        <?php echo do_shortcode('[' . $save_object['general']['shortcode'] . ']'); ?> 
      </div>
      <div id="compass-deco" class="col-48 d-flex justify-content-start justify-content-md-end order-1 order-md-2 mb-3">
        <div class="deco-compass <?php
          if ( get_sub_field('floorplate_element_colors') ) { echo 'floorplate-elements-' . $floorplate_element_colors; } ?>"> 
          <?php echo file_get_contents( $themePath . '/img/location-compass.svg'); ?>
        </div>
      </div>
      <div
        class="col col-apartment-disclaimer d-flex justify-content-start align-items-start align-items-md-center flex-column flex-md-row order-1 order-md-2 gutters">
        <h4 class="text-uppercase mt-3 mb-1 d-inline <?php
          if ( get_sub_field('floorplate_element_colors') ) { echo 'floorplate-elements-' . $floorplate_element_colors; } ?>"> 
          <span id="levelNumber"></span> Floorplate 
        </h4>
        <span class="vertical-sep d-none d-md-inline-block mx-3 <?php
          if ( get_sub_field('floorplate_element_colors') ) { echo 'floorplate-elements-' . $floorplate_element_colors; } ?>"></span>
        <h6 class="text-lowercase mb-3 d-inline <?php
          if ( get_sub_field('floorplate_element_colors') ) { echo 'floorplate-elements-' . $floorplate_element_colors; } ?>"> 
          (click on apartment to view plan) 
        </h6>
      </div>
      <div
        class="col-auto col-apartment-disclaimer d-flex justify-content-start align-items-center order-2 order-md-3 gutters">
        <h6 class="text-no-upper mb-0 <?php
          if ( get_sub_field('floorplate_element_colors') ) { echo 'floorplate-elements-' . $floorplate_element_colors; } ?>">
          <small>Please Note: Plans are not to scale</small>
        </h6>
      </div>
    </div>
  </div>
</div>
</div>
<div id="apartmentInfo" class="collapse collapse-apartment-info">
  <div class="wrapper wrapper-apartment-info">
    <div id="apartmentsWrapper "class="wrapper<?php foreach ( $apartment_wrapper_classes as $apartment_wrapper_class ) { 
      echo ' ' . $apartment_wrapper_class; } ?>">
      <div class="container container-apartment-info">
        <div class="row">
          <div class="col-48 col-lg-22 col-apartment-info gutters">
            <div class="row row-apartment-info">
              <div class="col-48 col-apartment-info gutters">
                <button id="backToTopApartmentInfo" type="button"
                  class="btn btn-link btn-light btn-back-to-top btn-back-to-top-info mb-3 <?php
            if ( get_sub_field('apartment_element_colors') ) { echo 'apartment-elements-' . $apartment_element_colors; } ?>">
                  <?php echo file_get_contents( $themePath . '/img/back-to-top-info.svg'); ?> </button>
              </div>
              <div class="col-48 col-apartment-info gutters">
                <div class="row row-apartment-info-inner inner-heading">
                  <div class="col-48 gutters mt-3 mb-3">
                    <h2 class="mb-0 <?php
            if ( get_sub_field('apartment_element_colors') ) { echo 'apartment-elements-' . $apartment_element_colors; } ?>"> Apartment <span class="data-apartment-number"></span></h2>
                    <h2 class="mb-0 <?php
            if ( get_sub_field('apartment_element_colors') ) { echo 'apartment-elements-' . $apartment_element_colors; } ?>"> Type <span id="data-apartment-plan"
                        class="text-uppercase"></span></h2>
                  </div>
                </div>
                <div class="col-48 gutters mb-3 d-block d-md-none">
                  <img class="img-fluid img-apartment-img data-apartment-image mt-3 mb-3" src="" />
                </div>
                <div class="row row-apartment-info-inner">
                  <div class="col-48 gutters mt-3 mb-3">
                    <h4 class="mb-0 <?php
            if ( get_sub_field('apartment_element_colors') ) { echo 'apartment-elements-' . $apartment_element_colors; } ?>"> <span id="data-apartment-level"></span> </h4>
                    <h4 class="mb-0 <?php
            if ( get_sub_field('apartment_element_colors') ) { echo 'apartment-elements-' . $apartment_element_colors; } ?>">
                      <span id="data-apartment-beds"></span> bed ＋ <span id="data-apartment-baths"></span> bath
                      <span class="data-apartment-cars-wrapper d-none">＋ <span id="data-apartment-cars"></span>
                        car bay</span>
                    </h4>
                  </div>
                  <div class="col-48 col-left-pad gutters mt-3 mb-3">
                    <p class="apartment-leftpad-item <?php
            if ( get_sub_field('apartment_element_colors') ) { echo 'apartment-elements-' . $apartment_element_colors; } ?>">Total Living Area<span
                        id="data-apartment-total-area"></span></p>
                    <p class="apartment-leftpad-item <?php
            if ( get_sub_field('apartment_element_colors') ) { echo 'apartment-elements-' . $apartment_element_colors; } ?>">Internal Architectural Area<span
                        id="data-apartment-internal-architectural-area"></span></p>
                    <p class="apartment-leftpad-item <?php
            if ( get_sub_field('apartment_element_colors') ) { echo 'apartment-elements-' . $apartment_element_colors; } ?>">Balcony Architectural Area<span
                        id="data-apartment-balcony-architectural-area"></span></p>
                    <p class="apartment-leftpad-item <?php
            if ( get_sub_field('apartment_element_colors') ) { echo 'apartment-elements-' . $apartment_element_colors; } ?>">Internal Strata Area<span
                        id="data-apartment-internal-strata-area"></span></p>
                    <p class="apartment-leftpad-item <?php
            if ( get_sub_field('apartment_element_colors') ) { echo 'apartment-elements-' . $apartment_element_colors; } ?>">Balcony Strata Area<span
                        id="data-apartment-balcony-strata-area"></span></p>
                    <p class="apartment-leftpad-item <?php
            if ( get_sub_field('apartment_element_colors') ) { echo 'apartment-elements-' . $apartment_element_colors; } ?>">Store Area<span id="data-apartment-store"></span></p>
                  </div>
                  <div class="col-48 gutters mt-3 mb-0">
                    <a href="" id="data-apartment-floorplan-url" class="btn btn-chevron-down-sm <?php
            if ( get_sub_field('apartment_element_colors') ) { echo 'apartment-elements-' . $apartment_element_colors; } ?>" target="_blank">
                      <span class="btn-chevron-text text-uppercase">Download Floorplan</span>
                      <?php echo file_get_contents($themePath . '/img/chevron-down-sm.svg'); ?> </a>
                  </div>
                  <div class="col-48 gutters mt-0 mb-3">
                    <a href="" id="data-apartment-floorplate-url" class="btn btn-chevron-down-sm <?php
            if ( get_sub_field('apartment_element_colors') ) { echo 'apartment-elements-' . $apartment_element_colors; } ?>" target="_blank">
                      <span class="btn-chevron-text text-uppercase">Download Floorplates</span>
                      <?php echo file_get_contents($themePath . '/img/chevron-down-sm.svg'); ?> </a>
                  </div>
                  <div class="col-48 gutters mt-0 mb-3 d-none <?php
            if ( get_sub_field('apartment_element_colors') ) { echo 'apartment-elements-' . $apartment_element_colors; } ?>">
                    <a href="<?php echo $siteURL; ?>/wp-content/uploads/Verdant-Download-Package.pdf"
                      class="btn btn-chevron-down-sm" target="_blank">
                      <span class="btn-chevron-text text-uppercase">Download Specifications</span>
                      <?php echo file_get_contents($themePath . '/img/chevron-down-sm.svg'); ?> </a>
                  </div>
                  <div class="col-48 gutters mt-3 mb-3">
                    <p class="mb-0 disclaimer-text <?php
            if ( get_sub_field('apartment_element_colors') ) { echo 'apartment-elements-' . $apartment_element_colors; } ?>">General Notes: Variations from strata plan may
                      apply. Furniture is indicative only. Shape and configuration of balconies, doors, windows,
                      condensers and ducts may differ from those illustrated. Areas of apartment given are based
                      on architectural measurements. This varies from Strata areas as it is a different method of
                      measuring the apartment.</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div
            class="col-48 col-lg-26 d-flex align-items-center justify-content-center col-apartment-image gutters">
            <img class="img-fluid img-apartment-img data-apartment-image mt-3 mb-3 d-none d-md-block" src="" />
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
;document.addEventListener( "DOMContentLoaded", function() { 
  (function ($, window, document, undefined ) {
    $(document).ready(function() {
      setTimeout(function() {
        var imageMapID = <?php echo $id; ?>;
        var settings = <?php echo $decoded_save['json']; ?>;
        $('#image-map-pro-'+imageMapID).imageMapPro(settings);
      }, 10);
    });
  })(jQuery, window, document);
});
</script>
</div>
<?php if ( get_sub_field('download_pdf_package') ) { ?>
<div class="apartment-cta-wrapper bg-secondary py-4 py-lg-5">
  <div class="container">
    <div class="row align-items-baseline justify-content-between px-4 px-md-0">
      <div class="col-auto gutters mt-4 mb-4" data-aos="fade-up">
        <h3 class="text-white d-inline-block mb-0 mr-4"> Download Apartment Package </h3>
        <h4 class="text-white d-inline-block mb-0"><small> Includes Apartment Plans and Floorplates </small></h4>
      </div>
      <div class="col-auto gutters mt-4 mb-4" data-aos="fade-up">
        <a type="link" class="btn btn-info bg-info py-3 px-4"
          href="<?php echo get_sub_field('download_pdf_package'); ?>"
          target="_blank"> Download PDF </a>
      </div>
    </div>
  </div>
</div>
<?php
        }
      }
    }
  }
}


