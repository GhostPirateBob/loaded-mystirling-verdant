<?php 
$video_cover_image = get_sub_field( 'video_cover_image' );
if ( get_sub_field( 'video_cover_image' ) && 
      get_sub_field( 'video_play_button' ) && 
      get_sub_field('video_embed_url') ) { 
?>
<div class="container">
  <div class="row">
    <div class="col-48 video-container bg-dark" data-aos="fade-up" data-video-url="<?php echo strip_tags(get_sub_field( 'video_embed_url' ), '<span>'); ?>">
      <div class="embed-container">
        <iframe id="vimeo-iframe" src="" data-vimeo-url="<?php echo strip_tags(get_sub_field('video_embed_url'), '<span>'); ?>?autoplay=1&color=24c7c4&title=0&byline=0&portrait=0" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" webkitAllowFullScreen mozallowfullscreen allow="autoplay; fullscreen" allowFullScreen></iframe>
      </div>
      <div id="overlay" class="overlay-logo-row w-100" <?php if ( $video_cover_image ) { ?>style="background-image: url('<?php echo $video_cover_image['url']; ?>');"<?php } ?>>
      <?php $video_play_button = get_sub_field( 'video_play_button' ); ?>
      <?php if ( $video_play_button ) { ?>
        <button type="button" class="btn btn-link btn-play-video p-0 m-0">
          <img src="<?php echo $video_play_button['url']; ?>" alt="<?php echo $video_play_button['alt']; ?>" />
        </button>
      <?php } ?>
      <?php $video_overlay_image = get_sub_field( 'video_overlay_image' ); ?>
      <?php if ( $video_overlay_image ) { ?>
        <div class="image-block img-fluid d-none d-xl-block">
          <div class="main-logo mt-4 mb-4 img-fluid">
            <img src="<?php echo $video_overlay_image['url']; ?>" alt="<?php echo $video_overlay_image['alt']; ?>" />
            <span class="horizontal-sep mb-4"></span>
            <p class="project-video-text justify-self-center text-uppercase mb-4 img-fluid">Project Video</p>
          </div>
        </div>
      <?php } ?>
      </div>
    </div>
  </div>
</div>
<script src="https://player.vimeo.com/api/player.js"></script>
<script>
jQuery('.btn-play-video').click(function (e) { 
  e.preventDefault();
  var vimeoURL = jQuery('#vimeo-iframe').attr('data-vimeo-url');
  jQuery('#vimeo-iframe').attr('src', vimeoURL);
  jQuery('#overlay').fadeOut();
});
</script>
<?php
  }
