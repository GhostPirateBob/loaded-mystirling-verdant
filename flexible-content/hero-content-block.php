<?php $block_count = count( get_sub_field('content_blocks') ); ?>
<div class="row row-fp-hero w-100">
  <div class="col-48 col-fp-hero jarallax">
    <?php $background_image = get_sub_field('background_image'); ?>
    <?php if ($background_image) { ?>
      <img src="<?php echo $background_image; ?>" class="img-fluid fp-hero-img fp-hero-plax jarallax-img" alt="<?php echo $siteTitle; ?>">
    <?php } ?>
    <?php if ( get_sub_field('announcement_image') ) {
      $announcement_image = get_sub_field('announcement_image'); ?>
      <div class="row row-banner-content sticker-row img-fluid aos-init aos-animate w-100" data-aos="fade">
        <div id="fp-sticker-peel-col-alt" class="col-auto gutters d-flex justify-content-start align-items-center">
          <img src="<?php echo $announcement_image['url']; ?>" class="img-fluid d-block" />
        </div>
      </div>
    <?php } ?>
  <?php if (have_rows('content_blocks')): $i = 0; ?>
    <div class="row w-100 container-fp-hero justify-content-center pt-3 pb-3">
    <?php while (have_rows('content_blocks')): the_row(); ?>
      <?php if (have_rows('image_group')): ?>
        <?php while (have_rows('image_group')): the_row(); ?>
      <div class="col-auto fp-hero-px-one d-flex justify-content-center flex-column align-items-center mt-3 mb-3 img-fluid" <?php
      if ( $block_count === 1 && $i === 0 ) {
        echo 'data-aos="fade"';
      }
      if ( $block_count === 2 && $i === 0 || $block_count === 3 && $i === 0 ) {
        echo 'data-aos="fade-down"';
      }
      if ( $block_count === 2 && $i === 1 ) {
        echo 'data-aos="fade-up"';
      }
      if ( $block_count === 3 && $i === 1 ) {
        echo 'data-aos="fade"';
      }
      if ( $block_count === 3 && $i === 2 ) {
        echo 'data-aos="fade-up"';
      }
      ?>>
      <?php if (get_sub_field('image_src') === 'wp') { ?>
        <?php 
        if (get_sub_field('wp_image')) { 
          $wp_image = get_sub_field('wp_image'); ?>
        <img class="img-fluid d-block" src="<?php echo $wp_image['url']; ?>" />
          <?php } ?>
        <?php } ?>
        <?php if (get_sub_field('image_src') === 'url') { ?>
          <?php echo strip_tags(get_sub_field('image_url'), '<span>'); ?>
        <?php } ?>
        <?php if (get_sub_field('image_src') === 'svg') { ?>
          <?php $image_svg = get_sub_field('image_svg');
          $image_svg = str_replace('<img', '<img class="img-fluid d-block"', $image_svg);
          echo $image_svg; ?>
        <?php } ?>
      </div>
          <?php endwhile; ?>
      <?php endif; ?>
    <?php $i++; endwhile; ?>
      </div>
<?php endif; ?>
  </div>
</div>
