<div class="container">
  <div class="row px-5 px-lg-0">
    <div class="col-48 col-lg-34 mx-auto mb-5 gutters">
      <div class="icons d-flex flex-wrap" data-aos="fade-up">

<?php
if ( have_rows( 'icon_repeater' ) ) {
  while ( have_rows( 'icon_repeater' ) ) { 
    the_row(); ?>
        <div class="icon-box">
    <?php
      $image = get_sub_field( 'image' );
      if ( get_sub_field( 'image' ) ) {
        $image = $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['SERVER_NAME'] . $image['url'];
    ?>
          <img src="<?php echo $image; ?>" />
    <?php } ?>
    <?php if ( get_sub_field( 'text' ) ) { ?>
          <p><?php echo strip_tags( get_sub_field( 'text' ) , '<span>'); ?></p>
    <?php } ?>
        </div>
    <?php
       }
      }
    ?>

      </div>
    </div>
  </div>
</div> 
