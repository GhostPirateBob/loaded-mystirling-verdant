<?php
$image_gallery_images = get_sub_field( 'images' );
if ( $image_gallery_images ) {
  $image_gallery_count = count($image_gallery_images);
  $image_gallery_array = array();
  $i = 0;
  foreach ( $image_gallery_images as $image_gallery_image ) {
    $image_gallery_array[$i]['url'] = $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['SERVER_NAME'] . $image_gallery_image['url'];
    $image_gallery_array[$i]['alt'] = $image_gallery_image['alt'];
    $image_gallery_array[$i]['caption'] = $image_gallery_image['caption'];
    $image_gallery_array[$i]['title'] = $image_gallery_image['title'];
    $i++;
  }
?>

<div class="container draggable-gallery-wrapper py-6 py-lg-7">
  <div class="row row-draggable-gallery">
    <div class="col-48 image-gallery-col"> 
      <div class="grid container-wrapper w-100">
        <div class="grid-sizer gutters"></div>
        <?php $gridCount = 0 ;
        foreach ( $image_gallery_array as $image ) {
          if ( $image['title'] === 'width2' ) { ?>
          <div class="grid-item gutters mb-2 mb-lg-3 grid-item--width2" data-item-id="<?php echo $gridCount; ?>">
            <img src="<?php echo $image['url']; ?>" class="img-fluid img-draggable" />
          </div>
          <?php } else { ?>
          <div class="grid-item gutters mb-2 mb-lg-3" data-item-id="<?php echo $gridCount; ?>">
            <button type="button" class="grid-swap-button" data-parent-item-id="<?php echo $gridCount; ?>"></button>
            <!-- <button type="button" class="grid-flip-button" data-parent-item-id="<?php echo $gridCount; ?>"></button> -->
            <img src="<?php echo $image['url']; ?>" class="img-fluid img-draggable" />
          </div>
          <?php } ?>
        <?php $gridCount++; } ?>
      </div>
    </div>
  </div>
  <?php if (get_sub_field( 'code' )) { ?>
  <div class="row row-draggable-gallery-code">
    <div class="col-48 gutters">
      <pre><?php echo esc_html( get_sub_field( 'code' ) ); ?></pre>
    </div>
  </div>
  <?php } ?>
</div>

<script src="https://s3-ap-southeast-2.amazonaws.com/loaded-public/loaded-static/draggableGallery/get-size.js"></script>
<script src="https://s3-ap-southeast-2.amazonaws.com/loaded-public/loaded-static/draggableGallery/matches-selector.js"></script>
<script src="https://s3-ap-southeast-2.amazonaws.com/loaded-public/loaded-static/draggableGallery/ev-emitter.js"></script>
<script src="https://s3-ap-southeast-2.amazonaws.com/loaded-public/loaded-static/draggableGallery/utils.js"></script>
<script src="https://s3-ap-southeast-2.amazonaws.com/loaded-public/loaded-static/draggableGallery/outlayerItem.js"></script>
<script src="https://s3-ap-southeast-2.amazonaws.com/loaded-public/loaded-static/draggableGallery/outlayer.js"></script>
<script src="https://s3-ap-southeast-2.amazonaws.com/loaded-public/loaded-static/draggableGallery/rect.js"></script>
<script src="https://s3-ap-southeast-2.amazonaws.com/loaded-public/loaded-static/draggableGallery/packer.js"></script>
<script src="https://s3-ap-southeast-2.amazonaws.com/loaded-public/loaded-static/draggableGallery/item.js"></script>
<script src="https://s3-ap-southeast-2.amazonaws.com/loaded-public/loaded-static/draggableGallery/packery.js"></script>
<script src="https://s3-ap-southeast-2.amazonaws.com/loaded-public/loaded-static/draggableGallery/imagesloaded.pkgd.js"></script>
<script src="https://s3-ap-southeast-2.amazonaws.com/loaded-public/loaded-static/draggableGallery/unipointer.js"></script>
<script src="https://s3-ap-southeast-2.amazonaws.com/loaded-public/loaded-static/draggableGallery/unidragger.js"></script>
<script src="https://s3-ap-southeast-2.amazonaws.com/loaded-public/loaded-static/draggableGallery/draggabilly.js"></script>

<script>
var grid = document.querySelector('.grid');
var pckry = new Packery( grid, {
  itemSelector: '.grid-item',
  columnWidth: '.grid-sizer',
  transitionDuration: '0.3s',
  shiftPercentResize: false,
  percentPosition: false,
  initLayout: false
});
var itemElems = grid.querySelectorAll('.grid-item');
for ( var i=0; i < itemElems.length; i++ ) {
  var itemElem = itemElems[i];
  var draggie = new Draggabilly( itemElem );
  pckry.bindDraggabillyEvents( draggie );
}
imagesLoaded( grid, function() {
  grid.classList.add('is-img-loaded');
  var itemPositions = localStorage.getItem( 'itemsShiftPositions' );
  if ( itemPositions ) {
    itemPositions = JSON.parse( itemPositions );
    pckry._resetLayout();
    // set item order and horizontal position from saved positions
    pckry.items = itemPositions.map( function( itemPosition ) {
      var itemElem = grid.querySelector('[data-item-id="' + itemPosition.id  + '"]');
      var item = pckry.getItem( itemElem );
      item.rect.x = parseFloat( itemPosition.x ) * pckry.packer.width;
      return item;
    });
    pckry.shiftLayout();
  } else {
    // if no initial positions, run packery layout
    pckry.layout();
  }
});

pckry.on( 'layoutComplete', savePositions );
pckry.on( 'dragItemPositioned', savePositions );

function savePositions() {
  var positions = getItemsShiftPositions();
  localStorage.setItem( 'itemsShiftPositions', JSON.stringify( positions ) );
}
function getItemsShiftPositions() {
  return pckry.items.map( function( item ) {
    return {
      id: item.element.getAttribute('data-item-id'),
      x: item.rect.x / pckry.packer.width
    }
  });
}

jQuery('.grid-swap-button').each(function (index, element) {
  jQuery(this).click(function (elem) { 
    elem.preventDefault();
    var gridX = jQuery('.grid').width();
    var windowX = jQuery('#page-wrapper').width();
    var leftOffset = ((windowX - gridX) / 2) + 80;
    var rightOffset = ((windowX - gridX) / 2);
    if ( (elem.pageX - leftOffset) <= (gridX / 2) ) {
      var currentSide = 'left';
    } else {
      var currentSide = 'right';
    }
    if ( !jQuery(this).parent().children('img').hasClass('ml-auto') && !jQuery(this).parent().children('img').hasClass('mr-auto') ) {
      jQuery(this).parent().children('img').addClass('ml-auto');
    }
    else if ( jQuery(this).parent().children('img').hasClass('ml-auto') ) {
      jQuery(this).parent().children('img').removeClass('ml-auto');
      jQuery(this).parent().children('img').addClass('mr-auto');
    }
    else if ( jQuery(this).parent().children('img').hasClass('mr-auto') ) {
      jQuery(this).parent().children('img').removeClass('mr-auto');
    }
  });
});
jQuery('.grid-flip-button').each(function (index, element) {
  jQuery(this).click(function (elem) { 
    elem.preventDefault();
    var gridX = jQuery('.grid').width();
    var windowX = jQuery('#page-wrapper').width();
    var leftOffset = ((windowX - gridX) / 2) + 80;
    var rightOffset = ((windowX - gridX) / 2);
    if ( (elem.pageX - leftOffset) <= (gridX / 2) ) {
      var currentSide = 'left';
    } else {
      var currentSide = 'right';
    }
    if ( !jQuery(this).parent().children('img').hasClass('mt-auto') && !jQuery(this).parent().children('img').hasClass('mb-auto') ) {
      jQuery(this).parent().children('img').addClass('mt-auto');
    }
    else if ( jQuery(this).parent().children('img').hasClass('mt-auto') ) {
      jQuery(this).parent().children('img').removeClass('mt-auto');
      jQuery(this).parent().children('img').addClass('mb-auto');
    }
    else if ( jQuery(this).parent().children('img').hasClass('mb-auto') ) {
      jQuery(this).parent().children('img').removeClass('mb-auto');
    }
  });
});
</script>
<?php
  }
