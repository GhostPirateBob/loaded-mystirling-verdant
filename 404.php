<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @package mystirling
 */

if ( ! defined( 'ABSPATH' ) ) {
  exit; // Exit if accessed directly.
}

get_header();

$container = get_theme_mod( 'understrap_container_type' );
?>

<div class="wrapper mh-86" id="error-404-wrapper">

  <div class="<?php echo esc_attr( $container ); ?>" id="content" tabindex="-1">

    <div class="row">

      <div class="col-md-48 content-area" id="primary">

        <main class="site-main" id="main">

          <section class="error-404 not-found">

            <header class="page-header">

              <h1 class="page-title"><?php esc_html_e( 'That page can&rsquo;t be found.', 'mystirling' ); ?></h1>

            </header>

            <div class="page-content">

              <p><?php esc_html_e( 'It looks like nothing was found at this location.', 'mystirling' ); ?></p>

            </div>

          </section>

        </main>

      </div>

    </div>

  </div>

</div>

<?php get_footer(); ?>
