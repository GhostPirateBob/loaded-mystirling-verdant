<?php
/**
 * The template for displaying search results pages.
 *
 * @package mystirling
 */

if ( ! defined( 'ABSPATH' ) ) {
  exit; // Exit if accessed directly.
}

get_header();

$container = get_theme_mod( 'understrap_container_type' );

?>

<div class="wrapper" id="search-wrapper">

  <div class="<?php echo esc_attr( $container ); ?>" id="content" tabindex="-1">

    <div class="row">

      <main class="site-main" id="main">

        <?php if ( have_posts() ) : ?>

          <?php while ( have_posts() ) : the_post(); ?>

            <?php the_content(); ?>

          <?php endwhile; ?>

        <?php endif; ?>

      </main>

    </div>

  </div>

</div>

<?php get_footer();
