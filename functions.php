<?php
/**
 * Understrap functions and definitions
 *
 * @package mystirling
 */

if ( ! defined( 'ABSPATH' ) ) {
  exit; // Exit if accessed directly.
}

add_image_size( 'fullwidth-background', 1920, 1200, array( 'center', 'center' ), false );
add_image_size( 'next-page-image', 900, 693, array( 'center', 'center' ), true );
add_image_size( 'content-item-image', 224, 64, array(), false );
add_image_size( 'announcement-sticker', 270, 270, array( 'center', 'center' ), true );
add_image_size( 'team-logo-image', 152, 34, array(), false );

add_theme_support('soil-clean-up');

add_theme_support('soil-disable-asset-versioning');
add_theme_support('soil-disable-trackbacks');

// add_theme_support('soil-jquery-cdn');
// add_theme_support('soil-js-to-footer');

add_theme_support('soil-nice-search');
add_theme_support('soil-relative-urls');
// add_theme_support('soil-disable-rest-api');
// add_theme_support('soil-nav-walker');

$understrap_includes = array(
  '/acf-level-select-field.php',
  '/enqueue.php',                         // Enqueue scripts and styles.
  '/custom.php',                          // Load custom functions.
  // '/acf-helper-functions.php',         // Load ACF helper functions.
  // '/gform-bootstrap.php',              // Gravity Forms Bootstrap 4 styling.
  '/gform-bootstrap-new.php',             // Gravity Forms Bootstrap 4 styling.
  '/theme-settings.php',                  // Initialize theme default settings.
  '/setup.php',                           // Theme setup and custom theme supports.
  // '/widgets.php',                      // Register widget area.
  // '/template-tags.php',                // Custom template tags for this theme.
  '/pagination.php',                      // Custom pagination for this theme.
  '/hooks.php',                           // Custom hooks.
  '/extras.php',                          // Custom functions that act independently of the theme templates.
  '/customizer.php',                      // Customizer additions.
  // '/custom-comments.php',              // Custom Comments file.
  '/jetpack.php',                         // Load Jetpack compatibility file.
  '/class-wp-bootstrap-navwalker.php',    // Load custom WordPress nav walker.
  '/class-soil-navwalker.php',            // Soil nav walker.
  '/editor.php',                          // Load Editor functions.
  '/deprecated.php'                       // Load deprecated functions.
);


if ( function_exists('get_field') ) {
  if ( get_field( 'filename', 'option' ) ) {
    $site_slug = strtolower(get_field( 'filename', 'option' ));
  } 
  if ( get_field( 'filename', 'option' ) === false || get_field( 'filename', 'option' ) === null ) {
    $site_slug = strtolower(basename( get_bloginfo('url') ));
  }
} else {
  $site_slug = strtolower(basename( get_bloginfo('url') ));
}

if ( $site_slug !== 'verdant' ) {
  $understrap_includes[] =   '/custom-fields-export.php';
  $understrap_includes[] = '/custom-admin-columns.php';
}

// $understrap_includes[] = '/custom-admin-columns.php';

foreach ( $understrap_includes as $file ) {
  $stylesheetPath = get_stylesheet_directory();
  $filepath = $stylesheetPath . '/inc' . $file;
  if ( ! $file ) {
    trigger_error( sprintf( 'Error locating /inc%s for inclusion', $file ), E_USER_ERROR );
  }
  require_once $filepath;
}

// ACF google map api key function
function google_map_api_key( $api ) {
  $api['key'] = 'AIzaSyBw-Ld2uyp74D05cs6QOK3CVpnKcjxJ1e0';
  return $api;
}

add_filter('acf/fields/google_map/api', 'google_map_api_key');

function testGmapStyleArray($data) {
  json_decode(' { "styles": ' . $data . ' } ');
  return (json_last_error() == JSON_ERROR_NONE);
}

function returnSVGMaps() {
  $svg_image_map_options = get_option('image-map-pro-wordpress-admin-options');
  $svg_image_map_meta = json_decode(json_encode($svg_image_map_options), true);
  $svg_image_map_meta_saves = $svg_image_map_meta['saves'];
  $svg_maps = array();
  foreach ( $svg_image_map_meta_saves as $save ) { 
    $decoded_save = sanitize_json_for_save($save);
    $decoded_save = json_decode($decoded_save['json']);
    $svg_maps[$decoded_save->id] = $decoded_save->general->name;
  }
  return $svg_maps;
}

class RecursiveArrayObject extends ArrayObject {
    function getArrayCopy()
    {
        $resultArray = parent::getArrayCopy();
        foreach($resultArray as $key => $val) {
            if (!is_object($val)) {
                continue;
            }
            $o = new RecursiveArrayObject($val);
            $resultArray[$key] = $o->getArrayCopy();
        }
        return $resultArray;
    }
}

function sanitize_json_for_save($save) {
  if (is_object($save)) {
    $saveObject = new RecursiveArrayObject($save);
    $save = $saveObject->getArrayCopy();
  }
  $json = $save['json'];

  $json = str_replace('\\\n', "", $json); // Remove new line characters inside tooltip contents
  $json = str_replace('\"', '"', $json); // Replace \" with "
  $json = str_replace("\\'", "'", $json); // Replace \' with '
  $json = str_replace('\\\"', '\"', $json); // Replace \\" with \"
  $json = str_replace('\'', '&quot;', $json); // Replace \\" with \"
  
  $decoded = json_decode($json);
  
  for ($i=0; $i<count($decoded->spots); $i++) {
    $spot = $decoded->spots[$i];

    if (isset($spot->tooltip_content->plain_text)) {
      $spot->tooltip_content->plain_text = do_shortcode($spot->tooltip_content->plain_text);

      $pattern = '/\\+"/';
      $spot->tooltip_content->plain_text = preg_replace($pattern, '\"', $spot->tooltip_content->plain_text);
      $spot->tooltip_content->plain_text = str_replace("\n", "", $spot->tooltip_content->plain_text);
    }
    
    if (isset($spot->tooltip_content->squares_settings)) {
      // Loop over containers
      for ($j=0; $j<count($spot->tooltip_content->squares_settings->containers); $j++) {
        $container = $spot->tooltip_content->squares_settings->containers[$j];
        $elements = $container->settings->elements;

        // Loop over elements
        if (is_array($elements)) {
          for ($k=0; $k<count($elements); $k++) {
            $element = $elements[$k];

            if ($element->settings->name == 'Paragraph') {
              // Replace
              if (isset($element->options->text->text)) {
                $element->options->text->text = do_shortcode($element->options->text->text);

                $pattern = '/\\+"/';
                $element->options->text->text = preg_replace($pattern, '\"', $element->options->text->text);
                $element->options->text->text = str_replace("\n", "", $element->options->text->text);
              }
            }
          }
        }
      }
    }
  }

  $save['json'] = json_encode($decoded);
  return $save;
}

// Enable the option show in rest
add_filter( 'acf/rest_api/field_settings/show_in_rest', '__return_true' );

// Enable the option edit in rest
add_filter( 'acf/rest_api/field_settings/edit_in_rest', '__return_true' );

function generate_apartment_object() {
  $query = new WP_Query(array(
    'post_type' => 'apartment',
    'post_status' => 'publish',
    'orderby' => 'ID',
    'order' => 'DESC',
    'posts_per_page' => -1
  ));
  $floorplateJSON = array();
  while ($query->have_posts()) {
    $query->the_post();
    $floorplateID = get_the_ID();
    $floorplateTitle = get_the_title();
    $floorplateContent = apply_filters( 'the_content', get_the_content() );
    $floorplateImage = "";
    if ( get_field('floorplate_image') ) { 
      $floorplateImage = get_field('floorplate_image', $floorplateID);
      $floorplateImage = 'https://' . $_SERVER['HTTP_HOST'] . $floorplateImage['url'];
    } 
    $floorplatePDF = "";
    if ( get_field( 'floorplate_pdf', $floorplateID) ) { 
      $floorplatePDF = get_field( 'floorplate_pdf', $floorplateID);
      $floorplatePDF = 'https://' . $_SERVER['HTTP_HOST'] . $floorplatePDF['url'];
    } 
    $floorplatePermalink = get_the_permalink();
    // $floorplatePDFURL = get_field( 'floorplate_pdf_url', $floorplateID);
    // $floorplateImageURL = get_field('floorplate_image_url', $floorplateID);
    $floorplateLevel = get_field( 'level', $floorplateID );
    $floorplateNumber = get_field( 'number', $floorplateID );
    $floorplateType = get_field( 'type', $floorplateID );
    $floorplateBeds = get_field( 'beds', $floorplateID );
    $floorplateBaths = get_field( 'baths', $floorplateID );
    $floorplateCars = get_field( 'carbays', $floorplateID );
    $floorplateMCBay = get_field( 'mc_bay', $floorplateID );
    $floorplatePlan = get_field( 'plan', $floorplateID );
    $floorplateAspect = get_field( 'aspect', $floorplateID );
    $floorplanStore = get_field( 'store', $floorplateID );
    $floorplanPolygon = get_field( 'floorplate_polygon', $floorplateID );
    $floorplanPolygonID = $floorplanPolygon['value'];
    $floorplanPolygonTitle = $floorplanPolygon['label'];
    if (get_field('pdf', $floorplateLevel['value'])) {
      $levelPDF = get_field('pdf', $floorplateLevel['value']);
      $levelPDF = $levelPDF['url'];
      $levelPDF = 'https://' . $_SERVER['HTTP_HOST'] . $levelPDF;
    } else {
      $levelPDF = "#";
    }
    $floorplateInternalArea = get_field( 'internal_architectural_area', $floorplateID );
    $floorplateOutdoorArea = get_field( 'balcony_architectural_area', $floorplateID );
    $floorplateInternalAreaStrata = get_field( 'internal_strata_area', $floorplateID );
    $floorplateOutdoorAreaStrata = get_field( 'balcony_strata_area', $floorplateID );
    $floorplateTotalArea = get_field( 'total_area', $floorplateID );
    $floorplateTotalOutdoorArea = get_field( 'total_outdoor_area', $floorplateID );
    if ( get_field( 'status', $floorplateID ) ) {
      $floorplanstatus = get_field( 'status', $floorplateID );
    } else {
      $floorplanstatus = 'available';
    }
    $floorplateArray = array(
      'postID' => strval($floorplateID),
      'image' => $floorplateImage,
      'polyID' => strval($floorplanPolygonID),
      'polyTitle' => strval($floorplanPolygonTitle),
      'permalink' => htmlentities($floorplatePermalink, ENT_QUOTES | ENT_IGNORE, "UTF-8"),
      'pdf' => htmlentities($floorplatePDF, ENT_QUOTES | ENT_IGNORE, "UTF-8"),
      'levelLabel' => strval($floorplateLevel['label']),
      'levelValue' => strval($floorplateLevel['value']),
      'level' => strval($floorplateLevel['label']),
      'levelPDF' => htmlentities($levelPDF, ENT_QUOTES | ENT_IGNORE, "UTF-8"),
      'number' => strval($floorplateNumber),
      'type' => strval($floorplateType),
      'beds' => strval($floorplateBeds),
      'baths' => strval($floorplateBaths),
      'cars' => strval($floorplateCars),
      'store' => strval($floorplanStore),
      'internalArchitecturalArea' => strval($floorplateInternalArea),
      'balconyArchitecturalArea' => strval($floorplateOutdoorArea),
      'internalStrataArea' => strval($floorplateInternalAreaStrata),
      'balconyStrataArea' => strval($floorplateOutdoorAreaStrata),
      'totalArea' => strval($floorplateTotalArea),
      'totalOutdoorArea' => strval($floorplateTotalOutdoorArea),
      'mcBay' => strval($floorplateMCBay),
      'plan' => strval($floorplatePlan),
      'aspect' => strval($floorplateAspect),
      'status' => $floorplanstatus
    );
    $floorplateJSON[strval($floorplanPolygonID)] = $floorplateArray;
  }
  wp_reset_query();
  return $floorplateJSON;
}

function apartments_level_query() {
  $levelQuery = new WP_Query(array(
    'post_type' => 'level',
    'post_status' => 'publish',
    'orderby' => 'title',
    'order' => 'DESC',
    'posts_per_page' => -1,
    'sort_column' => 'menu_order'
  ));
  $levelArray = array();
  $building_svg_shape = array();
  $floorplates_svg_layer = array();
  while ($levelQuery->have_posts()) {
    $levelQuery->the_post();
    $levelPostID = get_the_ID();
    $levelPost = get_post();
    if (get_field('building_svg_shape')) {
      $floorplates_svg_layer = get_field('floorplates_svg_layer');
      $building_svg_shape = get_field('building_svg_shape');
      $levelMenuOrder = $levelPost->menu_order;
      $levelPostTitle = get_the_title();
      $levelPDF = get_field('pdf');
      $levelFloorplatesSvgTitle = $floorplates_svg_layer['label'];
      $levelFloorplatesSvgID = $floorplates_svg_layer['value'];
      if ( array_key_exists('value', $building_svg_shape) && array_key_exists('label', $building_svg_shape) ) {
        $levelPolyTitle = $building_svg_shape['label'];
        $levelPolyID = $building_svg_shape['value'];
        $levelArray[$levelMenuOrder]['menu_order'] = $levelMenuOrder;
        $levelArray[$levelMenuOrder]['post_id'] = $levelPostID;
        $levelArray[$levelMenuOrder]['post_title'] = $levelPostTitle;
        if ( array_key_exists('value', $floorplates_svg_layer) && array_key_exists('label', $floorplates_svg_layer) ) {
          $levelArray[$levelMenuOrder]['floorplates_svg_title'] = $levelFloorplatesSvgTitle;
          $levelArray[$levelMenuOrder]['floorplates_svg_id'] = $levelFloorplatesSvgID;
        } else {
          $levelArray[$levelMenuOrder]['floorplates_svg_title'] = "";
          $levelArray[$levelMenuOrder]['floorplates_svg_id'] = "";
        }
        $levelArray[$levelMenuOrder]['poly_title'] = $levelPolyTitle;
        $levelArray[$levelMenuOrder]['poly_id'] = $levelPolyID;
        if ( get_field('pdf') ) {
          $levelArray[$levelMenuOrder]['pdf'] = 'https://' . $_SERVER['HTTP_HOST'] . $levelPDF["url"];
        } else {
          $levelArray[$levelMenuOrder]['pdf'] = '#';
        }
      }
    }
  }
  wp_reset_query();
  return $levelArray;
}

function generate_svg_maps_levels() {
  $svg_saves = array();
  $levels = array();
  $returnArray = array();
  
  if (get_field('floorplates_svg_map', 'option') && get_option('image-map-pro-wordpress-admin-options')) {
    $floorplates_svg_map = get_field('floorplates_svg_map', 'option');
    $svg_image_map_options = get_option('image-map-pro-wordpress-admin-options');
    $svg_image_map_meta = json_decode(json_encode($svg_image_map_options), true);
    $svg_image_map_meta_saves = $svg_image_map_meta['saves'];
    $svg_saves = array();
    foreach ( $svg_image_map_meta_saves as $save ) { 
      $decoded_save = sanitize_json_for_save($save);
      $decoded_save = json_decode($decoded_save['json']);
      $svg_saves[$decoded_save->id]['id'] = $decoded_save->id;
      $svg_saves[$decoded_save->id]['shortcode'] = $decoded_save->general->shortcode;
      $svg_saves[$decoded_save->id]['name'] = $decoded_save->general->name;
      $svg_saves[$decoded_save->id]['width'] = $decoded_save->general->width;
      $svg_saves[$decoded_save->id]['height'] = $decoded_save->general->height;
      $svg_saves[$decoded_save->id]['image'] = $decoded_save->image->url;
      if (intval($decoded_save->id) === intval($floorplates_svg_map['value'])) {
        $floorplates_svg_levels = $decoded_save->layers->layers_list;
        foreach ( $floorplates_svg_levels as $level ) {
          $levels[strval($level->id)]['id'] = strval($level->id);
          $levels[strval($level->id)]['title'] = $level->title;
          $levels[strval($level->id)]['image_url'] = $level->image_url;
          $levels[strval($level->id)]['image_width'] = $level->image_width;
          $levels[strval($level->id)]['image_height'] = $level->image_height;
        }
        if (count($levels) > 0) {
          $levels = array_reverse($levels);
        }
      }
    }
    $returnArray[0] = $svg_saves;
    $returnArray[1] = $levels;
  }
  return $returnArray;
}


