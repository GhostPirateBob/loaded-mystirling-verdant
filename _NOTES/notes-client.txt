================================================================================
--------------------------------------------------------------------------------
                MYSTIRLING MULTISITE FUNCTIONALITY NOTES
--------------------------------------------------------------------------------

              GENERAL/OVERALL - CMS REQUIREMENTS

  - Be able to change all content (images and text) through out the whole site
  - Be able to control ALL typefaces including,
     headings / first paragraph / body copy / buttons etc...
     ...fonts, sizes and colour,
     ...and change hover over colours for buttons, text and boxes
  - Be able to hide sections
  - Be able to add and delete ‘spacers’
  - Say if we don’t have a sub-heading where there is one on the wireframe 
    (or any text / image in the site), we would want to be able to remove not 
    only the text / image but the space provided for it.
  - Be able to change background colours of each section and have the option 
    to add a texture / image as a background.
  - Be able to hide buttons
  - Be able to select different image ‘cluters.’ 
    We will design different cluster configurations that we would like to be 
    able to select from through the site. Basically anywhere there is image 
    clusters on the site, we want to be able to have the option to change the 
    configuration of the clusters, which includes the number of images we want.
  - Under ‘Featured Apartments’ (which is on a number of pages) add and delete 
    featured apartments, their content and change their order.
  - Under ‘Our Developments’ (team and contact pages) be able to add, change, 
    and remove developments, their content and the order they’re displayed in.

--------------------------------------------------------------------------------

              NAVIGATION (ENQUIRE / MENU) - CMS REQUIREMENTS

  - Change colour and font of enquire button including their hover over state.
  - Change the menu names, font and colour 
  - Change the colour of the menu background

--------------------------------------------------------------------------------

              HOME PAGE - CMS REQUIREMENTS

  - Be able to change the hero image for the background of the top section.
  - Be able to add logo on top of the hero image and it still be responsive
  - Be able to add sales splashes eg ‘50% Sold’
  - Be able to add text underneath the logo and have control of the font,
    size and colour.
  - Be able to select from a vertical and a horizontal ‘external render image’ 
    layout configuration for the section underneath the top hero area.
  - Be able to change the ‘play video’ button colour

--------------------------------------------------------------------------------

              HOME PAGE (VERTICAL) - CMS REQUIREMENTS

We need to be able to do the following in the CMS:

  - Be able to select from a vertical and a horizontal ‘external render image’ 
    layout configuration for the section underneath the top hero area.
      (this page is the vertical version)

--------------------------------------------------------------------------------

              HEADER NAVIGATION - CMS REQUIREMENTS

We need to be able to do the following in the CMS:

  - Be able to update the My Stirling menu when required

--------------------------------------------------------------------------------

              HEADER ENQUIRE - CMS REQUIREMENTS

We need to be able to do the following in the CMS:

  - Be able to change the colour of this form background plus,
  - fonts / button colour / button hover over colour

--------------------------------------------------------------------------------

              LOCATION PAGE - CMS REQUIREMENTS

We need to be able to do the following in the CMS:

  - Be able to change the google map colour and points of interest
  - Be able to change points of interest on the aerial image
    (like on the Brixton website)
  - Be able to change how many ‘points of interest’ we want 
    (for each column for the list)

--------------------------------------------------------------------------------

              BUILDING PAGE - CMS REQUIREMENTS

We need to be able to do the following in the CMS:

  - Be able to add a list of icons in the amenities section
  - Amenity images - add as many as we like
  - with horizontal and vertical options

--------------------------------------------------------------------------------

              APARTMENTS PAGE (ARCHIVE) - CMS REQUIREMENTS

We need to be able to do the following in the CMS:
  - Be able to change the building line drawing and the level hover overs
  - Be able to add levels - and options to add 
      ‘basement’ ‘basement 2’ ‘ground’ ‘penthouse’
  - Be able to change floorplate and the apartment hover over selection areas
  - Be able to change floorplans and statistics
  - Be able to change PDF documents for the download buttons and,
  - ...Be able to hide the buttons
  - Have the option of having one or two colour scheme images per selection
  - Be able to add more colour schemes 
      (there’s usually 2-3 schemes for each development).
  - Be able to change the button names for the colour schemes,
    and the specification buttons.
  - Be able to hide the specification selection

--------------------------------------------------------------------------------

              GALLERY PAGE - CMS REQUIREMENTS

We need to be able to do the following in the CMS:

  - Be able to add as many images as we like to each section and they 
    still ‘grid up’ nicely in all desktop/ipad and mobile views.

--------------------------------------------------------------------------------

              TEAM PAGE - CMS REQUIREMENTS

We need to be able to do the following in the CMS:

  - Be able to add and remove teams and the content including logo

--------------------------------------------------------------------------------

              CONTACT PAGE - CMS REQUIREMENTS

We need to be able to do the following in the CMS:

  - Be able to add points to the map and add custom pins.

--------------------------------------------------------------------------------
